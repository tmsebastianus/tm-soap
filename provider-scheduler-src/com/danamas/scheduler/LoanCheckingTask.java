package com.danamas.scheduler;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.PrepaidProvider;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;

public class LoanCheckingTask {
	
Logger logger = Logger.getLogger(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	
	public void loanConfirmation() {
		try {
			
			logger.info("[ "+this.getClass().getName()+" -- loanConfirmation() --- START --- ]");
			
			/*
			List<Loan> loans = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), Restrictions.eq("status", LoanStatus.OPEN));
			for (Loan loan : loans) {
				Long prepaidProviderId = null;
				if (loan.getAccountId()>0) prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(loan.getAccountId());
				PrepaidProvider prepaidProvider = null;
				if (prepaidProviderId!=null) prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findById(prepaidProviderId);
				if (prepaidProvider!=null && prepaidProvider.getLoanCheckingUrl()!=null && prepaidProvider.getLoanCheckingUrl().length()>0) {
					
					logger.info("[ data loan-ID : "+loan.getLoanId()+" accountId : "+loan.getAccountId()+" prepaidProviderId : "+prepaidProviderId+" prepaidProvider.getLoanCheckingUrl() : "+prepaidProvider.getLoanCheckingUrl()+"  ] ");
					
					String email = accountDAOFactory.getAccountDAO().getEmailByAccountId(loan.getAccountId());
					LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findById(loan.getProductId());
					
					String[] status = JSONService.confirmLoan(prepaidProvider.getLoanCheckingUrl(), 90000, loan.getTransactionCode(), Formater.getFormatedOutputForm(2, loan.getAmount().doubleValue()), email, loanProduct.getCode(), Formater.getFormatedDate(loan.getLoanDate(), "ddMMyyyy"));
					
					// update status if success!
					logger.info(" [ status => "+status[0]+" "+status[1]+" ]");
					if (status!=null && status[0].length()>0 && status[0].equalsIgnoreCase("00")) {
						loan.setStatus(LoanStatus.APPROVED);
						loan.setFundingStatus(LoanFundingStatus.NEW);
					} else {
						loan.setStatus(LoanStatus.REJECTED);
						loan.setFundingStatus(null);
					}
					
				}
				// update loan
				loanDAOFactory.getLoanDAO().update(loan);			
				
			}
			*/
			
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, applicationSetup.getRejectLoanStatusInterval() * -1);
			
			List<PrepaidProvider> prepaidProviders = accountDAOFactory.getPrepaidProviderDAO().findByCriteria(Order.asc("prepaidProviderId"), Restrictions.eq("autoApproveLoan", Boolean.FALSE));
			for (PrepaidProvider row : prepaidProviders) {
				loanDAOFactory.getLoanDAO().updateLoanStatusRejected(row.getPrepaidProviderId(), cal.getTime());
			}
			
			logger.info("[ "+this.getClass().getName()+" -- loanConfirmation() --- END --- ]");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
