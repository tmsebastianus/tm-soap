package com.danamas.scheduler;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Date;
import java.util.List;

import com.tokomodal.loan.model.Voucher;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;

public class DailyActivateVoucherTask {

	Logger logger = Logger.getLogger(this.getClass());
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	
	public void validateVoucher() {
		try {
			logger.info(">>>>>>>>>>>>>>>>>>>> Start of DailyActivateVoucherTask <<<<<<<<<<<<<<<<<<<<");
			
			List<Voucher> activateVouchers = loanDAOFactory.getVoucherDAO().findByCriteria(Order.asc("voucherId"), Restrictions.eq("startDate", new Date()), Restrictions.eq("isActive", "N"));
			List<Voucher> deactivateVouchers = loanDAOFactory.getVoucherDAO().findByCriteria(Order.asc("voucherId"), Restrictions.lt("endDate", new Date()), Restrictions.eq("isActive", "Y"));
			
			for (Voucher voucher : activateVouchers) {
				voucher.setIsActive("Y");
				loanDAOFactory.getVoucherDAO().update(voucher);
			}
			
			for (Voucher voucher : deactivateVouchers) {
				voucher.setIsActive("N");
				loanDAOFactory.getVoucherDAO().update(voucher);
			}
					
			logger.info(">>>>>>>>>>>>>>>>>>>> End of DailyActivateVoucherTask <<<<<<<<<<<<<<<<<<<<");
		} catch (Exception e) {
			logger.error(">>>>>>>>>>>>>>>>>>>> Something error in DailyActivateVoucherTask <<<<<<<<<<<<<<<<<<<<");
			e.printStackTrace();
		} finally {
			
		}
	}
}
