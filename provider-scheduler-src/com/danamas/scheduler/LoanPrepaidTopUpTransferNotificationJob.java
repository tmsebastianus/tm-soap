package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class LoanPrepaidTopUpTransferNotificationJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			LoanPrepaidTopUpTransferNotificationTask loanPrepaidTopUpTransferNotificationTask = new LoanPrepaidTopUpTransferNotificationTask();
			loanPrepaidTopUpTransferNotificationTask.topUpNotification();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
