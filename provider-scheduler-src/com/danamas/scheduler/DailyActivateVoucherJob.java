package com.danamas.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DailyActivateVoucherJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		DailyActivateVoucherTask dailyActivateVoucherTask = new DailyActivateVoucherTask();
		dailyActivateVoucherTask.validateVoucher();
	}

}
