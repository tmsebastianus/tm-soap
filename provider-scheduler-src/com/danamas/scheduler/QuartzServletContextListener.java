package com.danamas.scheduler;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzServletContextListener implements ServletContextListener {
	
	Log logger = LogFactory.getFactory().getInstance(this.getClass());
	
	public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";
	private ServletContext ctx = null;
	private StdSchedulerFactory factory = null;


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		try {
            factory.getDefaultScheduler().shutdown();
		} catch (SchedulerException ex) {
			ex.printStackTrace();
            logger.error("Error stopping Quartz", ex);
		}
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ctx = arg0.getServletContext();
        try {

             factory = new StdSchedulerFactory();
             // Start the scheduler now
             factory.getScheduler().start();
             logger.info("Storing QuartzScheduler Factory : "+factory+" at " + QUARTZ_FACTORY_KEY);
             ctx.setAttribute(QUARTZ_FACTORY_KEY, factory);


       } catch (Exception ex) {
    	   ex.printStackTrace();
           logger.error("Quartz failed to initialize", ex);
       }
		
	}

}
