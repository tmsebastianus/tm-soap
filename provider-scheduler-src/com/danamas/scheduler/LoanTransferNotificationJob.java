package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class LoanTransferNotificationJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			LoanTransferNotificationTask loanTransferNotificationTask = new LoanTransferNotificationTask();
			loanTransferNotificationTask.sendNotification();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
