package com.danamas.scheduler;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.Lookup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;
import com.mpe.common.util.Constants;
import com.mpe.common.util.Formater;
import com.mpe.message.model.OutgoingEmail;
import com.mpe.message.model.dao.MessageDAOFactory;
import com.mpe.message.model.dao.MessageDAOFactoryHibernate;
import com.tokomodal.account.model.Account;
import com.tokomodal.account.model.AccountType;
import com.tokomodal.account.model.BankAccount;
import com.tokomodal.account.model.Personal;
import com.tokomodal.account.model.PrepaidProvider;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.account.model.other.Gender;
import com.tokomodal.loan.model.InterestType;
import com.tokomodal.loan.model.Loan;
import com.tokomodal.loan.model.LoanDisbursementTransferInfo;
import com.tokomodal.loan.model.LoanProduct;
import com.tokomodal.loan.model.LoanProductThirdParty;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.loan.model.other.LoanDisbursmentTransferNotifyList;

public class LoanTransferNotificationTask {
	
	Logger logger = Logger.getLogger(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	MessageDAOFactory messageDAOFactory = MessageDAOFactory.instance(MessageDAOFactoryHibernate.class);
	
	public void sendNotification() {
		try {
			logger.info("[ LoanTransferNotificationTask -- sendNotification() --- START --- ]");
			List<LoanDisbursmentTransferNotifyList> disbursementTransferInfos = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findLoanByNonePrepaidTopUp(10);
			logger.info(" count notify : "+disbursementTransferInfos.size());
			int i = 1;
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			for (LoanDisbursmentTransferNotifyList disbursementTransferInfoNotify : disbursementTransferInfos) {
				// send to json
				// 90s => 90000ms
				if (disbursementTransferInfoNotify.getStatus()!=null && disbursementTransferInfoNotify.getStatus().length()> 0 && 
				   (disbursementTransferInfoNotify.getStatus().startsWith("00") || disbursementTransferInfoNotify.getStatus().startsWith("d0"))) {
					
					/*String bankReferenceNumber = "";
					String accountCreditCode = "";
					if (disbursementTransferInfoNotify.getStatus().startsWith("d0")) {
						String ct = disbursementTransferInfoNotify.getTransferReference().substring(disbursementTransferInfoNotify.getTransferReference().indexOf("~~"));
						String a = ct.substring(2,ct.indexOf("861")-1);
						
						//String cb = disbursementTransferInfo.getTransferReference().substring(disbursementTransferInfo.getTransferReference().indexOf("(")+1, disbursementTransferInfo.getTransferReference().indexOf(".txt"));
						bankReferenceNumber = a;
						
						accountCreditCode = disbursementTransferInfoNotify.getTransactionCode();
					} else if (disbursementTransferInfoNotify.getStatus().startsWith("00")) bankReferenceNumber = disbursementTransferInfoNotify.getTransferReference();
					
					//Long loanId = loanDAOFactory.getLoanDAO().getLoanIdByLoanDisbursementTransferInfoId(disbursementTransferInfoNotify.getLoanDisbursementTransferInfoId());
					Long loanId = disbursementTransferInfoNotify.getLoanId();
					Long accountId = null;
					if (loanId!=null) accountId = loanDAOFactory.getLoanDAO().getAccountIdByLoanId(loanId);
					Long prepaidProviderId = null;
					if (accountId!=null) prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(accountId);
					PrepaidProvider prepaidProvider = null;
					if (prepaidProviderId!=null) prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findById(prepaidProviderId);
					
					if (prepaidProvider!=null && prepaidProvider.getBankTransferNotifUrl()!=null && prepaidProvider.getBankTransferNotifUrl().length()>0) {
						
						LoanDisbursementTransferInfo disbursementTransferInfo = null;
						disbursementTransferInfo = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findById(disbursementTransferInfoNotify.getLoanDisbursementTransferInfoId());
						if (disbursementTransferInfo!=null) {
							logger.info("<<<<data non pulsa : "+i+" bankReferenceNumber : "+bankReferenceNumber+" loanId : "+loanId+ " accountId : "+accountId+" prepaidProviderId : "+prepaidProviderId+" prepaidProvider.getBankTransferNotifUrl() : "+prepaidProvider.getBankTransferNotifUrl()+">>>> ");
							
							String[] status = JSONService.postTransferNotification(prepaidProvider.getBankTransferNotifUrl(), 90000, 
									accountCreditCode, disbursementTransferInfoNotify.getAmount().toString(), CommonUtil.getStringFromDate(disbursementTransferInfoNotify.getTransferCompleteDate(), "yyyyMMddHHmmss"), bankReferenceNumber);
							
							// update status if success!
							logger.info(" [ status => "+status[0]+" "+status[1]+" ]");
							//if (status!=null && status[0].length()>0 && (status[0].equalsIgnoreCase("00") || (status[0].equalsIgnoreCase("01")))) {
							if (status!=null && status[0].length()>0) {
								disbursementTransferInfo.setNotifySendDate(new Date());
								disbursementTransferInfo.setNotifyStatus(status[1]);
								loanDAOFactory.getLoanDisbursementTransferInfoDAO().update(disbursementTransferInfo);
								
								// drawdown confirmation!
								//Long loanId = loanDAOFactory.getLoanDAO().getLoanIdByLoanDisbursementTransferInfoId(disbursementTransferInfo.getLoanDisbursementTransferInfoId());
								if (loanId!=null) {
									//
									loanDAOFactory.getLoanDAO().updateDrawdownConfirmation(loanId, new Date(), prepaidProvider.getName());
								}
							}
						}
					} */
					Loan loan = loanDAOFactory.getLoanDAO().findById(disbursementTransferInfoNotify.getLoanId());
					LoanProduct product = loanDAOFactory.getLoanProductDAO().findById(loan.getProductId());
					BigDecimal adminFee = loan.getAmount().multiply(loan.getInterest().divide(new BigDecimal(100)));
					BigDecimal totalAmount = loan.getAmount().subtract(((loan.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))));
					
					Long prepaidProviderId = null;
					if (loan!=null && loan.getAccountId()>0) prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(loan.getAccountId());
					
					Account account = null;
					Session session = null;
					try {
						session = accountDAOFactory.getAccountDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
						account = (Account)session.createCriteria(Account.class)
								.setFetchMode("address", FetchMode.JOIN)
								.setFetchMode("businessAddress", FetchMode.JOIN)
								.setFetchMode("contact", FetchMode.JOIN)
								.add(Restrictions.eq("accountId", loan.getAccountId()))
								.setMaxResults(1)
								.uniqueResult();
					} finally {
						if (session!=null) session.close();
					}
					
					PrepaidProvider prepaidProvider = null;
					Session sessionPrepaid = null;
					try {
						sessionPrepaid = accountDAOFactory.getPrepaidProviderDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
						prepaidProvider = (PrepaidProvider)sessionPrepaid.createCriteria(PrepaidProvider.class)
								.setFetchMode("contact", FetchMode.JOIN)
								.add(Restrictions.eq("prepaidProviderId", prepaidProviderId))
								.setMaxResults(1)
								.uniqueResult();
					} finally {
						if (sessionPrepaid!=null) sessionPrepaid.close();
					}
					
					if (prepaidProvider!=null && prepaidProvider.getContact().getEmail1()!=null) {
						logger.info("<<<<data non pulsa : "+i+" loanId : "+loan.getLoanId()+ " accountId : "+account.getAccountId()+" prepaidProviderId : "+prepaidProviderId+">>>> ");
						//BUAT PROD
						Properties p2 = new Properties();
						ClassLoader loader = LoanTransferNotificationTask.class.getClassLoader();
					    URL r = loader.getResource("/");
					    String pathCls = r.getPath();
					    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
						p2.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
						Velocity.init(p2);
						VelocityContext context = new VelocityContext();
						
						if (account.getType()==AccountType.PERSONAL) {
							Personal personal = (Personal)account;
							if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
							else context.put("salutation", "Ibu/Sdri.");
						} else {
							context.put("salutation", "");
						}
						context.put("name", account.getName());
						context.put("drawdownDate", Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMMM yyyy"));
//						if (loan.getType()==InterestType.ADVANCE) {
//							context.put("amount", Formater.getFormatedOutput(0, loan.getAmount().doubleValue()));
//						} else {
						context.put("amount", Formater.getFormatedOutput(0, loan.getAmount().doubleValue()));
					//	}
						BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("prepaidProviderId", prepaidProviderId));
						Lookup lookup = basicDAOFactory.getLookupDAO().findById(bankAccount.getBank().getLookupId());
						context.put("bank", lookup.getName());
						context.put("accountNumber", loan.getAccountNumber());
						//LoanProduct product = loanDAOFactory.getLoanProductDAO().findById(loan.getProductId());
						context.put("loan", product.getName()+ " No	: "+loan.getTransactionCode());
						context.put("overDueDate", Formater.getFormatedDate(loan.getOverDueDate(), "dd MMMM yyyy"));
						context.put("adminFee", Formater.getFormatedOutput(0, adminFee.doubleValue()));
						context.put("totalAmount",  Formater.getFormatedOutput(0, totalAmount.doubleValue()));
						
						StringWriter w = new StringWriter();
					    Velocity.mergeTemplate("/template/drawdown.vm", context, w );
					    
						// save to email
//						OutgoingEmail outgoingEmail = new OutgoingEmail();
//						outgoingEmail.setEmailDate(new Date());
//						outgoingEmail.setSender(applicationSetup.getFromEmailAddress());
//						outgoingEmail.setSubject("[Tokomodal] Konfirmasi Pencairan Pinjaman");
//						outgoingEmail.setTo(prepaidProvider.getContact().getEmail1());
//						outgoingEmail.setMessage(w.toString());
//						outgoingEmail.setType("[Tokomodal] Konfirmasi Pencairan Pinjaman");
//						
//						messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail);
						
						LoanDisbursementTransferInfo disbursementTransferInfo = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findById(disbursementTransferInfoNotify.getLoanDisbursementTransferInfoId());
						disbursementTransferInfo.setNotifySendDate(new Date());
						disbursementTransferInfo.setNotifyStatus("SUKSES");
						loanDAOFactory.getLoanDisbursementTransferInfoDAO().update(disbursementTransferInfo);
					}
				}
				i++;
			}
			
			logger.info("[ LoanTransferNotificationTask -- sendNotification() --- END --- ]");			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendNotificationOld() {
		
		try {
			logger.info("[ LoanTransferNotificationTask -- sendNotification() --- START --- ]");
			//ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			
			//if (applicationSetup!=null && applicationSetup.getKimoBankTransferNotifUrl()!=null && applicationSetup.getKimoBankTransferNotifUrl().length()>0) {
				Object[] obj = new Object[2]; obj[0] = "00"; obj[1]="d0";
				List<LoanDisbursementTransferInfo> disbursementTransferInfos = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findByCriteria(Order.asc("loanDisbursementTransferInfoId"), 10, 
						Restrictions.or(Restrictions.isNull("notifySendDate"), Restrictions.sqlRestriction("SUBSTR(notify_status,1,2) IN ('99')")), 
						Restrictions.in("status", obj));
				
				logger.info(" count notify : "+disbursementTransferInfos.size());
				int i = 1;
				for (LoanDisbursementTransferInfo disbursementTransferInfo : disbursementTransferInfos) {
					
					//logger.info(" [ data : "+i+" disbursementTransferInfo > "+disbursementTransferInfo.getAccountCreditCode() + " ] ");
					
					// send to json
					// 90s => 90000ms
					if (disbursementTransferInfo.getStatus()!=null && disbursementTransferInfo.getStatus().length()> 0 && 
					   (disbursementTransferInfo.getStatus().startsWith("00") || disbursementTransferInfo.getStatus().startsWith("d0"))) {
						
						String bankReferenceNumber = "";
						String accountCreditCode = "";
						if (disbursementTransferInfo.getStatus().startsWith("d0")) {
							String ct = disbursementTransferInfo.getTransferReference().substring(disbursementTransferInfo.getTransferReference().indexOf("~~"));
							String a = ct.substring(2,ct.indexOf("861")-1);
							
							//String cb = disbursementTransferInfo.getTransferReference().substring(disbursementTransferInfo.getTransferReference().indexOf("(")+1, disbursementTransferInfo.getTransferReference().indexOf(".txt"));
							bankReferenceNumber = a;
							
							accountCreditCode = disbursementTransferInfo.getAccountCreditCode();
						} else if (disbursementTransferInfo.getStatus().startsWith("00")) bankReferenceNumber = disbursementTransferInfo.getTransferReference();
						
						Long loanId = loanDAOFactory.getLoanDAO().getLoanIdByLoanDisbursementTransferInfoId(disbursementTransferInfo.getLoanDisbursementTransferInfoId());
						Long accountId = null;
						if (loanId!=null) accountId = loanDAOFactory.getLoanDAO().getAccountIdByLoanId(loanId);
						Long prepaidProviderId = null;
						if (accountId!=null) prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(accountId);
						PrepaidProvider prepaidProvider = null;
						if (prepaidProviderId!=null) prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findById(prepaidProviderId);
						
						//logger.info("[ data : "+i+" bankReferenceNumber : "+bankReferenceNumber+" loanId : "+loanId+ " accountId : "+accountId+" prepaidProviderId : "+prepaidProviderId+" prepaidProvider.getBankTransferNotifUrl() : "+prepaidProvider.getBankTransferNotifUrl()+"  ] ");
						
						if (prepaidProvider!=null && prepaidProvider.getBankTransferNotifUrl()!=null && prepaidProvider.getBankTransferNotifUrl().length()>0) {
							
							logger.info("[ data non pulsa : "+i+" bankReferenceNumber : "+bankReferenceNumber+" loanId : "+loanId+ " accountId : "+accountId+" prepaidProviderId : "+prepaidProviderId+" prepaidProvider.getBankTransferNotifUrl() : "+prepaidProvider.getBankTransferNotifUrl()+"  ] ");
							
							String[] status = JSONService.postTransferNotification(prepaidProvider.getBankTransferNotifUrl(), 90000, 
									accountCreditCode, disbursementTransferInfo.getAmount().toString(), CommonUtil.getStringFromDate(disbursementTransferInfo.getTransferCompleteDate(), "yyyyMMddHHmmss"), bankReferenceNumber);
							
							// update status if success!
							logger.info(" [ status => "+status[0]+" "+status[1]+" ]");
							//if (status!=null && status[0].length()>0 && (status[0].equalsIgnoreCase("00") || (status[0].equalsIgnoreCase("01")))) {
							if (status!=null && status[0].length()>0) {
								disbursementTransferInfo.setNotifySendDate(new Date());
								disbursementTransferInfo.setNotifyStatus(status[1]);
								loanDAOFactory.getLoanDisbursementTransferInfoDAO().update(disbursementTransferInfo);
								
								// drawdown confirmation!
								//Long loanId = loanDAOFactory.getLoanDAO().getLoanIdByLoanDisbursementTransferInfoId(disbursementTransferInfo.getLoanDisbursementTransferInfoId());
								if (loanId!=null) {
									//
									loanDAOFactory.getLoanDAO().updateDrawdownConfirmation(loanId, new Date(), prepaidProvider.getName());
								}
								
								
							}
							
						} else {
							
							Loan loan = loanDAOFactory.getLoanDAO().findById(loanId);
							LoanProductThirdParty loanProductThirdParty = null; 
							if (loan != null)loanProductThirdParty = loanDAOFactory.getLoanProductThirdPartyDAO().findByCriteria(Restrictions.eq("loanProductId", loan.getProductId()));
							
							if (loanProductThirdParty != null && loanProductThirdParty.getDanamasPulsaTopUpServiceUrl() != null) {
								
								logger.info("[ data pulsa : "+i+" bankReferenceNumber : "+bankReferenceNumber+" loanId : "+loanId+ " accountId : "+accountId+"  ] ");
								
								String[] status = JSONService.postTransferNotificationAddTopUpSaldo(loanProductThirdParty.getDanamasPulsaTopUpServiceUrl(), 90000, 
										accountId, disbursementTransferInfo.getAmount().toString(), 
										bankReferenceNumber, CommonUtil.getStringFromDate(disbursementTransferInfo.getTransferCompleteDate(), "yyyyMMddHHmmss"), 
										"BORROWER", accountCreditCode);
								
								// update status if success!
								logger.info(" [ status => "+status[0]+" "+status[1]+" ]");
								//if (status!=null && status[0].length()>0 && (status[0].equalsIgnoreCase("00") || (status[0].equalsIgnoreCase("01")))) {
								if (status!=null && status[0].length()>0) {
									disbursementTransferInfo.setNotifySendDate(new Date());
									disbursementTransferInfo.setNotifyStatus(status[1]);
									loanDAOFactory.getLoanDisbursementTransferInfoDAO().update(disbursementTransferInfo);
									
									if (loanId!=null) {
										loanDAOFactory.getLoanDAO().updateDrawdownConfirmation(loanId, new Date(), "PULSA-QIMO");
									}
									
									
								}
								
							}
							
						}
					
					}
					i++;
				}
			//}
			
			logger.info("[ LoanTransferNotificationTask -- sendNotification() --- END --- ]");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
		
	}


}
