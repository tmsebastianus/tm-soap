package com.danamas.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tokomodal.account.model.Account;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.loan.model.Loan;
import com.tokomodal.loan.model.LoanFunding;
import com.tokomodal.loan.model.LoanFundingStatus;
import com.tokomodal.loan.model.LoanStatus;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;

public class PushMessagesToFcmTask {
	Logger logger = Logger.getLogger(this.getClass());
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	String apiKey;
	
	public void push() {
		try {
			logger.info(">>>>>>>>>>>>>>>>>>>> Start of PushMessagesToFcmTask <<<<<<<<<<<<<<<<<<<<");
			
			PropertiesConfiguration config = new PropertiesConfiguration("ftp.properties");
			apiKey = config.getString("apiKey");
			
			int success = 0;

			List<Account> accounts = accountDAOFactory.getAccountDAO().findByCriteria(Order.asc("accountId"), Restrictions.isNotNull("tokenFcm"));
			List<Loan> loans = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), Restrictions.eq("status", LoanStatus.APPROVED), Restrictions.eq("fundingStatus", LoanFundingStatus.NEW));
			
			//This is for ONE SIGNAL
			if (loans.size() >= 5) {
//				JSONArray tokenFcm = new JSONArray();
//				tokenFcm.put("4e002370-b7fa-4206-a218-5216ff709068");
				JSONObject body = new JSONObject();
				body.put("app_id", "24bd7c23-73ba-4574-8fa2-1dcc5d2c9316");
				
				JSONObject headings = new JSONObject();
				headings.put("id", "Ada " + loans.size() + " pinjaman yang belum diberikan modal saat ini. \nBerikan modal yuk!");
				headings.put("en", "Ada " + loans.size() + " pinjaman yang belum diberikan modal saat ini. \nBerikan modal yuk!");
//				--headings.put("en", "There are " + loans.size() + " 	yang belum diberikan modal saat ini. \nBerikan modal yuk!");
				
				JSONObject contents = new JSONObject();
				contents.put("id", "Segera lakukan pendanaan utk meningkatkan portofolio Anda!");
				contents.put("en", "Segera lakukan pendanaan utk meningkatkan portofolio Anda!");
				
				body.put("headings", headings);
				body.put("contents", contents);
				body.put("small_icon", "tokomodal_logo");
				body.put("included_segments", "Active Users");
				body.put("android_group", "loan-reminder");
				
				JSONArray button = new JSONArray();
				
				JSONObject button1 = new JSONObject();
				button1.put("id", "goExplore");
				button1.put("text", "Danai Sekarang");
				
				button.put(button1);
				body.put("buttons", button);
				
				
				URL url = new URL("https://onesignal.com/api/v1/notifications");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setRequestProperty("Authorization", "Basic MTJjNDEzNWMtMjcwMy00MWMzLTk5MDAtZjU2MWI1ZWQ4MmVk");
				conn.setDoOutput(true);
				
				String input = body.toString();
				System.out.println(body.toString());
				
				OutputStream os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();
				os.close();

				int responseCode = conn.getResponseCode();
//				System.out.println(responseCode);
				

				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
				    response.append(inputLine);
				}
				in.close();
				
//				System.out.println(response.toString());
			}
			
			//This is for FCM
//			for(Account account : accounts) {
//				String tokenFcm = account.getTokenFcm();
//				JSONObject body = new JSONObject();
//				body.put("to", tokenFcm);
//				
//				JSONObject notification = new JSONObject();
//				notification.put("body", "Ada " + loans.size() + " pinjaman yang belum diberikan modal saat ini. \nBerikan modal yuk!");
//				notification.put("title", "Danai Pinjaman");
//				
//				JSONObject data = new JSONObject();
//				data.put("body", "This is JSONObject of Data");
//				data.put("title", "Danai Pinjaman");
//				
//				body.put("notification", notification);
//				body.put("data", data);
//				
//				URL url = new URL("https://fcm.googleapis.com/fcm/send");
//				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//				conn.setDoOutput(true);
//				conn.setRequestMethod("POST");
//				conn.setRequestProperty("Content-Type", "application/json");
//				conn.setRequestProperty("Authorization", "key=" + apiKey);
//				conn.setDoOutput(true);
//				
//				String input = body.toString();
//				
//				OutputStream os = conn.getOutputStream();
//				os.write(input.getBytes());
//				os.flush();
//				os.close();
//
//				int responseCode = conn.getResponseCode();
//				
//				logger.info(">>>>>>>>>>>>>>>>>>>> PUSHING TOKEN TO FCM <<<<<<<<<<<<<<<<<<<<");
//
//				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//				String inputLine;
//				StringBuffer response = new StringBuffer();
//
//				while ((inputLine = in.readLine()) != null) {
//				    response.append(inputLine);
//				}
//				in.close();
//				
//				JSONObject jsonObject = new JSONObject (response.toString());
//				if(!jsonObject.get("success").toString().equals("0")) {
//					success += 1;
//				} 
//			}
//			logger.info(">>>>>>>>>>>>>>>>>>>> Pushed to " + String.valueOf(success) + " Device(s) <<<<<<<<<<<<<<<<<<<<");
			
			logger.info(">>>>>>>>>>>>>>>>>>>> End of PushMessagesToFcmTask <<<<<<<<<<<<<<<<<<<<");
		} catch (Exception e) {
			logger.info(">>>>>>>>>>>>>>>>>>>> Something error in PushMessagesToFcmTask <<<<<<<<<<<<<<<<<<<<");
			e.printStackTrace();
		} 
		
	}

}
