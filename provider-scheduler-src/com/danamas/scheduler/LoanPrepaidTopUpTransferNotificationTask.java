package com.danamas.scheduler;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.loan.model.Loan;
import com.tokomodal.loan.model.LoanDisbursementTransferInfo;
import com.tokomodal.loan.model.LoanProductThirdParty;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.loan.model.other.LoanDisbursmentTransferNotifyList;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;

public class LoanPrepaidTopUpTransferNotificationTask {
	
	Logger logger = Logger.getLogger(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	
	public void topUpNotification() {
		try {
			logger.info("[ LoanPrepaidTopUpTransferNotificationTask -- topUpNotification() --- START --- ]");
			
			/*
			Object[] obj = new Object[2]; obj[0] = "00"; obj[1]="d0";
			List<LoanDisbursementTransferInfo> disbursementTransferInfos = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findByCriteria(Order.asc("loanDisbursementTransferInfoId"), 10, 
					Restrictions.or(Restrictions.isNull("notifySendDate"), Restrictions.sqlRestriction("SUBSTR(notify_status,1,2) IN ('99')")), 
					Restrictions.in("status", obj));
			*/
			
			List<LoanDisbursmentTransferNotifyList> disbursementTransferInfos = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findLoanByPrepaidTopUp(10);
			logger.info(" count notify : "+disbursementTransferInfos.size());
			int i = 1;
			for (LoanDisbursmentTransferNotifyList disbursementTransferInfoNotify : disbursementTransferInfos) {
				
				// send to json
				// 90s => 90000ms
				if (disbursementTransferInfoNotify.getStatus()!=null && disbursementTransferInfoNotify.getStatus().length()> 0 && 
				   (disbursementTransferInfoNotify.getStatus().startsWith("00") || disbursementTransferInfoNotify.getStatus().startsWith("d0"))) {
					
					String bankReferenceNumber = "";
					String accountCreditCode = "";
					if (disbursementTransferInfoNotify.getStatus().startsWith("d0")) {
						String ct = disbursementTransferInfoNotify.getTransferReference().substring(disbursementTransferInfoNotify.getTransferReference().indexOf("~~"));
						String a = ct.substring(2,ct.indexOf("861")-1);
						
						//String cb = disbursementTransferInfo.getTransferReference().substring(disbursementTransferInfo.getTransferReference().indexOf("(")+1, disbursementTransferInfo.getTransferReference().indexOf(".txt"));
						bankReferenceNumber = a;
						
						accountCreditCode = disbursementTransferInfoNotify.getTransactionCode();
					} else if (disbursementTransferInfoNotify.getStatus().startsWith("00")) bankReferenceNumber = disbursementTransferInfoNotify.getTransferReference();
					
					//Long loanId = loanDAOFactory.getLoanDAO().getLoanIdByLoanDisbursementTransferInfoId(disbursementTransferInfoNotify.getLoanDisbursementTransferInfoId());
					Long loanId = disbursementTransferInfoNotify.getLoanId();
					Long accountId = null;
					if (loanId!=null) accountId = loanDAOFactory.getLoanDAO().getAccountIdByLoanId(loanId);
					Long prepaidProviderId = null;
					if (accountId!=null) prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(accountId);
					if (prepaidProviderId==null) {
					
						Loan loan = loanDAOFactory.getLoanDAO().findById(loanId);
						LoanProductThirdParty loanProductThirdParty = null; 
						if (loan != null)loanProductThirdParty = loanDAOFactory.getLoanProductThirdPartyDAO().findByCriteria(Restrictions.eq("loanProductId", loan.getProductId()));
						
						if (loanProductThirdParty != null && loanProductThirdParty.getDanamasPulsaTopUpServiceUrl() != null) {
							
							LoanDisbursementTransferInfo disbursementTransferInfo = null;
							disbursementTransferInfo = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findById(disbursementTransferInfoNotify.getLoanDisbursementTransferInfoId());
							if (disbursementTransferInfo!=null) {
								logger.info("<<<<data pulsa : "+i+" bankReferenceNumber : "+bankReferenceNumber+" loanId : "+loanId+ " accountId : "+accountId+">>>>");
								String[] status = JSONService.postTransferNotificationAddTopUpSaldo(loanProductThirdParty.getDanamasPulsaTopUpServiceUrl(), 90000, 
										accountId, disbursementTransferInfoNotify.getAmount().toString(), 
										bankReferenceNumber, CommonUtil.getStringFromDate(disbursementTransferInfoNotify.getTransferCompleteDate(), "yyyyMMddHHmmss"), 
										"BORROWER", accountCreditCode);
								
								// update status if success!
								logger.info(" [ status => "+status[0]+" "+status[1]+" ]");
								if (status!=null && status[0].length()>0) {
									disbursementTransferInfo.setNotifySendDate(new Date());
									disbursementTransferInfo.setNotifyStatus(status[1]);
									loanDAOFactory.getLoanDisbursementTransferInfoDAO().update(disbursementTransferInfo);
									
									if (loanId!=null) {
										loanDAOFactory.getLoanDAO().updateDrawdownConfirmation(loanId, new Date(), "PULSA-QIMO");
									}
									
								}
							}	
						}
					}
				}
				
				i++;
			}		
			
			logger.info("[ LoanPrepaidTopUpTransferNotificationTask -- topUpNotification() --- END --- ]");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
