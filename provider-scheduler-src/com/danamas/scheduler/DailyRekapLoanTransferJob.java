package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class DailyRekapLoanTransferJob  implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		DailyRekapLoanTransferTask dailyRekapLoanTransferTask = new DailyRekapLoanTransferTask();
		dailyRekapLoanTransferTask.dailyRekapLoanTransfer();
		
	}

}
