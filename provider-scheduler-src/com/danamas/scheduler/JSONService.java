package com.danamas.scheduler;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.mpe.common.util.CommonUtil;

public class JSONService {
	
	static Logger logger = Logger.getLogger("JSONService");
	
	public static String updateTransaction(String path, int timeout, long loanId, Date paymentDate) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			System.setProperty("jsse.enableSNIExtension", "false");			
			doTrustToCertificates();
			URL url = new URL(path+"/json/TransactionRemoveJsonAction_update");
	        connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("loanId", loanId)
	        	.add("paymentDate", CommonUtil.getStringFromDate(paymentDate, "ddMMyyyy"))
	        	;
	        JsonObject attendanceObj = objectBuilder.build();
	        
	        logger.info(" req-json >> "+attendanceObj);
	        
	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
	        
	        //String s = IOUtils.toString(is);
	        //logger.info(" s >> "+s);
	        
			rdr = Json.createReader(is);
			JsonObject obj = rdr.readObject();
			
			//logger.info(" resp-json >> "+obj);
			
			String status = obj.getString("status");
			
			
			return status;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}
	
	public static String[] postTransferNotification(String path, int timeout, 
			String loanTransactionCode, String amount, String transferDateTime, String bankReferenceNumber) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			System.setProperty("jsse.enableSNIExtension", "false");			
			doTrustToCertificates();
			URL url = new URL(path);
	        connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("loanTransactionCode", loanTransactionCode)
	        	.add("amount", amount)
	        	.add("transferDateTime", transferDateTime)
	        	.add("bankReferenceNumber", bankReferenceNumber)
	        	;
	        JsonObject attendanceObj = objectBuilder.build();
	        
	        logger.info(" req-json >> "+attendanceObj);
	        
	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
	        
	        //String s = IOUtils.toString(is);
	        //logger.info(" s >> "+s);
	        
			rdr = Json.createReader(is);
			
			// TODO ==> kimo side ????
			JsonObject obj = rdr.readObject();
			
			logger.info(" resp-json >> "+obj);
			
			JsonObject obj2 = obj.getJsonObject("responseResult");
			JsonObject obj3 = obj2.getJsonObject("responseCode");
			
			String[] strObj = new String[2];
			strObj[0] = obj3.getString("code");
			
			strObj[1] = obj3.getString("code")+"-"+(obj3.getString("name")!=null ? obj3.getString("name") : "") +"-"+ (obj3.getString("description")!=null ? obj3.getString("description"):"");
			
			return strObj;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}
	
	/*
	 * fungsing top up saldo pulsa
	 * http://10.2.2.221:8080/danamas-pulsa/qimo/AccountJSONAction_addTopUpSaldo.action?
	 * accountId=122&
	 * balanceTopUpAmount=990000&
	 * bankReference=0001&
	 * topUpDateTime=2017082292200&
	 * accountCategory=BORROWER&
	 * loanTransactionCode=LO1708DD0002
	 */
	public static String[] postTransferNotificationAddTopUpSaldo(String path, int timeout, 
			long accountId, String balanceTopUpAmount, String bankReference, String topUpDateTime,
			String accountCategory, String loanTransactionCode) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			doTrustToCertificates();
			URL url = new URL(path);
	        connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("accountId", accountId)
	        	.add("balanceTopUpAmount", balanceTopUpAmount)
	        	.add("topUpDateTime", topUpDateTime)
	        	.add("bankReference", bankReference)
	        	.add("accountCategory", accountCategory)
	        	.add("loanTransactionCode", loanTransactionCode)
	        	;
	        JsonObject attendanceObj = objectBuilder.build();
	        
	        logger.info(" req-json >> "+attendanceObj);
	        
	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
			rdr = Json.createReader(is);
			
			// TODO ==> kimo side ????
			JsonObject obj = rdr.readObject();
			
			logger.info(" resp-json >> "+obj);
			
			JsonObject obj2 = obj.getJsonObject("responseResult");
			JsonObject obj3 = obj2.getJsonObject("responseCode");
			
			String[] strObj = new String[2];
			strObj[0] = obj3.getString("code");
			
			strObj[1] = obj3.getString("code")+"-"+(obj3.getString("name")!=null ? obj3.getString("name") : "") +"-"+ (obj3.getString("description")!=null ? obj3.getString("description"):"");
			
			return strObj;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}	
	
	public static String[] confirmLoan(String path, int timeout, 
			String loanTransactionCode, String amount, String email, String loanProductCode, String loanDate) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			doTrustToCertificates();
			URL url = new URL(path);
	        connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("loanTransactionCode", loanTransactionCode)
	        	.add("amount", amount)
	        	.add("email", email)
	        	.add("loanProductCode", loanProductCode)
	        	.add("loanDate", loanDate)
	        	;
	        JsonObject attendanceObj = objectBuilder.build();
	        
	        logger.info(" req-json >> "+attendanceObj);
	        
	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
			rdr = Json.createReader(is);
			
			// TODO ==> kimo side ????
			JsonObject obj = rdr.readObject();
			
			logger.info(" resp-json >> "+obj);
			
			JsonObject obj2 = obj.getJsonObject("responseResult");
			JsonObject obj3 = obj2.getJsonObject("responseCode");
			
			String[] strObj = new String[2];
			strObj[0] = obj3.getString("code");
			
			strObj[1] = obj3.getString("code")+"-"+(obj3.getString("name")!=null ? obj3.getString("name") : "") +"-"+ (obj3.getString("description")!=null ? obj3.getString("description"):"");
			
			return strObj;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}
	
	public static String[] postVirtualAccountPaymentNotification(String path, int timeout, 
			String loanTransactionCode, String amount, String paymentDateTime, String bankReferenceNumber, String virtualAccount, String paymentStatus, String outstandingAmount) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			System.setProperty("jsse.enableSNIExtension", "false");			
			doTrustToCertificates();
			URL url = new URL(path);
	        connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("loanTransactionCode", loanTransactionCode)
	        	.add("amount", amount)
	        	.add("paymentDateTime", paymentDateTime)
	        	.add("bankReferenceNumber", bankReferenceNumber)
	        	.add("virtualAccount", virtualAccount)
	        	.add("paymentStatus", paymentStatus)
	        	.add("outstandingAmount", outstandingAmount)
	        	;
	        JsonObject attendanceObj = objectBuilder.build();
	        
	        logger.info(" req-json >> "+attendanceObj);
	        
	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
			rdr = Json.createReader(is);
			
			//String textStream = IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8.name());
			//logger.info("textStream >> "+textStream);
			
			// TODO ==> kimo side ????
			JsonObject obj = rdr.readObject();
			
			logger.info(" resp-json >> "+obj);
			
			JsonObject obj2 = obj.getJsonObject("responseResult");
			JsonObject obj3 = obj2.getJsonObject("responseCode");
			
			String[] strObj = new String[2];
			strObj[0] = obj3.getString("code");
			
			strObj[1] = obj3.getString("code")+"-"+(obj3.getString("name")!=null ? obj3.getString("name") : "") +"-"+ (obj3.getString("description")!=null ? obj3.getString("description"):"");
			
			return strObj;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}
	
	public static String[] postOtherTransactionTransferNotification(String path, int timeout, 
			String otTransactionCode, String amount, String transferDateTime, String bankReferenceNumber) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			doTrustToCertificates();
			URL url = new URL(path);
	        connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("transactionCode", otTransactionCode)
	        	.add("paymentAmount", amount)
	        	.add("paymentDateTime", transferDateTime)
	        	.add("bankReffNo", bankReferenceNumber)
	        	;
	        JsonObject attendanceObj = objectBuilder.build();
	        
	        logger.info(" req-json >> "+attendanceObj);

	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
			rdr = Json.createReader(is);
			
			// TODO ==> kimo side ????
			JsonObject obj = rdr.readObject();
			
			logger.info(" resp-json >> "+obj);
			
			String[] strObj = new String[2];
			strObj[0] = obj.getString("responseCode");
			strObj[1] = obj.getString("responseCode")+"-"+obj.getString("responseDescription").substring(0, obj.getString("responseDescription").length() > 250 ? 250 : obj.getString("responseDescription").length());			
			return strObj;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}	
	
	public static String[] postDepositNotification(String pathReminder, int timeout, Object[] obj) throws Exception {
		InputStream is = null;
		JsonReader rdr = null;
		OutputStreamWriter out = null;
		URLConnection connection = null;
		try {
			doTrustToCertificates();
			URL url = null;
			url = new URL(pathReminder);
			
			connection = url.openConnection();
	        connection.setDoOutput(true); // Triggers POST
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        
	        JsonObjectBuilder objectBuilder_1 = Json.createObjectBuilder();
	        
	        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
	        objectBuilder
	        	.add("email", obj[0].toString())
	        	.add("when", obj[1].toString())
	        	.add("amount", obj[2].toString())
	        	.add("lang", obj[3].toString())
	        	;
	        
	        objectBuilder_1.add("request", objectBuilder);
	        
	        JsonObject attendanceObj = objectBuilder_1.build();
	        
	        out = new OutputStreamWriter(connection.getOutputStream());
	        out.write(attendanceObj.toString());
			out.close();
	        
	        is = connection.getInputStream();
			rdr = Json.createReader(is);
			
			// TODO ==> kimo side ????
			JsonObject objX = rdr.readObject();
			JsonObject obj2 = objX.getJsonObject("callback");
			JsonString obj3 = obj2.getJsonString("responseStatus");
			JsonString obj4 = obj2.getJsonString("responseMessage");
			
			String strObj[] = new String[2];
			strObj[0] = obj3.toString();
			strObj[1] = obj4.toString();
			
			return strObj;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (is!=null) {
				is.close(); is=null;
			}
			if (rdr!=null) {
				rdr.close(); rdr=null;
			}
		}
	}
	
	public static void doTrustToCertificates() throws Exception {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
					
					@Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                        return;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                        return;
                    }
                }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String urlHostName, SSLSession session) {
                if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                    System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
                }
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

}
