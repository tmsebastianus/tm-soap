package com.danamas.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tokomodal.account.model.Account;
import com.tokomodal.account.model.AccountType;
import com.tokomodal.account.model.Personal;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.account.model.other.Gender;
import com.tokomodal.loan.model.CashOut;
import com.tokomodal.loan.model.CashOutDisbursementTransferInfo;
import com.tokomodal.loan.model.Loan;
import com.tokomodal.loan.model.LoanDigisign;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.otherTransaction.model.dao.OtherTransactionDAOFactory;
import com.tokomodal.otherTransaction.model.dao.OtherTransactionDAOFactoryHibernate;
import com.tokomodal.otherTransaction.other.OtherTransactionTransferNotifyList;
import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.Lookup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;
import com.mpe.common.util.Constants;
import com.mpe.common.util.Formater;
import com.mpe.message.model.OutgoingEmail;
import com.mpe.message.model.dao.MessageDAOFactory;
import com.mpe.message.model.dao.MessageDAOFactoryHibernate;

public class OtherTransactionTransferNotificationTask {
	Logger logger = Logger.getLogger(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	OtherTransactionDAOFactory otherTransactionDAOFactory = OtherTransactionDAOFactory.instance(OtherTransactionDAOFactoryHibernate.class);
	MessageDAOFactory messageDAOFactory = MessageDAOFactory.instance(MessageDAOFactoryHibernate.class);
	
	public void sendTransferNotification() {
		try {
			logger.info("[ OtherTransactionTransferNotificationTask.sendTransferNotification() --- START --- ]");
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			
			List<OtherTransactionTransferNotifyList> otTransferNotifies = otherTransactionDAOFactory.getOtherTransactionDAO().findOtherTransactionByNotifyStatus(20);
				
			logger.info("<<<<sendTransferNotification::count notify : "+otTransferNotifies.size()+" >>>>");
			int i = 0;
			for (OtherTransactionTransferNotifyList otTransferNotify : otTransferNotifies) {
				i++;
				if (otTransferNotify.getStatus()!=null && otTransferNotify.getStatus().length()> 0 && 
				   (otTransferNotify.getStatus().startsWith("00") || otTransferNotify.getStatus().startsWith("d0"))) {
					
					String bankReferenceNumber = "";
					if (otTransferNotify.getStatus().startsWith("d0")) {
						String ct = otTransferNotify.getTransferReference().substring(otTransferNotify.getTransferReference().indexOf("~~"));
						String a = ct.substring(2,ct.indexOf("861")-1);
						bankReferenceNumber = a;
						
					} else if (otTransferNotify.getStatus().startsWith("00")) bankReferenceNumber = otTransferNotify.getTransferReference();
				
					String[] status = JSONService.postOtherTransactionTransferNotification(applicationSetup.getDanamasPrepaidServiceUrl()+"/qimo/PaymentSaldoLenderJSONAction_sentNotif.action", 90000, 
					otTransferNotify.getTransactionCode(), otTransferNotify.getAmount().toString(), 
					CommonUtil.getStringFromDate(otTransferNotify.getTransferCompleteDate(), "yyyyMMddHHmmss"), bankReferenceNumber);
			        logger.info(i+" bankReffNo : "+bankReferenceNumber+" paymentAmount : "+otTransferNotify.getAmount()+" paymentDateTime : "+otTransferNotify.getTransferCompleteDate() +" transactionCode : "+otTransferNotify.getTransactionCode());
					logger.info(" [ status => "+status[0]+" "+status[1]+" ]");
					if (status!=null && status[0].length()>0) {
						otherTransactionDAOFactory.getOtherTransactionDAO().updateOtherTransactionNotifyStatus(otTransferNotify.getOtherTransactionId(), status[1], new Date());
					}
				}	
			}
			
			logger.info("[ OtherTransactionTransferNotificationTask.sendTransferNotification() --- END --- ]");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendCashOutTransferNotification() {
		Session session = null;
		try {
			logger.info("[ OtherTransactionTransferNotificationTask.sendCashOutTransferNotification() --- START --- ]");
			session = accountDAOFactory.getPersonalDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			List<CashOutDisbursementTransferInfo> cashOutDisbursementTransferInfos = loanDAOFactory.getCashOutDisbursementTransferInfoDAO().findByCriteria(Order.asc("cashOutDisbursementTransferInfoId"),Restrictions.eq("isEmail", "F"),(Restrictions.eq("status", "d0")));
			for(CashOutDisbursementTransferInfo cashOutDisbursementTransferInfo : cashOutDisbursementTransferInfos) {
				CashOut cashOut = (CashOut)session.createCriteria(CashOut.class)
						.add(Restrictions.eq("cashOutDisbursementTransferInfoId", cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId()))
						.setMaxResults(1).uniqueResult();
				
				Account account = (Account)session.createCriteria(Account.class)
						.setFetchMode("address", FetchMode.JOIN)
						.setFetchMode("businessAddress", FetchMode.JOIN)
						.setFetchMode("contact", FetchMode.JOIN)
						.add(Restrictions.eq("accountId", cashOut.getAccountId()))
						.setMaxResults(1)
						.uniqueResult();
				
				Properties p = new Properties();
				ClassLoader loader = GiroBillerTask.class.getClassLoader();
			    URL r = loader.getResource("/");
			    String pathCls = r.getPath();
			    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
				p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
				Velocity.init(p);
				
				//BUAT PROD
				VelocityContext context = new VelocityContext();
				if (account.getType()==AccountType.PERSONAL) {
					Personal personal = (Personal)account;
					if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
					else context.put("salutation", "Ibu/Sdri.");
				} else {
					context.put("salutation", "");
				}
				
				context.put("name", account.getName());
				context.put("date", Formater.getFormatedDate(cashOut.getCashOutDate(), "dd MMM yyyy"));
				context.put("amount", Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()));
				Lookup lookup = basicDAOFactory.getLookupDAO().findById(cashOut.getBankId());
				context.put("bank", lookup.getName());
				if (!lookup.getName().equals("BANK SINARMAS")) context.put("adminFee", "5,000");
				else context.put("adminFee", "0");
				context.put("tanggalDanJamPengajuan",Formater.getFormatedDate(cashOut.getCreateOn(), "dd-NM-yyyy"));
				context.put("accountNumber", cashOut.getAccountNumber());
				
				StringWriter w = new StringWriter();
			    Velocity.mergeTemplate("/template/cashOut.vm", context, w );
			    
				// save to email
				OutgoingEmail outgoingEmail = new OutgoingEmail();
				outgoingEmail.setEmailDate(new Date());
				outgoingEmail.setSender(applicationSetup.getFromEmailAddress());
				outgoingEmail.setSubject("[Tokomodal] Konfirmasi Penarikan Dana");
				outgoingEmail.setTo(account.getContact().getEmail1());
				outgoingEmail.setMessage(w.toString());
				outgoingEmail.setType("[Tokomodal] Konfirmasi Penarikan Dana");
				
				messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail);
				
				cashOutDisbursementTransferInfo.setIsEmail("T");
				loanDAOFactory.getCashOutDisbursementTransferInfoDAO().update(cashOutDisbursementTransferInfo);
				
				/*
				 * SEND PUSH NOTIF
				 */
				if(account.getTokenFcm() != null && account.getTokenFcm().length() > 0) {
					PropertiesConfiguration config = new PropertiesConfiguration("ftp.properties");
					String appId = config.getString("appId");
					
					JSONObject body = new JSONObject();
					body.put("app_id", appId);
					
					JSONObject headings = new JSONObject();
					headings.put("id", "Penarikan Dana Berhasil!");
					headings.put("en", "Cash Out Success!");
					
					JSONObject contents = new JSONObject();
					if (!lookup.getName().equals("BANK SINARMAS")) {
						contents.put("id", "Penarikan dana sebesar ke " + lookup.getName() + " sebesar Rp. " + Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()) + " (diluar biaya admin) berhasil.");
						contents.put("en", "Penarikan dana sebesar ke " + lookup.getName() + " sebesar Rp. " + Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()) + " (diluar biaya admin) berhasil.");
//						contents.put("en", "Cash Out to " + lookup.getName() + " with amount Rp. " + Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()) + " (exclude admin fee) success.");
					}
					else {
						contents.put("id", "Penarikan dana sebesar Rp. " + Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()) + " berhasil.");
						contents.put("en", "Penarikan dana sebesar Rp. " + Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()) + " berhasil.");
//						contents.put("en", "Cash Out with amount Rp. " + Formater.getFormatedOutput(0, cashOut.getAmount().doubleValue()) + " success.");
					}
					
					body.put("headings", headings);
					body.put("contents", contents);
					body.put("small_icon", "tokomodal_logo");
					body.put("large_icon", "https://tokomodal.co.id/assets/img/logo-v.png");
//					body.put("included_segments", "Active Users");
					
					JSONArray includePlayerIds = new JSONArray();
//					includePlayerIds.put(account.getTokenFcm());
					includePlayerIds.put("4e002370-b7fa-4206-a218-5216ff709068");
					body.put("include_player_ids", includePlayerIds);
					body.put("android_group", "cash-out");
					
					JSONArray button = new JSONArray();
					
					JSONObject button1 = new JSONObject();
					button1.put("id", "goHome");
					button1.put("text", "Detail");
					
					button.put(button1);
					body.put("buttons", button);
					
					
					URL url = new URL("https://onesignal.com/api/v1/notifications");
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Content-Type", "application/json");
					conn.setRequestProperty("Authorization", "Basic MTJjNDEzNWMtMjcwMy00MWMzLTk5MDAtZjU2MWI1ZWQ4MmVk");
					conn.setDoOutput(true);
					
					String input = body.toString();
					
					OutputStream os = conn.getOutputStream();
					os.write(input.getBytes());
					os.flush();
					os.close();

					int responseCode = conn.getResponseCode();
					

					BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
					    response.append(inputLine);
					}
					in.close();
				}
			}
			
			
			
			logger.info("[ OtherTransactionTransferNotificationTask.sendCashOutTransferNotification() --- END --- ]");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) session.close();
		}
	}
	
	public void updateLoanFundingStatusWhileInStuck() {
		try {
			loanDAOFactory.getLoanDAO().updateLoanFundingStatusWhileInStuck("NEW", "FULL", "APPROVED");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void postToDigisign() {
		try {
			
//			List<LoanDigisign> loanDigisigns = loanDAOFactory.getLoanDigisignDAO().findByCriteria(Order.asc("loanDigisignId"), Restrictions.eq("isFunded", true), Restrictions.eq("isPostedDigisign", false));
			
//			loanDAOFactory.getLoanDAO().postToDigisign(loanDigisigns);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
