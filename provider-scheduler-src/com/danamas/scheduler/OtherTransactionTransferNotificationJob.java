package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class OtherTransactionTransferNotificationJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			OtherTransactionTransferNotificationTask otTransferNotificationTask = new OtherTransactionTransferNotificationTask();
			otTransferNotificationTask.sendTransferNotification();
			otTransferNotificationTask.sendCashOutTransferNotification();
			otTransferNotificationTask.updateLoanFundingStatusWhileInStuck();
			otTransferNotificationTask.postToDigisign();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
