package com.tokomodal.scheduler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.mpe.common.util.CommonUtil;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.soap.other.LoanTransferNotif;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class TestJson {
	
	static String ip;
	static String userName;
	static String userPass;
	static String dir;
	static String dirPencairan;
	static String dirPayment;
	
	static {
		try {
			Configurations configs = new Configurations();
			Configuration config = configs.properties(new File("ftp.properties"));
			ip = config.getString("ip");
			userName = config.getString("userName");
			userPass = config.getString("userPass");
			dir = config.getString("dir");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		
		LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
		
		//SFTPClient client = null;
		try {
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -2);
			
			// get data H-1
			List<LoanTransferNotif> loanTransferNotifs = loanDAOFactory.getLoanDAO().getLoanTransferNotifByDrawdownDate(cal.getTime());
			System.out.println(" loanTransferNotifs >> "+loanTransferNotifs.size());
			
			
			// create xls-file
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			WritableWorkbook workbook = Workbook.createWorkbook(baos);
			WritableSheet sheet = workbook.createSheet("First Sheet", 0);
			
			WritableCellFormat integerFormat = new WritableCellFormat (NumberFormats.INTEGER); 
			//WritableCellFormat floatFormat = new WritableCellFormat (NumberFormats.FLOAT); 
			NumberFormat decFmt = new NumberFormat("#,##0.00");
			WritableCellFormat numberFormat = new WritableCellFormat(decFmt);
			NumberFormat rateFmt = new NumberFormat("#,##0.0000");
			WritableCellFormat rateFormat = new WritableCellFormat(rateFmt);
			DateFormat customDateFormat = new DateFormat ("dd MMM yyyy");
			WritableCellFormat dateFormat = new WritableCellFormat(customDateFormat);
			
			WritableCellFormat cell = new WritableCellFormat();
			cell.setAlignment(Alignment.LEFT);
			
			Label label = null;
			//header tabel
			int start = 2 ;
			// sheet mergecell(colum,baris,colum,baris)
			label = new Label(0,start,"Tanggal", cell);
			sheet.addCell(label);
			
			label = new Label(1,start,"Nama OBA", cell);
			sheet.addCell(label);
			
			label = new Label(2,start,"No Member OBA", cell);
			sheet.addCell(label);			
			
			label = new Label(3,start,"No Loan", cell);
			sheet.addCell(label);
			
			label = new Label(4,start,"Nominal", cell);
			sheet.addCell(label);
			
			start++;
			
			for (LoanTransferNotif loanTransferNotif : loanTransferNotifs) {
				
				if (loanTransferNotif.getLoanDate()!=null){
					DateTime loanDate = new DateTime(0, start, loanTransferNotif.getLoanDate(), dateFormat);
					sheet.addCell(loanDate);
				} else {
					Label loanDate = new Label(0, start, "");
					sheet.addCell(loanDate);
				}
				
				Label obaName = new Label(1, start, loanTransferNotif.getName(), cell);
				sheet.addCell(obaName);
				
				Label obaId = new Label(2, start, loanTransferNotif.getObaId(), cell);
				sheet.addCell(obaId);
				
				Label loanNo = new Label(3, start, loanTransferNotif.getTransactionCode(), cell);
				sheet.addCell(loanNo);
				
				jxl.write.Number amount = new jxl.write.Number(4, start, loanTransferNotif.getAmount().doubleValue(), numberFormat);
				sheet.addCell(amount);	
				
				start++;
				
			}
			
			workbook.write();
			workbook.close();
			
			/*// connect to FTP
			int reply = 0;
			boolean changeDir = false;
			
			client = new SFTPClient();
			
			if(!client.connect(ip, userName, userPass, 22)) {
				client.disconnect();
				System.out.println("FTP server : "+ip+" refused connection.");
		    } else {
				
				// send data
				if (client.isConnected()) {
					changeDir = client.changeDir(dir);
					
					if (!changeDir) throw new Exception("Can not change working directory : "+dir);
					
					
					String fileName = "Recon_Tokomodal_Purchase_"+(CommonUtil.getStringFromDate(cal.getTime(), "yyyyMMdd"))+".xls";
					
					
					InputStream stream = new ByteArrayInputStream(baos.toByteArray());
					
					if (client.uploadFile(stream, fileName)) {
						System.out.println("copy file "+fileName+" success");
						stream.close();stream=null;
					}
					
		    		if (stream!=null) {
		    			stream.close(); stream=null;
		    		}
					
					
					//client.disconnect();
					//client = null;
					
				}
		    }*/
			
			boolean succ = false;
			
			while (!succ) {
				
				Session session = null;
		        Channel channel = null;
		        ChannelSftp channelSftp = null;
		        System.out.println("preparing the host information for sftp.");
		        try {
		            JSch jsch = new JSch();
		            session = jsch.getSession(userName, ip, 22);
		            jsch.setKnownHosts(System.getProperty("user.home")+"/.ssh/known_hosts");
		            session.setPassword(userPass);
		            java.util.Properties config = new java.util.Properties();
		            config.put("StrictHostKeyChecking", "no");
		            config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
		            session.setConfig(config);
		            session.setTimeout(5000);
		            session.connect();
		            System.out.println("Host connected.");
		            channel = session.openChannel("sftp");
		            channel.connect();
		            System.out.println("sftp channel opened and connected.");
		            channelSftp = (ChannelSftp) channel;
		            channelSftp.cd(dir);
		            
		            System.out.println("dir >> "+channelSftp.pwd());
		            
		            InputStream stream = new ByteArrayInputStream(baos.toByteArray());
		            String fileName = "Recon_Tokomodal_Purchase_"+(CommonUtil.getStringFromDate(cal.getTime(), "yyyyMMdd"))+".xls";
		            channelSftp.put(stream, fileName);
		            
		            if (stream!=null) {
		    			stream.close(); stream=null;
		    		}
		            
		            System.out.println("File transfered successfully to host.");
		            
		            succ = true;
		            
		        } catch (Exception ex) {
		        	succ = false;
		        	ex.printStackTrace();
		             System.out.println("Exception found while tranfer the response.");
		        } finally{

		            if (channelSftp!=null) channelSftp.exit();
		            System.out.println("sftp Channel exited.");
		            if (channel!=null) channel.disconnect();
		            System.out.println("Channel disconnected.");
		            if (session!=null) session.disconnect();
		            System.out.println("Host Session disconnected.");
		        }
		        
		        Thread.sleep(10000);
				
				//if (succ) break;
			}
			
			
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}

	}

}
