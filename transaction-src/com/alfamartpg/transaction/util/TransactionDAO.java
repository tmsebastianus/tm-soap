package com.alfamartpg.transaction.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class TransactionDAO {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	Driver driver = null;
	
	public TransactionDAO(String jdbcUrlH2H, String jdbcUsernameH2H, String jdbcPasswordH2H) {
		super();
		try {
			this.driver = (Driver)Class.forName("org.postgresql.Driver").newInstance();
			DriverManager.registerDriver (this.driver);
			this.connection = DriverManager.getConnection(jdbcUrlH2H,jdbcUsernameH2H,jdbcPasswordH2H); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
		try {
			if (this.rs!=null) this.rs.close();
			if (this.ps!=null) this.ps.close();
		} catch(Exception exx) {}
		if (this.connection!=null) {
			try {
				this.connection.close();
			} catch (Exception e) {}
			this.connection = null;
		}
		
		if (this.driver != null) {
            try {
                DriverManager.deregisterDriver(this.driver);
                System.out.println("Deregistering jdbc driver: " + this.driver);
            } catch (SQLException e) {
            	//System.out.println("Error deregistering jdbc driver: " + this.driver + " : "+e.getMessage());
            	e.printStackTrace();
            }
            this.driver = null;
        }
	}
	
	public Long getLoanIdTransactionInquiryPostgres(Long loanId) throws Exception {
		Long a = null;

		try {
			String sql = "select loan_id as loanId from transaction_inquiry where loan_id = ? ";
			
			ps = connection.prepareStatement(sql);
			ps.setLong(1, loanId);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				a = rs.getLong(1);
			}

			
		} catch(Exception exception) {
			exception.printStackTrace();
			try {
				if (connection!=null) connection.rollback();
			} catch(Exception exx){}
		} finally {
			try {
				if (rs!=null) rs.close();
				if (ps!=null) ps.close();
			} catch(Exception exx) {}
		}
		
		return a;
	}
	
	public void update(BigDecimal penalty, BigDecimal total, Long loanId) {
		try {
			String sql = "" +
					"update transaction_inquiry " +
					"set penalty = ?, total = ?, change_on = ?, change_by = ? " +
					"where loan_id = ? " +
					"";
			
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setBigDecimal(1, penalty);
			ps.setBigDecimal(2, total);
			ps.setDate(3, new java.sql.Date(new Date().getTime()));
			ps.setString(4, "SERVICE H2H");
			ps.setLong(5, loanId);
			
			ps.executeUpdate();
			connection.commit();
		} catch(Exception e) {
			e.printStackTrace();
			try {
				if (connection!=null) connection.rollback();
			} catch(Exception exx){}
		} finally {
			try {
				if (rs!=null) rs.close();
				if (ps!=null) ps.close();
			} catch(Exception exx) {}
		}
	}
	
	public void insert(Long accountId, BigDecimal amount, String accountNumber, String accountName, Long dueDate, String transactionCode, Long loanId, BigDecimal penalty, BigDecimal total, String virtualAccount, Long productId, String productCode) {
		try {
			String sql = ""
					+ "INSERT INTO transaction_inquiry( "
					+ "transaction_inquiry_id, account_id, agent_id, agent_store_id, "
					+ "agent_transaction_id, amount, change_by, change_on, commit_dt, "
					+ "commit_rc, commit_rd, commit_tc, company_id, create_by, create_on, "
					+ "customer_code, customer_name, due_date, loan_code, loan_id, payment_dt, "
					+ "payment_period, payment_rc, payment_rd, payment_tc, penalty, "
					+ "product_code, product_id, total, virtual_account) "
					+ "VALUES (nextval('transaction_inquiry_seq'), ?, null, null, "
					+ "null, ?, null, null, null, "
					+ "null, null, null, null, ?, ?, "
					+ "?, ?, ?, ?, ?, null, "
					+ "?, null, null, null, ?, "
					+ "?, ?, ?, ?); "
					+ "";
			
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(sql);
			ps.setLong(1, accountId);
			ps.setBigDecimal(2, amount);
			ps.setString(3, "SERVICE H2H");
			ps.setDate(4, new java.sql.Date(new Date().getTime()));
			ps.setString(5, accountNumber);
			ps.setString(6, accountName);
			ps.setDate(7, new java.sql.Date(dueDate));
			ps.setString(8, transactionCode);
			ps.setLong(9, loanId);
			ps.setString(10, "DAILY");
			ps.setBigDecimal(11, penalty);
			ps.setString(12, productCode);
			ps.setLong(13, productId);
			ps.setBigDecimal(14, total);
			ps.setString(15, virtualAccount);
			
			ps.executeUpdate();
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (connection!=null) connection.rollback();
			} catch(Exception exx){}
		} finally {
			try {
				if (rs!=null) rs.close();
				if (ps!=null) ps.close();
			} catch(Exception exx) {}
		}
	}
}
