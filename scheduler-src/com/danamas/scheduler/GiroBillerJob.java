package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class GiroBillerJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		try {
			
			GiroBillerTask giroBillerTask = new GiroBillerTask();
			giroBillerTask.insertCashIn();
			giroBillerTask.insertPaymentMultiLoan();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}  

}
