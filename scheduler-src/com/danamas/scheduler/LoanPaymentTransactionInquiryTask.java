package com.danamas.scheduler;

import java.util.List;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alfamartpg.transaction.util.TransactionDAO;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.loan.model.other.LoanList;

public class LoanPaymentTransactionInquiryTask {
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	
	public void transactionInquiry() {
		try {
			log.info(" <<<<<<<< START LoanPaymentTransactionInquiryTask "+this.getClass().getSimpleName()+" transactionInquiry() >>>>>>>>");
			
			List<LoanList> list = loanDAOFactory.getLoanDAO().findLoanPaymentTransactionInquiry();
			
			PropertiesConfiguration config = new PropertiesConfiguration("copyDB.properties");
			String jdbcUrlH2H = config.getString("jdbcUrlH2H");
			String jdbcUsernameH2H = config.getString("jdbcUsernameH2H");
			String jdbcPasswordH2H = config.getString("jdbcPasswordH2H");

			TransactionDAO transactionDao = new TransactionDAO(jdbcUrlH2H,jdbcUsernameH2H,jdbcPasswordH2H);
			
			for (LoanList row : list) {
				if (row!=null && row.getLoanId()>0) {
					Long loanId = transactionDao.getLoanIdTransactionInquiryPostgres(row.getLoanId());
					
					if (loanId!=null && loanId==row.getLoanId()) {
						transactionDao.update(row.getPenaltyAmount(), row.getAmount().add(row.getPenaltyAmount()), row.getLoanId());
					} else {
						transactionDao.insert(row.getAccountId(), row.getAmount(), row.getAccountCode(), row.getAccountName(), row.getOverDueDate().getTime(), row.getTransactionCode(), row.getLoanId(), row.getPenaltyAmount(), row.getAmount().add(row.getPenaltyAmount()), row.getVirtualAccount(), row.getProductId(), row.getProduct());
					}

				}
			}
			
			transactionDao.closeConnection();			
			
			log.info(" <<<<<<<< END LoanPaymentTransactionInquiryTask "+this.getClass().getSimpleName()+" transactionInquiry() >>>>>>>>");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
