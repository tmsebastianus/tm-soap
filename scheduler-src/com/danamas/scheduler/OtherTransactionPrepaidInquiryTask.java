package com.danamas.scheduler;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.otherTransaction.model.OtherTransaction;
import com.tokomodal.otherTransaction.model.dao.OtherTransactionDAOFactory;
import com.tokomodal.otherTransaction.model.dao.OtherTransactionDAOFactoryHibernate;
import com.tokomodal.otherTransaction.other.OtherTransactionStatus;
import com.tokomodal.otherTransaction.other.OtherTransactionType;
import com.tokomodal.prepaid.other.PrepaidJSONService;
import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;

public class OtherTransactionPrepaidInquiryTask {
	
	Logger logger = Logger.getLogger(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	OtherTransactionDAOFactory otherTransactionDAOFactory = OtherTransactionDAOFactory.instance(OtherTransactionDAOFactoryHibernate.class);
	
	public void inquiry() {
		try {
			
			logger.info(" ************ "+this.getClass().getName()+" *** inquiry() *** START ********** ");
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			if (applicationSetup.getDanamasPrepaidServiceUrl()!=null && applicationSetup.getDanamasPrepaidServiceUrl().length()>0) {
				List<OtherTransaction> otherTransactions = otherTransactionDAOFactory.getOtherTransactionDAO().findByCriteria(Order.asc("otherTransactionId"), Restrictions.eq("otherTransactionType", OtherTransactionType.PREPAID_TRANSACTION), Restrictions.or(Restrictions.eq("otherTransactionStatus", OtherTransactionStatus.OPEN), Restrictions.eq("otherTransactionStatus", OtherTransactionStatus.PENDING)));
				logger.info(" ************ inquirySize >>>>> "+ otherTransactions.size() +" ********** ");
				for (OtherTransaction otherTransaction : otherTransactions) {
					
					// inquiry
					String[] s = PrepaidJSONService.inquiryTransaction(applicationSetup.getDanamasPrepaidServiceUrl(), 90000, otherTransaction.getAccountId(), otherTransaction.getTransactionCode());
					if (s!=null) {
						if (s[0]!=null && s[0].equalsIgnoreCase("0")) {
							// fail
							otherTransaction.setOtherTransactionStatus(OtherTransactionStatus.FAIL);
							otherTransaction.setReversal(true);
							otherTransaction.setReversalDateTime(new Date());
						} else if (s[0]!=null && s[0].equalsIgnoreCase("1")) {
							// success
							otherTransaction.setOtherTransactionStatus(OtherTransactionStatus.SUCCESS);
						} else if (s[0]!=null && s[0].equalsIgnoreCase("2")) {
							// pending
							otherTransaction.setOtherTransactionStatus(OtherTransactionStatus.PENDING);
						} else if (s[0]!=null && s[0].equalsIgnoreCase("13")) {
							// biller error
							otherTransaction.setOtherTransactionStatus(OtherTransactionStatus.PENDING);
						} else {
							otherTransaction.setOtherTransactionStatus(OtherTransactionStatus.FAIL);
							otherTransaction.setReversal(true);
							otherTransaction.setReversalDateTime(new Date());
						}
						// update
						otherTransactionDAOFactory.getOtherTransactionDAO().update(otherTransaction);
					}
				}
			}
			
			
			logger.info(" ************ "+this.getClass().getName()+" *** inquiry() *** END ********** ");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
