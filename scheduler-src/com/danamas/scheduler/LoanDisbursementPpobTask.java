package com.danamas.scheduler;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.Account;
import com.tokomodal.account.model.BankAccount;
import com.tokomodal.account.model.PrepaidProvider;
import com.tokomodal.account.model.PrepaidReseller;
//import com.tokomodal.account.model.Rdp2pBankAccount;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.account.model.other.PrepaidProviderTransfer;
import com.tokomodal.investment.model.InvestmentProduct;
import com.tokomodal.investment.model.InvestmentRedeem;
import com.tokomodal.investment.model.InvestmentRedeemDisbursementTransferInfo;
import com.tokomodal.investment.model.dao.InvestmentDAOFactory;
import com.tokomodal.investment.model.dao.InvestmentDAOFactoryHibernate;
//import com.tokomodal.loan.model.AccountBalanceHistory;
import com.tokomodal.loan.model.BankMethod;
import com.tokomodal.loan.model.CashIn;
//import com.tokomodal.loan.model.CashInDisbursementTransferInfoRdp2p;
import com.tokomodal.loan.model.CashInStatus;
import com.tokomodal.loan.model.CashOut;
import com.tokomodal.loan.model.CashOutDisbursementTransferInfo;
//import com.tokomodal.loan.model.CashOutDisbursementTransferInfoRdp2p;
import com.tokomodal.loan.model.CashOutStatus;
import com.tokomodal.loan.model.DisbursementStatus;
import com.tokomodal.loan.model.InterestType;
import com.tokomodal.loan.model.KewajibanLain;
import com.tokomodal.loan.model.KlDisbursementTransferInfo;
import com.tokomodal.loan.model.Loan;
import com.tokomodal.loan.model.LoanDisbursementTransferInfo;
//import com.tokomodal.loan.model.LoanDisbursementTransferInfoRdp2p;
import com.tokomodal.loan.model.LoanDrawdown;
import com.tokomodal.loan.model.LoanFunding;
import com.tokomodal.loan.model.LoanFundingStatus;
import com.tokomodal.loan.model.LoanPaymentStatus;
import com.tokomodal.loan.model.LoanProduct;
import com.tokomodal.loan.model.LoanProductThirdParty;
import com.tokomodal.loan.model.Payment;
import com.tokomodal.loan.model.PaymentDetail;
//import com.tokomodal.loan.model.PaymentDetailDisbursementTransferInfoRdp2p;
import com.tokomodal.loan.model.PaymentStatus;
import com.tokomodal.loan.model.TenorUnit;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
//import com.tokomodal.loan.model.other.AccountBalanceHistoryTransactionType;
import com.tokomodal.loan.model.other.BankDirectTransferType;
import com.mpe.amortization.model.LoanAmortization;
import com.mpe.amortization.model.dao.AmortizationDAOFactory;
import com.mpe.amortization.model.dao.AmortizationDAOFactoryHibernate;
import com.mpe.amortization.util.AmortizationBean;
import com.mpe.amortization.util.AmortizationCalculator;
import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.Lookup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;
import com.mpe.common.util.Formater;

public class LoanDisbursementPpobTask {
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	AmortizationDAOFactory amortizationDAOFactory = AmortizationDAOFactory.instance(AmortizationDAOFactoryHibernate.class);
	InvestmentDAOFactory investmentDAOFactory = InvestmentDAOFactory.instance(InvestmentDAOFactoryHibernate.class);

	public void processAm() {
		try {
			log.info(" <<<<<<<< START LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processAm() >>>>>>>>");
			
			// bank danamas!
			BankAccount danamasBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
			
			List<InvestmentRedeem> investmentRedeems = investmentDAOFactory.getInvestmentRedeemDAO().findByCriteria(
					Order.asc("investmentRedeemId"),
					Restrictions.isNull("accountId"),
					Restrictions.isNotNull("sentDateTime"), 
					//Restrictions.isNotNull("confirmDateTime"), // karena begitu data dikirim am minta ditransfer dana
					Restrictions.isNull("irDisbursTransferInfoId"),
					Restrictions.eq("transactionType", "1"),
					Restrictions.gt("amount", BigDecimal.ZERO));
			
			int totLoan = investmentRedeems.size()>0 ? investmentRedeems.size() : 0;
			log.info("AssetManagement count() >> "+totLoan);
			Session session = null; 
			Transaction transaction = null;
			Long idInfo = null; 
			InvestmentRedeemDisbursementTransferInfo cekDataIr;
			for (InvestmentRedeem ir : investmentRedeems) {
				
				cekDataIr = investmentDAOFactory.getInvestmentRedeemDisbursementTransferInfoDAO().findByCriteria(Restrictions.eq("accountCreditCode", ir.getTransactionCode()));
				if (cekDataIr!=null) {
					log.info("AWAS sudah ada data ir : "+ir.getTransactionCode());
					investmentDAOFactory.getInvestmentRedeemDAO().updateIrDisbursementTransferInfoId(ir.getInvestmentRedeemId(), cekDataIr.getIrDisbursementTransferInfoId());
				} else {
					// new session
					session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
					try {
						// start transaction
						transaction = session.beginTransaction(); 
					
						if (ir.getSentDateTime() != null && ir.getTransactionType().equalsIgnoreCase("1") &&
						   (ir.getAmount() != null && ir.getAmount().compareTo(new BigDecimal(0)) >0)) {
							
							InvestmentRedeemDisbursementTransferInfo irDisbursementTransferInfo = new InvestmentRedeemDisbursementTransferInfo();
							irDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
							irDisbursementTransferInfo.setAccountDebitCode("TMU");
							irDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
							irDisbursementTransferInfo.setAmount(ir.getAmount());
							
							InvestmentProduct investmentProduct = investmentDAOFactory.getInvestmentProductDAO().findById(ir.getInvestmentProductId(), session);
							if (investmentProduct != null) {
								
								// sementara hanya yang bank sinarmas
								Lookup lookup = basicDAOFactory.getLookupDAO().findById(investmentProduct.getBank().getLookupId(), session);
								//System.out.println(lookup.getCode());
								if (lookup !=null && lookup.getCode().trim().startsWith("153")) {
									
									irDisbursementTransferInfo.setAccountCredit(investmentProduct.getAccountNumber());
									irDisbursementTransferInfo.setAccountCreditCode(ir.getTransactionCode());
									irDisbursementTransferInfo.setAccountCreditName(investmentProduct.getAccountName());
									
								}	
								
							}
							
							// make sure only account_credit is not null are saved
							if (irDisbursementTransferInfo.getAccountCredit()!=null && irDisbursementTransferInfo.getAccountCredit().length()>0) {
								
								investmentDAOFactory.getInvestmentRedeemDisbursementTransferInfoDAO().save(irDisbursementTransferInfo, session);
								idInfo = irDisbursementTransferInfo.getIrDisbursementTransferInfoId();
								
								//ir.setIrDisbursTransferInfoId(irDisbursementTransferInfo.getIrDisbursementTransferInfoId());
								//investmentDAOFactory.getInvestmentRedeemDAO().update(ir);
								
								investmentDAOFactory.getInvestmentRedeemDAO().updateIrDisbursementTransferInfoId(ir.getInvestmentRedeemId(), idInfo, session);
								
							}
							
						}
						
						transaction.commit();
						
					} catch (Exception ex) {
						if (transaction!=null) transaction.rollback();
						ex.printStackTrace();
					} finally {
						if (session!=null) {
							session.close(); session=null;
						}
					}
					
					// ambil ir_disburs_transfer_info_id
					log.info("irId: "+ir.getInvestmentRedeemId()+" idInfo: "+idInfo);
					if (idInfo!=null && idInfo>0) {
						investmentDAOFactory.getInvestmentRedeemDAO().updateIrDisbursementTransferInfoId(ir.getInvestmentRedeemId(), idInfo);
						idInfo = null;
					} else {
						log.info("=== investmentRedeem : "+ir.getInvestmentRedeemId()+" tc : "+ir.getTransactionCode()+" idInfo null!!!");
					}
				
				}
				
			}
			
			log.info(" <<<<<<<< END of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processAm() >>>>>>>>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void processKl() {
		try {
			
			log.info(" <<<<<<<< START LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processKl() >>>>>>>>");
			
			// bank danamas!
			BankAccount danamasBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
			
			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
			List<Long> kewajibanLainIds = loanDAOFactory.getKewajibanLainDAO().getKewajibanLainIdByNullCloseDateTime();
			BankAccount bankAccount = null;
			int totLoan = kewajibanLainIds.size()>0 ? kewajibanLainIds.size() : 0;
			log.info("kewajiban-lain count() >> "+totLoan);
			for (Long kewajibanLainId : kewajibanLainIds) {
				
				KewajibanLain kewajibanLain = loanDAOFactory.getKewajibanLainDAO().findById(kewajibanLainId);
				// only for funding_status is TRANSFERED
				if (kewajibanLain.getAmount() != null && kewajibanLain.getAmount().compareTo(new BigDecimal(0)) >0) {
					
					// search on table PrepaidReseller, for now prepaidReseller only
					//LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findByCriteria(Restrictions.eq("loanProductId", loan.getProductId()));
							
					KlDisbursementTransferInfo klDisbursementTransferInfo = new KlDisbursementTransferInfo(); 
					klDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
					klDisbursementTransferInfo.setAccountDebitCode("TMU");
					klDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
					klDisbursementTransferInfo.setAmount(kewajibanLain.getAmount());
					
					//if (loanProduct.isPrepaidReseller()) {
						
						PrepaidReseller prepaidReseller = accountDAOFactory.getPrepaidResellerDAO().findByCriteria(Restrictions.eq("accountId", kewajibanLain.getAccountId()));
						if (prepaidReseller != null) {
							PrepaidProvider prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findByCriteria(Restrictions.eq("prepaidProviderId", prepaidReseller.getProvider().getPrepaidProviderId()));
							if (prepaidProvider != null) {
								
								// rek prepaid-provider! or borrower
								bankAccount = null;
								/*if (prepaidProvider.getBankTransfer() != null && prepaidProvider.getBankTransfer() == true) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("prepaidProviderId", prepaidProvider.getPrepaidProviderId()));
								else bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("accountId", loan.getAccountId()));*/
								if (prepaidProvider.getTransfer()==PrepaidProviderTransfer.PROVIDER_BANK_ACCOUNT) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("prepaidProviderId", prepaidProvider.getPrepaidProviderId()));
								else if (prepaidProvider.getTransfer()==PrepaidProviderTransfer.BORROWER_BANK_ACCOUNT) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("accountId", kewajibanLain.getAccountId()));
								// TODO 
								// VA
								
								if (bankAccount != null) {
									
									// sementara hanya yang bank sinarmas
									Lookup lookup = basicDAOFactory.getLookupDAO().findById(bankAccount.getBank().getLookupId());
									//System.out.println(lookup.getCode());
									if (lookup !=null && lookup.getCode().trim().startsWith("153")) {
										
										klDisbursementTransferInfo.setAccountCredit(bankAccount.getAccountNumber());
										klDisbursementTransferInfo.setAccountCreditCode(CommonUtil.getStringFromDate(kewajibanLain.getPaymentDateTime(), "yyMMddHHmmss"));
										klDisbursementTransferInfo.setAccountCreditName(bankAccount.getAccountName());
										
									}	
								}
							}
						}
					//}
					
					// for general user
					
					// make sure only account_credit is not null are saved
					if (klDisbursementTransferInfo.getAccountCredit()!=null && klDisbursementTransferInfo.getAccountCredit().length()>0) {
						
						loanDAOFactory.getKlDisbursementTransferInfoDAO().save(klDisbursementTransferInfo);
						
						kewajibanLain.setKlDisbursementTransferInfoId(klDisbursementTransferInfo.getKlDisbursementTransferInfoId());
						//kewajibanLain.setCloseDateTime(new Date());
						//kewajibanLain.setCloseNote("AUTO CREDIT BY SERVICE");
						loanDAOFactory.getKewajibanLainDAO().update(kewajibanLain);
						
					}
				}	
				
			}
			
			log.info(" <<<<<<<< END of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processPpob() >>>>>>>>");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	
	
	public void processPpob() {
		try {
			log.info(" <<<<<<<< START LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processPpob() >>>>>>>>");
			
			// bank danamas!
			BankAccount danamasBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));

			// validation for loan.amount == sum(loan_funding.amount) or must be 100%
			List<Loan> loanValids = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), 
					Restrictions.eq("fundingStatus", LoanFundingStatus.FULL),
					Restrictions.isNull("loanDisbursementTransferInfoId"));
			
			BigDecimal totFundingAmount;			
			boolean z = false;
			int amountCompare = 0;
			for (Loan loan : loanValids) {
				
				amountCompare = loan.getFundingAmount().compareTo(loan.getAmount());
				z = false;
				
				if (loan.getFundingAmount() != null && amountCompare != 0) {
					
					// jika lebih
					if (amountCompare == 1) {
						// delete semua loan-funding! yg bikin ancur / id >= start id ancur
						totFundingAmount = BigDecimal.ZERO;
						List<LoanFunding> loanFundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
						
						for (LoanFunding loanFundingX : loanFundings) {
							totFundingAmount = totFundingAmount.add(loanFundingX.getAmount());
							
							if (z == false) {
								
								// cek apakah sudah over funding
								if (totFundingAmount.compareTo(loan.getAmount())==1) z = true;
								
							}
							
							if (z == true) {
								// delete semua saja funding yang mulai rusak 
								loanDAOFactory.getLoanFundingDAO().delete(loanFundingX.getLoanFundingId());
							}
							
						}
						
						// biar aman
						Loan lastLoan = loanDAOFactory.getLoanDAO().findById(loan.getLoanId());
						
						// jika loan.fundingAmount < loan.amount maka, update loan.funding_status = NEW
						// jika loan.fundingAmount = loan.amount maka, status loan.funding_status tetap FULL atau tidak diupdate
						if (lastLoan.getFundingAmount().compareTo(loan.getAmount()) == -1) {
							
							Loan ll = loanDAOFactory.getLoanDAO().findById(loan.getLoanId());
							ll.setFundingStatus(LoanFundingStatus.NEW);
							loanDAOFactory.getLoanDAO().update(ll);
							
						}
						
					} else if (amountCompare == -1) {
						
						// jika kurang
						Loan ll = loanDAOFactory.getLoanDAO().findById(loan.getLoanId());
						
						ll.setFundingStatus(LoanFundingStatus.NEW);
						loanDAOFactory.getLoanDAO().update(ll);
					}
					
				}
				
			}	
			
			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
			List<Loan> loans = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), 
					Restrictions.eq("fundingStatus", LoanFundingStatus.FULL),
					Restrictions.isNull("loanDisbursementTransferInfoId"));
			
			BankAccount bankAccount = null;
			int totLoan = loans.size()>0 ? loans.size() : 0;
			log.info("loan count() >> "+totLoan);
			Session session = null; 
			Transaction transaction = null;
			Long idInfo = null; 
			for (Loan loan : loans) {
				// pengecekan sebelum insert, pastikan tidak ada data loan.getTransactionCode() yg double
				LoanDisbursementTransferInfo cekData = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findByCriteria(Restrictions.eq("accountCreditCode", loan.getTransactionCode()));
				log.info("cekData : "+cekData);
				if (cekData != null) {
					
					log.info("AWAS sudah ada data loan : "+loan.getTransactionCode());
					loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoId(loan.getLoanId(), cekData.getLoanDisbursementTransferInfoId());
					log.info("Update loan::loanId > "+loan.getLoanId()+" with infoId > "+cekData.getLoanDisbursementTransferInfoId());
					
				} else {
					// new session
					session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
					try {
						// start transaction
						transaction = session.beginTransaction(); 
								
						// only for funding_status is TRANSFERED
						if (loan.getFundingStatus()==LoanFundingStatus.FULL && (loan.getTotal() != null && loan.getTotal().compareTo(new BigDecimal(0)) >0)) {
							
							// search on table PrepaidReseller, for now prepaidReseller only
							LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findByCriteria(session, Restrictions.eq("loanProductId", loan.getProductId()));
									
							LoanDisbursementTransferInfo loanDisbursementTransferInfo = new LoanDisbursementTransferInfo(); 
							loanDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
							loanDisbursementTransferInfo.setAccountDebitCode("TMU");
							loanDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
							loanDisbursementTransferInfo.setAmount(loan.getTotal());
							log.info("loanId : "+loan.getLoanId()+" productId : "+loan.getProductId()+" >> "+loanProduct.isPrepaidReseller());
							if (loanProduct.isPrepaidReseller()) {
								
								PrepaidReseller prepaidReseller = accountDAOFactory.getPrepaidResellerDAO().findByCriteria(session, Restrictions.eq("accountId", loan.getAccountId()));
								if (prepaidReseller != null) {
									PrepaidProvider prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findByCriteria(session, Restrictions.eq("prepaidProviderId", prepaidReseller.getProvider().getPrepaidProviderId()));
									if (prepaidProvider != null) {
										
										// rek prepaid-provider! or borrower
										bankAccount = null;
										/*if (prepaidProvider.getBankTransfer() != null && prepaidProvider.getBankTransfer() == true) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("prepaidProviderId", prepaidProvider.getPrepaidProviderId()));
										else bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("accountId", loan.getAccountId()));*/
										if (prepaidProvider.getTransfer()==PrepaidProviderTransfer.PROVIDER_BANK_ACCOUNT) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(session, Restrictions.eq("prepaidProviderId", prepaidProvider.getPrepaidProviderId()));
										else if (prepaidProvider.getTransfer()==PrepaidProviderTransfer.BORROWER_BANK_ACCOUNT) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(session, Restrictions.eq("accountId", loan.getAccountId()));
										// TODO 
										// VA
										if (bankAccount != null) {
											
											// sementara hanya yang bank sinarmas
											Lookup lookup = basicDAOFactory.getLookupDAO().findById(bankAccount.getBank().getLookupId(), session);
											log.info(lookup.getCode());
											if (lookup !=null) {
												// bank sinarmas
												if (lookup.getCode().trim().startsWith("153")) {
													loanDisbursementTransferInfo.setAccountCredit(bankAccount.getAccountNumber());
													loanDisbursementTransferInfo.setAccountCreditCode(loan.getTransactionCode());
													loanDisbursementTransferInfo.setAccountCreditName(bankAccount.getAccountName());
													loanDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
													loanDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
												} else {
													// non bank sinarmas
													if (lookup.getMemberCode() != null) {
														loanDisbursementTransferInfo.setAccountCredit(bankAccount.getAccountNumber());
														loanDisbursementTransferInfo.setAccountCreditCode(loan.getTransactionCode());
														loanDisbursementTransferInfo.setAccountCreditName(bankAccount.getAccountName());
														loanDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.EXTERNAL);
														loanDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
													}
												}
											}	
										}
									}
								}
								
							} else {
							
								// for general user
								// product pulsa
								LoanProductThirdParty loanProductThirdParty = loanDAOFactory.getLoanProductThirdPartyDAO().findByCriteria(session, Restrictions.eq("loanProductId", loan.getProductId()));
								
								log.info("loanProduct3Party : "+loanProductThirdParty);
								if (loanProductThirdParty!=null) {

									Lookup lookup = basicDAOFactory.getLookupDAO().findById(loanProductThirdParty.getKimoTopUpServiceBank().getLookupId(), session);
									log.info(lookup.getCode());
									if (lookup !=null && lookup.getCode().trim().startsWith("153")) {
										
										loanDisbursementTransferInfo.setAccountCredit(loanProductThirdParty.getKimoTopUpServiceAccountNumber());
										loanDisbursementTransferInfo.setAccountCreditCode(loan.getTransactionCode());
										loanDisbursementTransferInfo.setAccountCreditName(loanProductThirdParty.getKimoTopUpServiceAccountName());
										
									}
								}
								
							}	
							
							// make sure only account_credit is not null are saved
							if (loanDisbursementTransferInfo.getAccountCredit()!=null && loanDisbursementTransferInfo.getAccountCredit().length()>0) {
								
								loanDAOFactory.getLoanDisbursementTransferInfoDAO().save(loanDisbursementTransferInfo, session);
								idInfo = loanDisbursementTransferInfo.getLoanDisbursementTransferInfoId();
								
								//loan.setLoanDisbursementTransferInfoId(loanDisbursementTransferInfo.getLoanDisbursementTransferInfoId());
								//loanDAOFactory.getLoanDAO().update(loan, session);
								log.info("loanId >>> "+loan.getLoanId()+ " idInfo >>> "+idInfo);
								loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoId(loan.getLoanId(), idInfo, session);
								
							}
						}	
							
						transaction.commit();
						
					} catch (Exception ex) {
						if (transaction!=null) transaction.rollback();
						ex.printStackTrace();
					} finally {
						if (session!=null) {
							session.close(); session=null;
						}
					}
					
					// ambil loan_transfer_info
					log.info("loanId: "+loan.getLoanId()+" idInfo: "+idInfo);
					if (idInfo!=null && idInfo>0) {
						loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoId(loan.getLoanId(), idInfo);
						idInfo = null;
					} else {
						log.info("=== loan : "+loan.getLoanId()+" tc : "+loan.getTransactionCode()+" idInfo null!!!");
					}
					
				} //
				
			}
			
			log.info(" <<<<<<<< END of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processPpob() >>>>>>>>");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void drawdownLoanAfterPpob() {
		try {
			
			log.info(" <<<<<<<< START of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" drawdownLoanAfterPpob() >>>>>>>>");
			
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			List<Long> loanIds = loanDAOFactory.getLoanDAO().findLoanIdUnDrawdown(10);
			for (Long loanId : loanIds) {
				// get loan - by ID
				Loan loan = loanDAOFactory.getLoanDAO().findById(loanId);
				Long prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(loan.getAccountId());
				String prepaidProviderName = null;
				if (prepaidProviderId!=null) prepaidProviderName = accountDAOFactory.getPrepaidProviderDAO().getProviderNameByPrepaidProviderId(prepaidProviderId);
				LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findById(loan.getProductId());
				
				/*AccountBalanceHistory*/
//				Long accountBalanceHistoryId = new Long(0);
				
				if (loan.getFundingStatus()==LoanFundingStatus.FULL && loan.getInterest().compareTo(new BigDecimal(0))>0) {

					/*
					 * added 23.02.2017 by giant
					 * for amortization
					 */
					if (loanProduct.getInstallment() != null && loanProduct.isInstallment() == Boolean.TRUE) {
						
						List<AmortizationBean> c = AmortizationCalculator.calAmor(loan.getAmount(), loan.getInterest(), loan.getTenor(), new Date(), "FLAT", loan.getType().name());					
						log.info(" c amort size > "+c.size());
						
						BigDecimal paymentAmount1st = BigDecimal.ZERO;
						BigDecimal principalPayment = BigDecimal.ZERO;
						BigDecimal interestPayment = BigDecimal.ZERO;
						if (loan.getType()==InterestType.ARREAR) {
							
							for (AmortizationBean row : c) {
								LoanAmortization loanAmortization = new LoanAmortization();
								loanAmortization.setLoanId(loan.getLoanId());
								loanAmortization.setCounter(row.getCounter());
								loanAmortization.setDueDate(row.getDueDate());
								loanAmortization.setPreviousBalance(row.getBeginBalance());
								loanAmortization.setEndBalance(row.getEndBalance());
								loanAmortization.setPrincipal(row.getPrincipal());
								loanAmortization.setInterest(row.getInterest());
								loanAmortization.setPaymentDate(row.getPaymentDate());
								loanAmortization.setPaymentAmount(BigDecimal.ZERO);
								loanAmortization.setPenalty(BigDecimal.ZERO);
								loanAmortization.setPaymentPenalty(BigDecimal.ZERO);
								loanAmortization.setPrincipalPayment(BigDecimal.ZERO);
								loanAmortization.setInterestPayment(BigDecimal.ZERO);
								
								amortizationDAOFactory.getLoanAmortizationDAO().save(loanAmortization);
							}
							
						} else if (loan.getType()==InterestType.ADVANCE) {
							
							for (AmortizationBean row : c) {
								
								if (row.getCounter()==1) {
									paymentAmount1st = row.getPrincipal().add(row.getInterest());
									principalPayment = row.getPrincipal();
									interestPayment = row.getInterest();
								}
								
								LoanAmortization loanAmortization = new LoanAmortization();
								loanAmortization.setLoanId(loan.getLoanId());
								loanAmortization.setCounter(row.getCounter());
								loanAmortization.setDueDate(row.getDueDate());
								loanAmortization.setPreviousBalance(row.getBeginBalance());
								loanAmortization.setEndBalance(row.getEndBalance());
								loanAmortization.setPrincipal(row.getPrincipal());
								loanAmortization.setInterest(row.getInterest());
								loanAmortization.setPaymentDate(row.getPaymentDate());
								loanAmortization.setPaymentAmount(BigDecimal.ZERO);
								loanAmortization.setPenalty(BigDecimal.ZERO);
								loanAmortization.setPaymentPenalty(BigDecimal.ZERO);
								loanAmortization.setPrincipalPayment(BigDecimal.ZERO);
								loanAmortization.setInterestPayment(BigDecimal.ZERO);
								
								amortizationDAOFactory.getLoanAmortizationDAO().save(loanAmortization);
							}
						
						// payment for 1st rental
						// if (loan.getType()==InterestType.ADVANCE) {
							
							Calendar curr = new GregorianCalendar();
							curr.set(Calendar.HOUR_OF_DAY, 0);
							curr.set(Calendar.MINUTE, 0);
							curr.set(Calendar.SECOND, 0);
							curr.set(Calendar.MILLISECOND, 0);
							
							Payment payment = new Payment();
							payment.setGiroBillerId(null);
							BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
							if (bankAccount!=null) {
								payment.setBankId(bankAccount.getBank().getLookupId());
								payment.setAccountName(bankAccount.getAccountName());
								payment.setAccountNumber(bankAccount.getAccountNumber());
								payment.setToBankAccountId(bankAccount.getBankAccountId());
							}
							
							payment.setAmount(paymentAmount1st);
							payment.setPrincipalPayment(principalPayment);
							payment.setInterestPayment(interestPayment);
							payment.setTotalPaymentAmount(paymentAmount1st);
							
							payment.setCreateBy("H2H-KIMO-1stPAYMENT");
							payment.setCreateOn(new Date());
							payment.setMethod(BankMethod.ATM);
							payment.setPaymentDate(curr.getTime());
							payment.setPaymentDateTime(new Date());
							payment.setStatus(PaymentStatus.CONFIRMED);
							Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
							String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("PAYMENT", l, payment.getPaymentDate(), true);
							payment.setTransactionCode(s);		
							
							// distribute to lender
							List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
							for (LoanFunding funding :fundings) {
								PaymentDetail paymentDetail = new PaymentDetail();
								paymentDetail.setPayment(payment);
								// pokok saja
								paymentDetail.setAmount((payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount(),2,BigDecimal.ROUND_HALF_UP));
								paymentDetail.setDeductionAmount(new BigDecimal(0));
								paymentDetail.setLenderId(funding.getLenderId());
								paymentDetail.setLoanFundingId(funding.getLoanFundingId());
								paymentDetail.setNote("Cicilan Pokok ke 1 ("+loan.getTransactionCode()+" a/n "+loan.getAccountName()+")");
								//
								payment.getPaymentDetils().add(paymentDetail);
							}
							
							payment.setUploadFileId(null);
							
							// find rental angsuran pertama
							Long amorId = amortizationDAOFactory.getLoanAmortizationDAO().findAmortizationIdByCounter(loan.getLoanId(), 1);
							if (amorId!=null && amorId>0) {
								LoanAmortization loanAmortization = amortizationDAOFactory.getLoanAmortizationDAO().findById(amorId);
								loanAmortization.setPaymentAmount(loanAmortization.getPrincipal().add(loanAmortization.getInterest()));
								loanAmortization.setPaymentDate(curr.getTime());
								loanAmortization.setPenalty(BigDecimal.ZERO);
								loanAmortization.setPaymentPenalty(BigDecimal.ZERO);
								loanAmortization.setPrincipalPayment(loanAmortization.getPrincipal());
								loanAmortization.setInterestPayment(loanAmortization.getInterest());
								amortizationDAOFactory.getLoanAmortizationDAO().update(loanAmortization);
							}
							
							payment.setLoanAmortizationId(amorId);
							payment.setLoanId(loan.getLoanId());
							payment.setPenalty(BigDecimal.ZERO);
							payment.setOtherIncome(BigDecimal.ZERO);
							payment.setPenaltyPayment(BigDecimal.ZERO);
							payment.setNote("Cicilan Pokok ke 1 ("+loan.getTransactionCode()+" a/n "+loan.getAccountName()+")");
							
							loanDAOFactory.getPaymentDAO().save(payment);
							
							// update loan
							loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
							
						}
						
					} else {
						/*
						 * old code only below, before installment feature
						 */
						List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
						for (LoanFunding funding : fundings) {
							LoanDrawdown drawdown = new LoanDrawdown();
							drawdown.setDeductionAmount((((funding.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))).multiply(loan.getCommission())).divide(new BigDecimal(100)));
							if (loan.getInsurance()!=null){
								BigDecimal amount = ((funding.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))).subtract(drawdown.getDeductionAmount()).subtract((((funding.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))).multiply(loan.getInsurance())).divide(new BigDecimal(100)));
								drawdown.setAmount(amount.setScale(0, BigDecimal.ROUND_HALF_UP));
							}else {
								BigDecimal amount = ((funding.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))).subtract(drawdown.getDeductionAmount()).subtract((((funding.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))).multiply(BigDecimal.ZERO)).divide(new BigDecimal(100)));
								drawdown.setAmount(amount.setScale(0, BigDecimal.ROUND_HALF_UP));
							}
							/**LAMA
							 * drawdown.setAmount(((funding.getAmount().multiply(loan.getInterest())).divide(new BigDecimal(100))).subtract(drawdown.getDeductionAmount()));
							**/
							drawdown.setLenderId(funding.getLenderId());
							drawdown.setLoanFundingId(funding.getLoanFundingId());
							drawdown.setLoanId(loan.getLoanId());
							drawdown.setNote("Penerimaan bunga pinjaman "+loan.getTransactionCode()+ " "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy"));
							loanDAOFactory.getLoanDrawdownDAO().save(drawdown);
							
							Account account = accountDAOFactory.getAccountDAO().findByCriteria(Restrictions.eq("accountId", funding.getLenderId()));
							
							/*AccountBalanceHistory*/
//							if(account.getIsMigrated() != null && account.getIsMigrated()) {
//								AccountBalanceHistory accountBalanceHistory = new AccountBalanceHistory();
//								accountBalanceHistory.setAccountId(funding.getLenderId());
//								accountBalanceHistory.setType("Kredit");
//								accountBalanceHistory.setAmount(drawdown.getAmount());
//								accountBalanceHistory.setNote("Penerimaan bunga pinjaman "+loan.getTransactionCode()+ " "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy"));
//								accountBalanceHistory.setBalanceDate(loan.getDrawdownDateTime());
//								accountBalanceHistory.setTransactionType(AccountBalanceHistoryTransactionType.INTEREST);
//								accountBalanceHistory.setKeyId(drawdown.getLoanDrawdownId());
//								
//								BigDecimal availableCashBefore = loanDAOFactory.getAccountBalanceHistoryDAO().calculateAvailableCash(funding.getLenderId());
//								accountBalanceHistory.setAvailableCash(availableCashBefore.add(accountBalanceHistory.getAmount()));
//								loanDAOFactory.getAccountBalanceHistoryDAO().save(accountBalanceHistory);
//
//								accountBalanceHistoryId = accountBalanceHistory.getAccountBalanceHistoryId();
//								accountBalanceHistory = loanDAOFactory.getAccountBalanceHistoryDAO().findByCriteria(Restrictions.eq("accountId", funding.getLenderId()), Restrictions.eq("transactionType", AccountBalanceHistoryTransactionType.FUNDING), Restrictions.eq("keyId", funding.getLoanFundingId()));
//								accountBalanceHistory.setStatus("SUCCESS");
//								loanDAOFactory.getAccountBalanceHistoryDAO().update(accountBalanceHistory);
//							}
						}
					}
				}
				
				loan.setDrawdownNote("Pencairan pinjaman otomatis oleh H2H-"+prepaidProviderName+" pada "+Formater.getFormatedDate(new Date(), "dd MMM yyyy HH:mm:ss"));
				//loan.setNote("Pencairan pinjaman pada "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy"));
				loan.setNote("Pencairan pinjaman pada "+Formater.getFormatedDate(new Date(), "dd MMM yyyy"));
				Calendar c = new GregorianCalendar();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				loan.setDrawdownDate(c.getTime());
				loan.setDrawdownDateTime(new Date());
				//c.setTime(loan.getDrawdownDate());
				
				/*AccountBalanceHistory*/
//				if (accountBalanceHistoryId != 0 && accountBalanceHistoryId != null) {
//					AccountBalanceHistory accountBalanceHistory = loanDAOFactory.getAccountBalanceHistoryDAO().findByCriteria(Restrictions.eq("accountBalanceHistoryId", accountBalanceHistoryId));
//					accountBalanceHistory.setBalanceDate(loan.getDrawdownDateTime());
//					loanDAOFactory.getAccountBalanceHistoryDAO().update(accountBalanceHistory);
//				}
	
				if (loanProduct.getInstallment()!=null && loanProduct.getInstallment()) {
					if (loanProduct.getType()==InterestType.ADVANCE) {
						if (loan.getUnit()==TenorUnit.DAY) {
							c.add(Calendar.DATE, loan.getTenor()-1);
							loan.setOverDueDate(c.getTime());
						} else if (loan.getUnit()==TenorUnit.WEEK) {
							c.add(Calendar.WEEK_OF_YEAR, loan.getTenor()-1);
							loan.setOverDueDate(c.getTime());
						} else if (loan.getUnit()==TenorUnit.MONTH) {
							c.add(Calendar.MONTH, loan.getTenor()-1);
							loan.setOverDueDate(c.getTime());
						} else if (loan.getUnit()==TenorUnit.YEAR) {
							c.add(Calendar.YEAR, loan.getTenor()-1);
							loan.setOverDueDate(c.getTime());
						}
					} else {
						if (loan.getUnit()==TenorUnit.DAY) {
							c.add(Calendar.DATE, loan.getTenor());
							loan.setOverDueDate(c.getTime());
						} else if (loan.getUnit()==TenorUnit.WEEK) {
							c.add(Calendar.WEEK_OF_YEAR, loan.getTenor());
							loan.setOverDueDate(c.getTime());
						} else if (loan.getUnit()==TenorUnit.MONTH) {
							c.add(Calendar.MONTH, loan.getTenor());
							loan.setOverDueDate(c.getTime());
						} else if (loan.getUnit()==TenorUnit.YEAR) {
							c.add(Calendar.YEAR, loan.getTenor());
							loan.setOverDueDate(c.getTime());
						}
					}
				} else {
					if (loan.getUnit()==TenorUnit.DAY) {
						c.add(Calendar.DATE, loan.getTenor());
						loan.setOverDueDate(c.getTime());
					} else if (loan.getUnit()==TenorUnit.WEEK) {
						c.add(Calendar.WEEK_OF_YEAR, loan.getTenor());
						loan.setOverDueDate(c.getTime());
					} else if (loan.getUnit()==TenorUnit.MONTH) {
						c.add(Calendar.MONTH, loan.getTenor());
						loan.setOverDueDate(c.getTime());
					} else if (loan.getUnit()==TenorUnit.YEAR) {
						c.add(Calendar.YEAR, loan.getTenor());
						loan.setOverDueDate(c.getTime());
					}
				}
				
				
				//SET FROM BANK ACCOUNT ID ke DANAMAS
				BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"),Restrictions.isNull("prepaidProviderId"));
				if (bankAccount!=null && loan.getFromBankAccountId()==null) loan.setFromBankAccountId(bankAccount.getBankAccountId());
				
				//loan.setDrawdownDate(new Date());
				loan.setFundingStatus(LoanFundingStatus.TRANSFERED);
				loan.setChangeOn(new Date());
				// update ppob flag!
				if ((applicationSetup.getIsPpobLoanActive()==null || (applicationSetup.getIsPpobLoanActive()!=null && applicationSetup.getIsPpobLoanActive()==Boolean.FALSE)) 
					&& (applicationSetup.getIsDirectTransferActive()==null || (applicationSetup.getIsDirectTransferActive()!=null && applicationSetup.getIsDirectTransferActive()==Boolean.FALSE))) {
					if (loan.getLoanDisbursementTransferInfoId()==null) loan.setLoanDisbursementTransferInfoId(new Long(0));
				} 
				/*
				 *  set loan-counter
				 *  29-05-2017
				 */
//				loan.setLoanCounter(loanDAOFactory.getLoanDAO().getLastLoanCounter(loan.getAccountId(), true));
				
				loanDAOFactory.getLoanDAO().update(loan);
			}
			
			log.info(" <<<<<<<< END of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" drawdownLoanAfterPpob() >>>>>>>>");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}
	
	
	public void processCashOutPpob() {
		try {
			
			log.info(" <<<<<<<< START LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processCashOutPpob() >>>>>>>>");
			
			// bank danamas!
			BankAccount danamasBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
			
			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
			List<CashOut> cashOuts = loanDAOFactory.getCashOutDAO().findByCriteria(Order.asc("cashOutId"), 
					Restrictions.eq("status", CashOutStatus.UNPAID),
					Restrictions.isNull("cashOutDisbursementTransferInfoId"),
					Restrictions.isNull("parent"));
//					Restrictions.eqOrIsNull("isRdp2p", false));

			//BankAccount bankAccount = null;
			int totCo = cashOuts.size()>0 ? cashOuts.size() : 0;
			log.info("cashOut count() >> "+totCo);
			CashOutDisbursementTransferInfo cekDataCo;
			Session session = null; 
			Transaction transaction = null;
			Long idInfo = null; 
			for (CashOut cashOut : cashOuts) {
				
				// pengecekan double data
				cekDataCo = loanDAOFactory.getCashOutDisbursementTransferInfoDAO().findByCriteria(Restrictions.eq("accountCreditCode", cashOut.getTransactionCode()));
				log.info("cekData : "+cekDataCo);
				if (cekDataCo != null) {
					
					log.info("AWAS sudah ada data co : "+cashOut.getTransactionCode());
					loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), cekDataCo.getCashOutDisbursementTransferInfoId());
					
				} else {
				
					BankAccount lenderBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("accountId", cashOut.getAccountId()), Restrictions.isNull("prepaidProviderId"));
					Calendar c1 = new GregorianCalendar();
					// TODO
					ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
					if (applicationSetup.getEndOfMonth() != null) {
						c1.setTime(applicationSetup.getEndOfMonth());
					} else {
						c1.set(Calendar.YEAR, 2016);
						c1.set(Calendar.MONTH, Calendar.JULY);
						c1.set(Calendar.DATE, 1);
					}
					
					BigDecimal totalBalance = (loanDAOFactory.getAccountBalanceDAO().getAccountBalanceAmountForCashOutByAccountId(cashOut.getAccountId(), c1.getTime())).setScale(0,BigDecimal.ROUND_HALF_UP);
					// only for funding_status is TRANSFERED
					
					if (cashOut.getAmount().compareTo(totalBalance) > 0) {
						session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
						loanDAOFactory.getCashOutDAO().preventMinusAvailableCash(cashOut.getCashOutId(), session);
					} else if (lenderBankAccount!=null && cashOut.getAmount().compareTo(BigDecimal.ZERO)>0) {
	
						// sementara hanya bank sinarmas
						Lookup lookup = basicDAOFactory.getLookupDAO().findById(lenderBankAccount.getBank().getLookupId());
						log.info(lookup.getCode());
						
						//SINARMAS || BCA || MANDIRI || PERMATA
						
						if (lookup !=null && (lookup.getCode().trim().startsWith("153") || lookup.getCode().trim().startsWith("014") || lookup.getCode().trim().startsWith("008") || lookup.getCode().trim().startsWith("013"))) {
				
							session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
							try {
								// start transaction
								if (lookup.getCode().trim().startsWith("153")) {
									transaction = session.beginTransaction();
									
									CashOut cashOut2 = new CashOut();
									cashOut2.setStatusPpob("PENDING");
									
									CashOutDisbursementTransferInfo cashOutDisbursementTransferInfo = new CashOutDisbursementTransferInfo(); 
									cashOutDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
									cashOutDisbursementTransferInfo.setAccountDebitCode("TMU");
									cashOutDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
									cashOutDisbursementTransferInfo.setAmount(cashOut.getAmount());
									cashOutDisbursementTransferInfo.setAccountCredit(lenderBankAccount.getAccountNumber());
									cashOutDisbursementTransferInfo.setAccountCreditCode(cashOut.getTransactionCode());
									cashOutDisbursementTransferInfo.setAccountCreditName(lenderBankAccount.getAccountName());
									cashOutDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
									cashOutDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
									cashOutDisbursementTransferInfo.setIsEmail("F");
										
									loanDAOFactory.getCashOutDisbursementTransferInfoDAO().save(cashOutDisbursementTransferInfo, session);
									idInfo = cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId();
									
									//cashOut.setCashOutDisbursementTransferInfoId(cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId());
									//loanDAOFactory.getCashOutDAO().update(cashOut);
									
									loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), idInfo, session);
									loanDAOFactory.getCashOutDAO().updateCashOutStatusPpobSinarmas(cashOut.getCashOutId(), cashOut2.getStatusPpob(), session);
									
									transaction.commit();
							
								} else {
									if(lookup.getMemberCode() != null) {
										transaction = session.beginTransaction(); 
										
										CashOut cashOut2 = new CashOut();
										cashOut2.setStatusPpob("PENDING");
										
										CashOutDisbursementTransferInfo cashOutDisbursementTransferInfo = new CashOutDisbursementTransferInfo(); 
										cashOutDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
										cashOutDisbursementTransferInfo.setAccountDebitCode("TMU");
										cashOutDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
										cashOutDisbursementTransferInfo.setAmount(cashOut.getAmount());
										cashOutDisbursementTransferInfo.setAccountCredit(lenderBankAccount.getAccountNumber());
										cashOutDisbursementTransferInfo.setAccountCreditCode(cashOut.getTransactionCode());
										cashOutDisbursementTransferInfo.setAccountCreditName(lenderBankAccount.getAccountName());
										cashOutDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.EXTERNAL);
										cashOutDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
										cashOutDisbursementTransferInfo.setIsEmail("F");
											
										loanDAOFactory.getCashOutDisbursementTransferInfoDAO().save(cashOutDisbursementTransferInfo, session);
										idInfo = cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId();
										
										//cashOut.setCashOutDisbursementTransferInfoId(cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId());
										//loanDAOFactory.getCashOutDAO().update(cashOut);
										
										loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), idInfo, session);
										loanDAOFactory.getCashOutDAO().updateCashOutStatusPpobNonSinarmas(cashOut.getCashOutId(), cashOut2.getStatusPpob(), session);
										
										transaction.commit();
								
									}
								}
									
							} catch (Exception ex) {
								if (transaction!=null) transaction.rollback();
								ex.printStackTrace();
							} finally {
								if (session!=null) {
									session.close(); session=null;
								}
							}
							
							// biar aman
							log.info("cashOutId: "+cashOut.getCashOutId()+" idInfo: "+idInfo);
							if (idInfo!=null) {
								loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), idInfo);
								idInfo = null;
							} else {
								log.info("=== CashOut : "+cashOut.getCashOutId()+" tc : "+cashOut.getTransactionCode()+" idInfo null!!!");
							}
							
						}	
					}
				}
			}
			
			log.info(" <<<<<<<< END of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processCashOutPpob() >>>>>>>>");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
//	public void processCashInPpobRdp2p() {
//		try {
//			BankAccount tokomodalBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
//			
//			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
//			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
//			List<CashIn> cashIns = loanDAOFactory.getCashInDAO().findByCriteria(Order.asc("cashInId"), 
//					Restrictions.eq("status", CashInStatus.CONFIRMED),
//					Restrictions.isNotNull("isRdp2p"),
//					Restrictions.eq("isPosted", false),
//					Restrictions.eq("isRdp2p", true));
//
//			//BankAccount bankAccount = null;
//			int totalCashIn = cashIns.size()>0 ? cashIns.size() : 0;
//			
//			CashInDisbursementTransferInfoRdp2p cekDataCo;
//			Session session = null; 
//			Transaction transaction = null;
//			Long idInfo = null; 
//			for (CashIn cashIn : cashIns) {
//				cekDataCo = loanDAOFactory.getCashInDisbursementTransferInfoRdp2pDAO().findByCriteria(Restrictions.eq("accountCreditCode", cashIn.getTransactionCode()));
//				
//				if (cekDataCo != null) {
//					
//					log.info("AWAS sudah ada data ci : "+cashIn.getTransactionCode());
//					loanDAOFactory.getCashInDAO().updateCashInDisbursementTransferInfoRdp2pId(cashIn.getCashInId(), cekDataCo.getCashInDisbursementTransferInfoRdp2pId());
//					
//				} else {
//				
//					Rdp2pBankAccount lenderBankAccount = accountDAOFactory.getRdp2pBankAccountDAO().findByCriteria(Restrictions.eq("accountId", cashIn.getAccountId()), Restrictions.isNull("prepaidProviderId"));
//					
//					// only for funding_status is TRANSFERED
//					if (lenderBankAccount!=null && cashIn.getAmount().compareTo(BigDecimal.ZERO)>0) {
//	
//						// sementara hanya bank sinarmas
//						Lookup lookup = basicDAOFactory.getLookupDAO().findById(lenderBankAccount.getBank().getLookupId());
//						log.info(lookup.getCode());
//						
//						//SINARMAS || BCA || MANDIRI || PERMATA
//						
//						if (lookup !=null && (lookup.getCode().trim().startsWith("153") || lookup.getCode().trim().startsWith("014") || lookup.getCode().trim().startsWith("008") || lookup.getCode().trim().startsWith("013"))) {
//				
//							session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
//							try {
//								// start transaction
//									transaction = session.beginTransaction();
//									
//									CashInDisbursementTransferInfoRdp2p cashInDisbursementTransferInfoRdp2p = new CashInDisbursementTransferInfoRdp2p(); 
//									cashInDisbursementTransferInfoRdp2p.setAccountDebit(tokomodalBankAccount.getAccountNumber());
//									cashInDisbursementTransferInfoRdp2p.setAccountDebitCode("TMU");
//									cashInDisbursementTransferInfoRdp2p.setAccountDebitName(tokomodalBankAccount.getAccountName());
//									cashInDisbursementTransferInfoRdp2p.setAmount(cashIn.getAmount());
//									cashInDisbursementTransferInfoRdp2p.setAccountCredit(lenderBankAccount.getAccountNumber());
//									cashInDisbursementTransferInfoRdp2p.setAccountCreditCode(cashIn.getTransactionCode());
//									cashInDisbursementTransferInfoRdp2p.setAccountCreditName(lenderBankAccount.getAccountName());
//									cashInDisbursementTransferInfoRdp2p.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
//									cashInDisbursementTransferInfoRdp2p.setBankMemberCode(lookup.getMemberCode());
//										
//									loanDAOFactory.getCashInDisbursementTransferInfoRdp2pDAO().save(cashInDisbursementTransferInfoRdp2p, session);
//									loanDAOFactory.getCashInDAO().updateCashInDisbursementTransferInfoRdp2pId(cashIn.getCashInId(), cashInDisbursementTransferInfoRdp2p.getCashInDisbursementTransferInfoRdp2pId(), session);
//									
//									transaction.commit();
//							
//							} catch (Exception ex) {
//								if (transaction!=null) transaction.rollback();
//								ex.printStackTrace();
//							} finally {
//								if (session!=null) {
//									session.close(); session=null;
//								}
//							}
//						}	
//					}
//				}
//			}
//			
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}
	
//	public void processCashOutPpobRdp2p() {
//		try {
//			BankAccount tokomodalBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
//			
//			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
//			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
//			List<CashOut> cashOutRdp2ps = loanDAOFactory.getCashOutDAO().findByCriteria(Order.asc("cashOutId"),
//					Restrictions.eq("status", CashOutStatus.UNPAID),
//					Restrictions.eq("isRdp2p", true),
//					Restrictions.isNotNull("isRdp2p"),
//					Restrictions.isNull("cashOutDisbursementTransferInfoRdp2pId"));
//			
//			//BankAccount bankAccount = null;
//			int totalCashOutRdp2p = cashOutRdp2ps.size()>0 ? cashOutRdp2ps.size() : 0;
//			
//			CashOutDisbursementTransferInfoRdp2p cekDataCo;
//			Session session = null; 
//			Transaction transaction = null;
//			Long idInfo = null; 
//			for (CashOut cashOut : cashOutRdp2ps) {
//				cekDataCo = loanDAOFactory.getCashOutDisbursementTransferInfoRdp2pDAO().findByCriteria(Restrictions.eq("accountDebitCode", cashOut.getTransactionCode()));
//				
//				if (cekDataCo != null) {
//					
//					log.info("AWAS sudah ada data co : "+cashOut.getTransactionCode());
//					loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoRdp2pId(cashOut.getCashOutId(), cekDataCo.getCashOutDisbursementTransferInfoRdp2pId());
//					
//				} else {
//				
//					Rdp2pBankAccount rdp2pBankAccount = accountDAOFactory.getRdp2pBankAccountDAO().findByCriteria(Restrictions.eq("accountId", cashOut.getAccountId()), Restrictions.isNull("prepaidProviderId"));
//					
//					// only for funding_status is TRANSFERED
//					if (rdp2pBankAccount!=null && cashOut.getAmount().compareTo(BigDecimal.ZERO)>0) {
//	
//						// sementara hanya bank sinarmas
//						Lookup lookup = basicDAOFactory.getLookupDAO().findById(rdp2pBankAccount.getBank().getLookupId());
//						
//						//SINARMAS || BCA || MANDIRI || PERMATA
//						
//						if (lookup !=null && (lookup.getCode().trim().startsWith("153") || lookup.getCode().trim().startsWith("014") || lookup.getCode().trim().startsWith("008") || lookup.getCode().trim().startsWith("013"))) {
//				
//							session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
//							try {
//								// start transaction
//									transaction = session.beginTransaction();
//									
//									CashOutDisbursementTransferInfoRdp2p cashOutDisbursementTransferInfoRdp2p = new CashOutDisbursementTransferInfoRdp2p(); 
//									cashOutDisbursementTransferInfoRdp2p.setAccountDebit(rdp2pBankAccount.getAccountNumber());
//									cashOutDisbursementTransferInfoRdp2p.setAccountDebitCode(cashOut.getTransactionCode());
//									cashOutDisbursementTransferInfoRdp2p.setAccountDebitName(rdp2pBankAccount.getAccountName());
//									cashOutDisbursementTransferInfoRdp2p.setAmount(cashOut.getAmount());
//									cashOutDisbursementTransferInfoRdp2p.setAccountCredit(tokomodalBankAccount.getAccountNumber());
//									cashOutDisbursementTransferInfoRdp2p.setAccountCreditCode("TMU");
//									cashOutDisbursementTransferInfoRdp2p.setAccountCreditName(tokomodalBankAccount.getAccountName());
//									cashOutDisbursementTransferInfoRdp2p.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
//									cashOutDisbursementTransferInfoRdp2p.setBankMemberCode(lookup.getMemberCode());
//										
//									loanDAOFactory.getCashOutDisbursementTransferInfoRdp2pDAO().save(cashOutDisbursementTransferInfoRdp2p, session);
//									loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoRdp2pId(cashOut.getCashOutId(), cashOutDisbursementTransferInfoRdp2p.getCashOutDisbursementTransferInfoRdp2pId(), session);
//									
//									transaction.commit();
//							
//							} catch (Exception ex) {
//								if (transaction!=null) transaction.rollback();
//								ex.printStackTrace();
//							} finally {
//								if (session!=null) {
//									session.close(); session=null;
//								}
//							}
//						}	
//					}
//				}
//			}
//						
//			List<CashOut> cashOuts = loanDAOFactory.getCashOutDAO().findByCriteria(Order.asc("cashOutId"),
//					Restrictions.eq("status", CashOutStatus.UNPAID),
//					Restrictions.eq("isRdp2p", true),
//					Restrictions.isNotNull("isRdp2p"),
//					Restrictions.isNull("cashOutDisbursementTransferInfoId"),
//					Restrictions.eq("disbursementStatus", DisbursementStatus.SUCCESS));
//			
//			//BankAccount bankAccount = null;
//			int totalCashOut = cashOuts.size()>0 ? cashOuts.size() : 0;
//			
//			CashOutDisbursementTransferInfo cekDataCoDisburs;
//			session = null; 
//			transaction = null;
//			idInfo = null; 
//			for (CashOut cashOut : cashOuts) {
//				cekDataCoDisburs = loanDAOFactory.getCashOutDisbursementTransferInfoDAO().findByCriteria(Restrictions.eq("accountCreditCode", cashOut.getTransactionCode()));
//				cekDataCo = loanDAOFactory.getCashOutDisbursementTransferInfoRdp2pDAO().findByCriteria(Restrictions.eq("accountDebitCode", cashOut.getTransactionCode()), Restrictions.eq("status", "d0"));
//				
//				if (cekDataCoDisburs != null) {
//					
//					log.info("AWAS sudah ada data co : "+cashOut.getTransactionCode());
//					loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), cekDataCoDisburs.getCashOutDisbursementTransferInfoId());
//					
//				} else if (cekDataCo != null) {
//					BankAccount lenderBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("accountId", cashOut.getAccountId()), Restrictions.isNull("prepaidProviderId"));
//					
//					// only for funding_status is TRANSFERED
//					if (lenderBankAccount!=null && cashOut.getAmount().compareTo(BigDecimal.ZERO)>0) {
//	
//						// sementara hanya bank sinarmas
//						Lookup lookup = basicDAOFactory.getLookupDAO().findById(lenderBankAccount.getBank().getLookupId());
//						log.info(lookup.getCode());
//						
//						//SINARMAS || BCA || MANDIRI || PERMATA
//						
//						if (lookup !=null && (lookup.getCode().trim().startsWith("153") || lookup.getCode().trim().startsWith("014") || lookup.getCode().trim().startsWith("008") || lookup.getCode().trim().startsWith("013"))) {
//				
//							session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
//							try {
//								// start transaction
//								if (lookup.getCode().trim().startsWith("153")) {
//									transaction = session.beginTransaction();
//									
//									CashOut cashOut2 = new CashOut();
//									cashOut2.setStatusPpob("PENDING");
//									
//									CashOutDisbursementTransferInfo cashOutDisbursementTransferInfo = new CashOutDisbursementTransferInfo(); 
//									cashOutDisbursementTransferInfo.setAccountDebit(tokomodalBankAccount.getAccountNumber());
//									cashOutDisbursementTransferInfo.setAccountDebitCode("TMU");
//									cashOutDisbursementTransferInfo.setAccountDebitName(tokomodalBankAccount.getAccountName());
//									cashOutDisbursementTransferInfo.setAmount(cashOut.getAmount());
//									cashOutDisbursementTransferInfo.setAccountCredit(lenderBankAccount.getAccountNumber());
//									cashOutDisbursementTransferInfo.setAccountCreditCode(cashOut.getTransactionCode());
//									cashOutDisbursementTransferInfo.setAccountCreditName(lenderBankAccount.getAccountName());
//									cashOutDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
//									cashOutDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
//									cashOutDisbursementTransferInfo.setIsEmail("F");
//										
//									loanDAOFactory.getCashOutDisbursementTransferInfoDAO().save(cashOutDisbursementTransferInfo, session);
//									idInfo = cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId();
//									
//									//cashOut.setCashOutDisbursementTransferInfoId(cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId());
//									//loanDAOFactory.getCashOutDAO().update(cashOut);
//									
//									loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), idInfo, session);
//									loanDAOFactory.getCashOutDAO().updateCashOutStatusPpobSinarmas(cashOut.getCashOutId(), cashOut2.getStatusPpob(), session);
//									
//									transaction.commit();
//							
//								} else {
//									if(lookup.getMemberCode() != null) {
//										transaction = session.beginTransaction(); 
//										
//										CashOut cashOut2 = new CashOut();
//										cashOut2.setStatusPpob("PENDING");
//										
//										CashOutDisbursementTransferInfo cashOutDisbursementTransferInfo = new CashOutDisbursementTransferInfo(); 
//										cashOutDisbursementTransferInfo.setAccountDebit(tokomodalBankAccount.getAccountNumber());
//										cashOutDisbursementTransferInfo.setAccountDebitCode("TMU");
//										cashOutDisbursementTransferInfo.setAccountDebitName(tokomodalBankAccount.getAccountName());
//										cashOutDisbursementTransferInfo.setAmount(cashOut.getAmount());
//										cashOutDisbursementTransferInfo.setAccountCredit(lenderBankAccount.getAccountNumber());
//										cashOutDisbursementTransferInfo.setAccountCreditCode(cashOut.getTransactionCode());
//										cashOutDisbursementTransferInfo.setAccountCreditName(lenderBankAccount.getAccountName());
//										cashOutDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.EXTERNAL);
//										cashOutDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
//										cashOutDisbursementTransferInfo.setIsEmail("F");
//											
//										loanDAOFactory.getCashOutDisbursementTransferInfoDAO().save(cashOutDisbursementTransferInfo, session);
//										idInfo = cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId();
//										
//										//cashOut.setCashOutDisbursementTransferInfoId(cashOutDisbursementTransferInfo.getCashOutDisbursementTransferInfoId());
//										//loanDAOFactory.getCashOutDAO().update(cashOut);
//										
//										loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), idInfo, session);
//										loanDAOFactory.getCashOutDAO().updateCashOutStatusPpobNonSinarmas(cashOut.getCashOutId(), cashOut2.getStatusPpob(), session);
//										
//										transaction.commit();
//								
//									}
//								}
//									
//							} catch (Exception ex) {
//								if (transaction!=null) transaction.rollback();
//								ex.printStackTrace();
//							} finally {
//								if (session!=null) {
//									session.close(); session=null;
//								}
//							}
//							
//							// biar aman
//							log.info("cashOutId: "+cashOut.getCashOutId()+" idInfo: "+idInfo);
//							if (idInfo!=null) {
//								loanDAOFactory.getCashOutDAO().updateCashOutDisbursementTransferInfoId(cashOut.getCashOutId(), idInfo);
//								idInfo = null;
//							} else {
//								log.info("=== CashOut : "+cashOut.getCashOutId()+" tc : "+cashOut.getTransactionCode()+" idInfo null!!!");
//							}
//							
//						}	
//					}
//				}
//			}
//			
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}
	
//	public void processPaymentDetailPpobRdp2p() {
//		try {
//			BankAccount tokomodalBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
//			
//			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
//			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
//			List<PaymentDetail> paymentDetails = loanDAOFactory.getPaymentDetailDAO().findByCriteria(Order.asc("paymentDetailId"),
//					Restrictions.isNull("paymentDetailTransferInfoRdp2pId"),
//					Restrictions.eq("isRdp2p", true),
//					Restrictions.isNotNull("isRdp2p"));
//
//			//BankAccount bankAccount = null;
//			int totalPaymentDetail = paymentDetails.size()>0 ? paymentDetails.size() : 0;
//			
//			PaymentDetailDisbursementTransferInfoRdp2p cekDataPaymentDetailRdp2p;
//			Session session = null; 
//			Transaction transaction = null;
//			Long idInfo = null; 
//			for (PaymentDetail paymentDetail : paymentDetails) {
//				cekDataPaymentDetailRdp2p = loanDAOFactory.getPaymentDetailDisbursementTransferInfoRdp2pDAO().findByCriteria(Restrictions.eq("accountCreditCode", paymentDetail.getPayment().getTransactionCode()));
//				
//				if (cekDataPaymentDetailRdp2p != null) {
//					
//					log.info("AWAS sudah ada data payment detail : "+ paymentDetail.getPayment().getTransactionCode());
//					loanDAOFactory.getPaymentDetailDAO().updatePaymentDetailDisbursementTransferInfoRdp2pId(paymentDetail.getPaymentDetailId(), cekDataPaymentDetailRdp2p.getPaymentDetailDisbursementTransferInfoRdp2pId());
//					
//				} else {
//				
//					Rdp2pBankAccount lenderBankAccount = accountDAOFactory.getRdp2pBankAccountDAO().findByCriteria(Restrictions.eq("accountId", paymentDetail.getLenderId()), Restrictions.isNull("prepaidProviderId"));
//					
//					// only for funding_status is TRANSFERED
//					if (lenderBankAccount!=null && paymentDetail.getAmount().compareTo(BigDecimal.ZERO)>0) {
//	
//						// sementara hanya bank sinarmas
//						Lookup lookup = basicDAOFactory.getLookupDAO().findById(lenderBankAccount.getBank().getLookupId());
//						log.info(lookup.getCode());
//						
//						//SINARMAS || BCA || MANDIRI || PERMATA
//						
//						if (lookup !=null && (lookup.getCode().trim().startsWith("153") || lookup.getCode().trim().startsWith("014") || lookup.getCode().trim().startsWith("008") || lookup.getCode().trim().startsWith("013"))) {
//				
//							session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
//							try {
//								// start transaction
//									transaction = session.beginTransaction();
//									
//									PaymentDetailDisbursementTransferInfoRdp2p paymentDetailDisbursementTransferInfoRdp2p = new PaymentDetailDisbursementTransferInfoRdp2p();
//									paymentDetailDisbursementTransferInfoRdp2p.setAccountDebit(tokomodalBankAccount.getAccountNumber());
//									paymentDetailDisbursementTransferInfoRdp2p.setAccountDebitCode("TMU");
//									paymentDetailDisbursementTransferInfoRdp2p.setAccountDebitName(tokomodalBankAccount.getAccountName());
//									
//									if (paymentDetail.getPayment().getPenaltyPayment() != null && paymentDetail.getPayment().getPenaltyPayment().compareTo(BigDecimal.ZERO)>0) { paymentDetailDisbursementTransferInfoRdp2p.setAmount(paymentDetail.getAmount().add(paymentDetail.getPayment().getPenaltyPayment())); }
//									else { paymentDetailDisbursementTransferInfoRdp2p.setAmount(paymentDetail.getAmount()); }
//									
//									paymentDetailDisbursementTransferInfoRdp2p.setAccountCredit(lenderBankAccount.getAccountNumber());
//									paymentDetailDisbursementTransferInfoRdp2p.setAccountCreditCode(paymentDetail.getPayment().getTransactionCode());
//									paymentDetailDisbursementTransferInfoRdp2p.setAccountCreditName(lenderBankAccount.getAccountName());
//									paymentDetailDisbursementTransferInfoRdp2p.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
//									paymentDetailDisbursementTransferInfoRdp2p.setBankMemberCode(lookup.getMemberCode());
//										
//									loanDAOFactory.getPaymentDetailDisbursementTransferInfoRdp2pDAO().save(paymentDetailDisbursementTransferInfoRdp2p, session);
//									loanDAOFactory.getPaymentDetailDAO().updatePaymentDetailDisbursementTransferInfoRdp2pId(paymentDetail.getPaymentDetailId(), paymentDetailDisbursementTransferInfoRdp2p.getPaymentDetailDisbursementTransferInfoRdp2pId(), session);
//									
//									transaction.commit();
//							
//							} catch (Exception ex) {
//								if (transaction!=null) transaction.rollback();
//								ex.printStackTrace();
//							} finally {
//								if (session!=null) {
//									session.close(); session=null;
//								}
//							}
//						}	
//					}
//				}
//			}
//			
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}
	
//	public void processLoanPpobRdp2p() {
//		try {
//			BankAccount tokomodalBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
//			
//			// validation for loan.amount == sum(loan_funding.amount) or must be 100%
//			List<Loan> loanValids = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), 
//					Restrictions.eq("fundingStatus", LoanFundingStatus.FULL),
//					Restrictions.eq("isRdp2p", true),
//					Restrictions.isNull("loanDisbursementTransferInfoRdp2pId"),
//					Restrictions.isNull("disbursementStatus"));
//			
//			BigDecimal totFundingAmount;			
//			boolean z = false;
//			int amountCompare = 0;
//			for (Loan loan : loanValids) {
//				
//				amountCompare = loan.getFundingAmount().compareTo(loan.getAmount());
//				z = false;
//				
//				if (loan.getFundingAmount() != null && amountCompare != 0) {
//					
//					// jika lebih
//					if (amountCompare == 1) {
//						// delete semua loan-funding! yg bikin ancur / id >= start id ancur
//						totFundingAmount = BigDecimal.ZERO;
//						List<LoanFunding> loanFundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
//						
//						for (LoanFunding loanFundingX : loanFundings) {
//							totFundingAmount = totFundingAmount.add(loanFundingX.getAmount());
//							
//							if (z == false) {
//								
//								// cek apakah sudah over funding
//								if (totFundingAmount.compareTo(loan.getAmount())==1) z = true;
//								
//							}
//							
//							if (z == true) {
//								// delete semua saja funding yang mulai rusak 
//								loanDAOFactory.getLoanFundingDAO().delete(loanFundingX.getLoanFundingId());
//							}
//							
//						}
//						
//						// biar aman
//						Loan lastLoan = loanDAOFactory.getLoanDAO().findById(loan.getLoanId());
//						
//						// jika loan.fundingAmount < loan.amount maka, update loan.funding_status = NEW
//						// jika loan.fundingAmount = loan.amount maka, status loan.funding_status tetap FULL atau tidak diupdate
//						if (lastLoan.getFundingAmount().compareTo(loan.getAmount()) == -1) {
//							
//							Loan ll = loanDAOFactory.getLoanDAO().findById(loan.getLoanId());
//							ll.setFundingStatus(LoanFundingStatus.NEW);
//							loanDAOFactory.getLoanDAO().update(ll);
//							
//						}
//						
//					} else if (amountCompare == -1) {
//						
//						// jika kurang
//						Loan ll = loanDAOFactory.getLoanDAO().findById(loan.getLoanId());
//						
//						ll.setFundingStatus(LoanFundingStatus.NEW);
//						loanDAOFactory.getLoanDAO().update(ll);
//					}
//					
//				}
//				
//			}	
//			
//			// for the 1st time lunch, we should replace loan.loan_disburs_transfer_info_id with 0 to avoid double transfer
//			// run this query for 1st time => UPDATE LOAN SET LOAN_DISBURS_TRANSFER_INFO_ID = 0 WHERE FUNDING_STATUS='TRANSFERED';
//			List<Loan> loans = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), 
//					Restrictions.eq("fundingStatus", LoanFundingStatus.FULL),
//					Restrictions.eq("isRdp2p", true),
//					Restrictions.isNull("loanDisbursementTransferInfoRdp2pId"),
//					Restrictions.isNull("disbursementStatus"));
//			
//			int totLoan = loans.size()>0 ? loans.size() : 0;
//			log.info("loan count() >> "+totLoan);
//			Session session = null; 
//			Transaction transaction = null;
//			Long idInfo = null; 
//			for (Loan loan : loans) {
//				// pengecekan sebelum insert, pastikan tidak ada data loan.getTransactionCode() yg double
//				LoanDisbursementTransferInfoRdp2p cekData = loanDAOFactory.getLoanDisbursementTransferInfoRdp2pDAO().findByCriteria(Restrictions.eq("accountDebitCode", loan.getTransactionCode()));
//				log.info("cekData : "+cekData);
//				if (cekData != null) {
//					
//					log.info("AWAS sudah ada data loan : "+loan.getTransactionCode());
//					loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoRdp2pId(loan.getLoanId(), cekData.getLoanDisbursementTransferInfoRdp2pId());
//					log.info("Update loan::loanId > "+loan.getLoanId()+" with infoId > "+cekData.getLoanDisbursementTransferInfoRdp2pId());
//					
//				} else {
//					// new session
//					session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
//					try {
//						LoanFunding loanFunding = loanDAOFactory.getLoanFundingDAO().findByCriteria(Restrictions.eq("loanId", loan.getLoanId()));
//						Rdp2pBankAccount rdp2pBankAccount = accountDAOFactory.getRdp2pBankAccountDAO().findByCriteria(Restrictions.eq("accountId", loanFunding.getLenderId()), Restrictions.isNull("prepaidProviderId"));
//						
//						// start transaction
//						transaction = session.beginTransaction();
//						
//						// only for funding_status is TRANSFERED
//						if (rdp2pBankAccount != null && loan.getFundingStatus()==LoanFundingStatus.FULL && (loan.getTotal() != null && loan.getTotal().compareTo(new BigDecimal(0)) >0)) {
//							
//							// search on table PrepaidReseller, for now prepaidReseller only
//							LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findByCriteria(session, Restrictions.eq("loanProductId", loan.getProductId()));
//									
//							LoanDisbursementTransferInfoRdp2p loanDisbursementTransferInfoRdp2p = new LoanDisbursementTransferInfoRdp2p(); 
//							loanDisbursementTransferInfoRdp2p.setAccountDebit(rdp2pBankAccount.getAccountNumber());
//							loanDisbursementTransferInfoRdp2p.setAccountDebitCode(loan.getTransactionCode());
//							loanDisbursementTransferInfoRdp2p.setAccountDebitName(rdp2pBankAccount.getAccountName());
//							loanDisbursementTransferInfoRdp2p.setAmount(loan.getTotal());
//							log.info("loanId : "+loan.getLoanId()+" productId : "+loan.getProductId()+" >> "+loanProduct.isPrepaidReseller());
//							if (loanProduct.isPrepaidReseller()) {
//								
//								PrepaidReseller prepaidReseller = accountDAOFactory.getPrepaidResellerDAO().findByCriteria(session, Restrictions.eq("accountId", loan.getAccountId()));
//								if (prepaidReseller != null) {
//									Lookup lookup = basicDAOFactory.getLookupDAO().findById(rdp2pBankAccount.getBank().getLookupId(), session);
//									log.info(lookup.getCode());
//									if (lookup !=null) {
//										// bank sinarmas
//										if (lookup.getCode().trim().startsWith("153")) {
//											loanDisbursementTransferInfoRdp2p.setAccountCredit(tokomodalBankAccount.getAccountNumber());
//											loanDisbursementTransferInfoRdp2p.setAccountCreditCode("TMU");
//											loanDisbursementTransferInfoRdp2p.setAccountCreditName(tokomodalBankAccount.getAccountName());
//											loanDisbursementTransferInfoRdp2p.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
//											loanDisbursementTransferInfoRdp2p.setBankMemberCode(lookup.getMemberCode());
//										} else {
//											// non bank sinarmas
//											if (lookup.getMemberCode() != null) {
//												loanDisbursementTransferInfoRdp2p.setAccountCredit(tokomodalBankAccount.getAccountNumber());
//												loanDisbursementTransferInfoRdp2p.setAccountCreditCode("TMU");
//												loanDisbursementTransferInfoRdp2p.setAccountCreditName(tokomodalBankAccount.getAccountName());
//												loanDisbursementTransferInfoRdp2p.setBankDirectTransferType(BankDirectTransferType.EXTERNAL);
//												loanDisbursementTransferInfoRdp2p.setBankMemberCode(lookup.getMemberCode());
//											}
//										}
//									}
//								}
//								
//							}
//							
//							// make sure only account_credit is not null are saved
//							if (loanDisbursementTransferInfoRdp2p.getAccountCredit()!=null && loanDisbursementTransferInfoRdp2p.getAccountCredit().length()>0) {
//								
//								loanDAOFactory.getLoanDisbursementTransferInfoRdp2pDAO().save(loanDisbursementTransferInfoRdp2p, session);
//								idInfo = loanDisbursementTransferInfoRdp2p.getLoanDisbursementTransferInfoRdp2pId();
//								
//								//loan.setLoanDisbursementTransferInfoId(loanDisbursementTransferInfo.getLoanDisbursementTransferInfoId());
//								//loanDAOFactory.getLoanDAO().update(loan, session);
//								log.info("loanId >>> "+loan.getLoanId()+ " idInfo >>> "+idInfo);
//								loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoRdp2pId(loan.getLoanId(), idInfo, session);
//								
//							}
//						}	
//							
//						transaction.commit();
//						
//					} catch (Exception ex) {
//						if (transaction!=null) transaction.rollback();
//						ex.printStackTrace();
//					} finally {
//						if (session!=null) {
//							session.close(); session=null;
//						}
//					}
//					
//					// ambil loan_transfer_info
//					log.info("loanId: "+loan.getLoanId()+" idInfo: "+idInfo);
//					if (idInfo!=null && idInfo>0) {
//						loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoRdp2pId(loan.getLoanId(), idInfo);
//						idInfo = null;
//					} else {
//						log.info("=== loan : "+loan.getLoanId()+" tc : "+loan.getTransactionCode()+" idInfo null!!!");
//					}
//					
//				}	
//			}
//			
//			loans = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("loanId"), 
//					Restrictions.eq("fundingStatus", LoanFundingStatus.FULL),
//					Restrictions.eq("isRdp2p", true),
//					Restrictions.eq("disbursementStatus", DisbursementStatus.SUCCESS.toString()),
//					Restrictions.isNull("loanDisbursementTransferInfoId"));
//			
//			BankAccount bankAccount = null;
//			totLoan = loans.size()>0 ? loans.size() : 0;
//			log.info("loan count() >> "+totLoan);
//			session = null; 
//			transaction = null;
//			idInfo = null; 
//			for (Loan loan : loans) {
//				// pengecekan sebelum insert, pastikan tidak ada data loan.getTransactionCode() yg double
//				LoanDisbursementTransferInfo cekData = loanDAOFactory.getLoanDisbursementTransferInfoDAO().findByCriteria(Restrictions.eq("accountCreditCode", loan.getTransactionCode()));
//				log.info("cekData : "+cekData);
//				if (cekData != null) {
//					
//					log.info("AWAS sudah ada data loan : "+loan.getTransactionCode());
//					loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoId(loan.getLoanId(), cekData.getLoanDisbursementTransferInfoId());
//					log.info("Update loan::loanId > "+loan.getLoanId()+" with infoId > "+cekData.getLoanDisbursementTransferInfoId());
//					
//				} else {
//					// new session
//					session = loanDAOFactory.getLoanDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
//					try {
//						// start transaction
//						transaction = session.beginTransaction(); 
//								
//						// only for funding_status is TRANSFERED
//						if (loan.getFundingStatus()==LoanFundingStatus.FULL && (loan.getTotal() != null && loan.getTotal().compareTo(new BigDecimal(0)) >0)) {
//							
//							// search on table PrepaidReseller, for now prepaidReseller only
//							LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findByCriteria(session, Restrictions.eq("loanProductId", loan.getProductId()));
//									
//							LoanDisbursementTransferInfo loanDisbursementTransferInfo = new LoanDisbursementTransferInfo(); 
//							loanDisbursementTransferInfo.setAccountDebit(tokomodalBankAccount.getAccountNumber());
//							loanDisbursementTransferInfo.setAccountDebitCode("TMU");
//							loanDisbursementTransferInfo.setAccountDebitName(tokomodalBankAccount.getAccountName());
//							loanDisbursementTransferInfo.setAmount(loan.getTotal());
//							log.info("loanId : "+loan.getLoanId()+" productId : "+loan.getProductId()+" >> "+loanProduct.isPrepaidReseller());
//							if (loanProduct.isPrepaidReseller()) {
//								
//								PrepaidReseller prepaidReseller = accountDAOFactory.getPrepaidResellerDAO().findByCriteria(session, Restrictions.eq("accountId", loan.getAccountId()));
//								if (prepaidReseller != null) {
//									PrepaidProvider prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findByCriteria(session, Restrictions.eq("prepaidProviderId", prepaidReseller.getProvider().getPrepaidProviderId()));
//									if (prepaidProvider != null) {
//										
//										// rek prepaid-provider! or borrower
//										bankAccount = null;
//										/*if (prepaidProvider.getBankTransfer() != null && prepaidProvider.getBankTransfer() == true) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("prepaidProviderId", prepaidProvider.getPrepaidProviderId()));
//										else bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.eq("accountId", loan.getAccountId()));*/
//										if (prepaidProvider.getTransfer()==PrepaidProviderTransfer.PROVIDER_BANK_ACCOUNT) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(session, Restrictions.eq("prepaidProviderId", prepaidProvider.getPrepaidProviderId()));
//										else if (prepaidProvider.getTransfer()==PrepaidProviderTransfer.BORROWER_BANK_ACCOUNT) bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(session, Restrictions.eq("accountId", loan.getAccountId()));
//										// TODO 
//										// VA
//										if (bankAccount != null) {
//											
//											// sementara hanya yang bank sinarmas
//											Lookup lookup = basicDAOFactory.getLookupDAO().findById(bankAccount.getBank().getLookupId(), session);
//											log.info(lookup.getCode());
//											if (lookup !=null) {
//												// bank sinarmas
//												if (lookup.getCode().trim().startsWith("153")) {
//													loanDisbursementTransferInfo.setAccountCredit(bankAccount.getAccountNumber());
//													loanDisbursementTransferInfo.setAccountCreditCode(loan.getTransactionCode());
//													loanDisbursementTransferInfo.setAccountCreditName(bankAccount.getAccountName());
//													loanDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.INTERNAL);
//													loanDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
//												} else {
//													// non bank sinarmas
//													if (lookup.getMemberCode() != null) {
//														loanDisbursementTransferInfo.setAccountCredit(bankAccount.getAccountNumber());
//														loanDisbursementTransferInfo.setAccountCreditCode(loan.getTransactionCode());
//														loanDisbursementTransferInfo.setAccountCreditName(bankAccount.getAccountName());
//														loanDisbursementTransferInfo.setBankDirectTransferType(BankDirectTransferType.EXTERNAL);
//														loanDisbursementTransferInfo.setBankMemberCode(lookup.getMemberCode());
//													}
//												}
//											}	
//										}
//									}
//								}
//								
//							}	
//							
//							// make sure only account_credit is not null are saved
//							if (loanDisbursementTransferInfo.getAccountCredit()!=null && loanDisbursementTransferInfo.getAccountCredit().length()>0) {
//								
//								loanDAOFactory.getLoanDisbursementTransferInfoDAO().save(loanDisbursementTransferInfo, session);
//								idInfo = loanDisbursementTransferInfo.getLoanDisbursementTransferInfoId();
//								
//								//loan.setLoanDisbursementTransferInfoId(loanDisbursementTransferInfo.getLoanDisbursementTransferInfoId());
//								//loanDAOFactory.getLoanDAO().update(loan, session);
//								log.info("loanId >>> "+loan.getLoanId()+ " idInfo >>> "+idInfo);
//								loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoId(loan.getLoanId(), idInfo, session);
//								
//							}
//						}	
//							
//						transaction.commit();
//						
//					} catch (Exception ex) {
//						if (transaction!=null) transaction.rollback();
//						ex.printStackTrace();
//					} finally {
//						if (session!=null) {
//							session.close(); session=null;
//						}
//					}
//					
//					// ambil loan_transfer_info
//					log.info("loanId: "+loan.getLoanId()+" idInfo: "+idInfo);
//					if (idInfo!=null && idInfo>0) {
//						loanDAOFactory.getLoanDAO().updateLoanDisbursementTransferInfoId(loan.getLoanId(), idInfo);
//						idInfo = null;
//					} else {
//						log.info("=== loan : "+loan.getLoanId()+" tc : "+loan.getTransactionCode()+" idInfo null!!!");
//					}
//					
//				} //
//				
//			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}
}
