package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class AccountBalanceEodJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		AccountBalanceEodTask accountBalanceEodTask = new AccountBalanceEodTask();
		accountBalanceEodTask.calculateAccountBalanceDaily();
		
	}
	

}
