package com.danamas.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;


import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mpe.amortization.model.LoanAmortization;
import com.mpe.amortization.model.dao.AmortizationDAOFactory;
import com.mpe.amortization.model.dao.AmortizationDAOFactoryHibernate;
import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.Lookup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;
import com.mpe.common.util.Constants;
import com.mpe.common.util.Formater;
import com.mpe.message.model.OutgoingEmail;
import com.mpe.message.model.dao.MessageDAOFactory;
import com.mpe.message.model.dao.MessageDAOFactoryHibernate;
import com.mpe.upload.model.dao.UploadDAOFactory;
import com.mpe.upload.model.dao.UploadDAOFactoryHibernate;
import com.tokomodal.account.model.Account;
import com.tokomodal.account.model.AccountType;
import com.tokomodal.account.model.BankAccount;
import com.tokomodal.account.model.Personal;
import com.tokomodal.account.model.PrepaidProvider;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.account.model.other.Gender;
import com.tokomodal.ar.model.GiroBiller;
import com.tokomodal.ar.model.dao.ArDAOFactory;
import com.tokomodal.ar.model.dao.ArDAOFactoryHibernate;
import com.tokomodal.loan.model.AccountBalanceHistory;
import com.tokomodal.loan.model.BankMethod;
import com.tokomodal.loan.model.CashIn;
import com.tokomodal.loan.model.CashInStatus;
import com.tokomodal.loan.model.InterestType;
import com.tokomodal.loan.model.KewajibanLain;
import com.tokomodal.loan.model.Loan;
import com.tokomodal.loan.model.LoanFunding;
import com.tokomodal.loan.model.LoanFundingStatus;
import com.tokomodal.loan.model.LoanPaymentStatus;
import com.tokomodal.loan.model.LoanProduct;
import com.tokomodal.loan.model.LoanStatus;
import com.tokomodal.loan.model.Payment;
import com.tokomodal.loan.model.PaymentDetail;
import com.tokomodal.loan.model.PaymentStatus;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.loan.model.other.AccountBalanceHistoryTransactionType;
import com.tokomodal.loan.model.other.EmailNotificationTo;

public class GiroBillerTask {
	
	//Log log = LogFactory.getFactory().getInstance(this.getClass());
	public Logger log = Logger.getLogger(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	MessageDAOFactory messageDAOFactory = MessageDAOFactory.instance(MessageDAOFactoryHibernate.class);
	UploadDAOFactory uploadDAOFactory = UploadDAOFactory.instance(UploadDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	ArDAOFactory arDAOFactory = ArDAOFactory.instance(ArDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	AmortizationDAOFactory amortizationDAOFactory = AmortizationDAOFactory.instance(AmortizationDAOFactoryHibernate.class);
	
	
	
	public void insertCashIn() {
		try {
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.set(Calendar.YEAR,1970);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.MINUTE, -5);
			
			Calendar cal02 = new GregorianCalendar();
			cal02.setTime(new Date());
			cal02.set(Calendar.YEAR,1970);
			cal02.set(Calendar.MONTH, 0);
			cal02.set(Calendar.DAY_OF_MONTH, 1);
			cal02.add(Calendar.MINUTE, -1);
			cal02.set(Calendar.MILLISECOND, 0);
			
			Calendar cal2 = new GregorianCalendar();
			cal2.setTime(new Date());
			cal2.set(Calendar.HOUR_OF_DAY,0);
			cal2.set(Calendar.MINUTE,0);
			cal2.set(Calendar.SECOND,0);
			cal2.set(Calendar.MILLISECOND,0);
			
			List<GiroBiller> giroBillers = arDAOFactory.getGiroBillerDAO().findByCriteria(Order.asc("giroBillerId"), 5, Restrictions.eq("posted", Boolean.FALSE),Restrictions.eq("reversal", Boolean.FALSE), Restrictions.isNull("loanProductId"),
					Restrictions.or(Restrictions.lt("transactionDate", cal2.getTime()), 
							Restrictions.and(Restrictions.eq("transactionDate", cal2.getTime()), 
							Restrictions.lt("transactionTime", cal.getTime())))
					);
			//3-MAY-2017 ALL PAYMENT METHOD -5 MINUTES
			/*Object[] o = {cal2.getTime(), cal2.getTime(), cal02.getTime(), cal15.getTime()};
			Type[] t = {StandardBasicTypes.DATE, StandardBasicTypes.DATE, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.TIMESTAMP};
			String sqlR = " ( transaction_date < ? OR (transaction_date = ? AND transaction_time <=  (case when (institution_code = '153') then ? else ? end )) ) ";
			List<GiroBiller> giroBillers = arDAOFactory.getGiroBillerDAO().findByCriteria(Order.asc("giroBillerId"), 5, Restrictions.eq("posted", Boolean.FALSE),Restrictions.eq("reversal", Boolean.FALSE), Restrictions.isNull("loanProductId"),
					Restrictions.sqlRestriction(sqlR, o, t)
					);*/
			log.info(" [ start cash-in giro biller >>>> "+giroBillers.size()+" ] ");
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			for (GiroBiller giroBiller :giroBillers) {
				
				// cek simas.NET BSM ACC-NUMBER?
				if (applicationSetup.getClaimSimasNetBsmAccNumber()!=null && applicationSetup.getClaimSimasNetBsmAccNumber().length()>0 && giroBiller.getFromAccountNumber()!=null && giroBiller.getFromAccountNumber().length()>0 && applicationSetup.getClaimSimasNetBsmAccNumber().equalsIgnoreCase(giroBiller.getFromAccountNumber())) {
					
					// regular cash-IN
					CashIn cashIn = new CashIn();
					cashIn.setAccountId(giroBiller.getAccountId());
					BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
					//BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findById(payment.getToBankAccountId());
					if (bankAccount!=null) {
						cashIn.setBankId(bankAccount.getBank().getLookupId());
						cashIn.setAccountName(bankAccount.getAccountName());
						cashIn.setAccountNumber(bankAccount.getAccountNumber());
						cashIn.setToBankAccountId(bankAccount.getBankAccountId());
					}
					cashIn.setAmount(giroBiller.getTransactionAmount());
					cashIn.setCashInDate(giroBiller.getTransactionDate());
					cashIn.setConfirmedDate(giroBiller.getTransactionDate());
					cashIn.setCreateBy("GIRO-BILLER");
					cashIn.setCreateOn(new Date());
					cashIn.setMethod("GIRO_BILLER");
					cashIn.setNote("Claim Simas.NET");
					cashIn.setOtherIncome(new BigDecimal(0));
					cashIn.setStatus(CashInStatus.CONFIRMED);
					Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
					String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("CASH IN", l, cashIn.getCashInDate(), true);
					cashIn.setTransactionCode(s);
					cashIn.setUploadFileId(null);

					loanDAOFactory.getCashInDAO().save(cashIn);
					
					//
					giroBiller.setPosted(true);
					arDAOFactory.getGiroBillerDAO().update(giroBiller);
					
				} else {
					// regular cash-IN
					CashIn cashIn = new CashIn();
//					AccountBalanceHistory accountBalanceHistory = new AccountBalanceHistory();;
					Account account = accountDAOFactory.getAccountDAO().findByCriteria(Restrictions.eq("accountId", giroBiller.getAccountId()));

					cashIn.setAccountId(giroBiller.getAccountId());
					
					/*AccountBalanceHistory*/
					/*
					if(account.getIsMigrated() != null && account.getIsMigrated()) {
						accountBalanceHistory.setAccountId(giroBiller.getAccountId());
					}
					
					if(account.getIsRdp2p() != null && account.getIsRdp2p()) {
						cashIn.setIsRdp2p(true);
						cashIn.setIsPosted(false);
					}*/
					
					BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
					//BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findById(payment.getToBankAccountId());
					if (bankAccount!=null) {
						cashIn.setBankId(bankAccount.getBank().getLookupId());
						cashIn.setAccountName(bankAccount.getAccountName());
						cashIn.setAccountNumber(bankAccount.getAccountNumber());
						cashIn.setToBankAccountId(bankAccount.getBankAccountId());
					}
					cashIn.setAmount(giroBiller.getTransactionAmount());
					cashIn.setCashInDate(giroBiller.getTransactionDate());
					cashIn.setConfirmedDate(giroBiller.getTransactionDate());
					cashIn.setCreateBy("GIRO-BILLER");
					cashIn.setCreateOn(new Date());
					cashIn.setMethod("GIRO_BILLER");
					cashIn.setNote("Deposit dana");
					cashIn.setOtherIncome(new BigDecimal(0));
					cashIn.setStatus(CashInStatus.CONFIRMED);
					
					/*AccountBalanceHistory*/
					
					/*
					if(account.getIsMigrated() != null && account.getIsMigrated()) {
						accountBalanceHistory.setAmount(giroBiller.getTransactionAmount());
						accountBalanceHistory.setType("Kredit");
						accountBalanceHistory.setNote("Deposit dana");
						accountBalanceHistory.setTransactionType(AccountBalanceHistoryTransactionType.CASH_IN);
						accountBalanceHistory.setBalanceDate(cashIn.getCreateOn());
						BigDecimal availableCashBefore = loanDAOFactory.getAccountBalanceHistoryDAO().calculateAvailableCash(giroBiller.getAccountId());
						accountBalanceHistory.setAvailableCash(availableCashBefore.add(giroBiller.getTransactionAmount()));
						
						account.setAvailableCash(accountBalanceHistory.getAvailableCash());
					}*/
					Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
					String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("CASH IN", l, cashIn.getCashInDate(), true);
					cashIn.setTransactionCode(s);
					cashIn.setUploadFileId(null);
					loanDAOFactory.getCashInDAO().save(cashIn);
					
					/*AccountBalanceHistory*/
					/*if(account.getIsMigrated() != null && account.getIsMigrated()) {
						accountBalanceHistory.setKeyId(cashIn.getCashInId());
						loanDAOFactory.getAccountBalanceHistoryDAO().save(accountBalanceHistory);
						accountDAOFactory.getAccountDAO().update(account);
					}*/
					//ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
					
					if (applicationSetup!=null && applicationSetup.getMobileNotificationCashInUrl()!=null && applicationSetup.getMobileNotificationCashInUrl().length()>0) {
			    		try {
							//String USER_AGENT = "Mozilla/5.0";
							//SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
							
							//String email = accountDAOFactory.getAccountDAO().getEmailByAccountId(cashIn.getAccountId());
							String mobile = accountDAOFactory.getAccountDAO().getMobileByAccountId(cashIn.getAccountId());
							/*email = StringUtils.replace(email, "@", "_at_");
							email = StringUtils.replace(email, ".", "_dot_");*/
							// http://10.2.2.146:8080/p2p/push/newloan/aming_at_aming_dot_com/reseller_pulsa/1000000?callback=
							//String url = applicationSetup.getMobileNotificationUrl()+ email+"/"+name+"/"+product+"/"+Formater.getFormatedOutputForm(0, loan.getAmount().doubleValue())+"?callback=";
							//String url = applicationSetup.getMobileNotificationCashInUrl() + email +"/"+ cashIn.getCreateOn().getTime() +"/"+ Formater.getFormatedOutputForm(0, cashIn.getAmount().doubleValue()) +"?callback=";
							//String url = applicationSetup.getMobileNotificationCashInUrl() + "e=" + email +"&w="+ cashIn.getCreateOn().getTime() +"&a"+ Formater.getFormatedOutputForm(0, cashIn.getAmount().doubleValue()) + "&lang=en";
							//HttpClient clientX = HttpClientBuilder.create().build();
							//log.info(url);
							//HttpGet request = new HttpGet(url);
							/*int CONNECTION_TIMEOUT_MS = 60 * 1000; // Timeout in millis.
							RequestConfig requestConfig = RequestConfig.custom()
								    .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
								    .setConnectTimeout(CONNECTION_TIMEOUT_MS)
								    .setSocketTimeout(CONNECTION_TIMEOUT_MS)
								    .build();
							request.setConfig(requestConfig);*/
							// TODO

							// add request header
							/*request.addHeader("User-Agent", USER_AGENT);
							HttpResponse response = clientX.execute(request);*/
							
							Object[] obj = new Object[4];
							obj[0] = mobile.trim();
							obj[1] = cashIn.getCreateOn().getTime();
							obj[2] = Formater.getFormatedOutputForm(0, cashIn.getAmount().doubleValue());
							obj[3] = "en";
							
							String[] response = JSONService.postDepositNotification(applicationSetup.getMobileNotificationCashInUrl(), 90000, obj);

							log.info("Response Code : " + response[0]);

						} catch (Exception e) {
							e.printStackTrace();
						}
			    	}
					
					//
					giroBiller.setPosted(true);
					arDAOFactory.getGiroBillerDAO().update(giroBiller);
					
					//INSERT OUTGOING EMAIL
					//Long prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(giroBiller.getAccountId());
					Properties p2 = new Properties();
					ClassLoader loader = GiroBillerTask.class.getClassLoader();
				    URL r = loader.getResource("/");
				    String pathCls = r.getPath();
				    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
					p2.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
					Velocity.init(p2);
					
					/*PrepaidProvider prepaidProvider = null;
					Session session = null;
					try {
						session = accountDAOFactory.getPrepaidProviderDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
						prepaidProvider = (PrepaidProvider)session.createCriteria(PrepaidProvider.class)
								.setFetchMode("prepaidVouchers", FetchMode.JOIN)
								.setFetchMode("contact", FetchMode.JOIN)
								.add(Restrictions.eq("prepaidProviderId", new Long(prepaidProviderId)))
								//.setCacheable(true)
								.uniqueResult();
					} finally {
						if (session!=null) session.close();
					}*/
					
					try {
						
						
						StringWriter w2 = new StringWriter();
						//BUAT PROD
						VelocityContext context = new VelocityContext();
						String name = accountDAOFactory.getAccountDAO().getNameByAccountId(giroBiller.getAccountId());
						String email = accountDAOFactory.getAccountDAO().getEmailByAccountId(giroBiller.getAccountId());
						context.put("name", name);
						context.put("amount", Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue()));
						
						if (account.getType()==AccountType.PERSONAL) {
							Personal personal = (Personal)account;
							if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
							else context.put("salutation", "Ibu/Sdri.");
						} else {
							context.put("salutation", "");
						}
						
						Velocity.mergeTemplate("/template/cashInByVirtualAccount.vm", context, w2 );
												
						// save to email
						OutgoingEmail outgoingEmail2 = new OutgoingEmail();
						outgoingEmail2.setEmailDate(new Date());
						outgoingEmail2.setSender("Tokomodal - Customer Service <cs@tokomodal.co.id>");
						outgoingEmail2.setSubject("[Tokomodal] Pemberitahuan Setor Dana Tanggal "+Formater.getFormatedDate(new Date(), "dd-MMM-yyyy"));
						outgoingEmail2.setTo(email);
						outgoingEmail2.setMessage(w2.toString());
						outgoingEmail2.setType("[Tokomodal] Pemberitahuan Setor Dana");
						
						messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail2);
						
						/*
						 * SEND PUSH NOTIF
						 */
						if(account.getTokenFcm() != null && account.getTokenFcm().length() > 0) {
//						if(1==1) {
							PropertiesConfiguration config = new PropertiesConfiguration("ftp.properties");
							String appId = config.getString("appId");
							
							JSONObject body = new JSONObject();
							body.put("app_id", appId);
							
							JSONObject headings = new JSONObject();
							headings.put("id", "Setor Dana Berhasil!");
							headings.put("en", "Cash In Success!");
//							--headings.put("en", "There are " + loans.size() + " 	yang belum diberikan modal saat ini. \nBerikan modal yuk!");
							
							JSONObject contents = new JSONObject();
							contents.put("id", "Setor Dana sebesar Rp. " + Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue()) + " berhasil. Happy funding!");
							contents.put("en", "Cash In with amount Rp. " + Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue()) + " success. Happy funding!");
							
							body.put("headings", headings);
							body.put("contents", contents);
							body.put("small_icon", "tokomodal_logo");
							body.put("large_icon", "https://tokomodal.co.id/assets/img/logo-v.png");
//							body.put("included_segments", "Active Users");
							
							JSONArray includePlayerIds = new JSONArray();
							includePlayerIds.put(account.getTokenFcm());
//							includePlayerIds.put("4e002370-b7fa-4206-a218-5216ff709068");
							body.put("include_player_ids", includePlayerIds);
							body.put("android_group", "cash-in");
							
							JSONArray button = new JSONArray();
							
							JSONObject button1 = new JSONObject();
							button1.put("id", "goHome");
							button1.put("text", "Check it out!");
							
							button.put(button1);
							body.put("buttons", button);
							
							
							/*PUSH TO ONESIGNAL*/
//							URL url = new URL("https://onesignal.com/api/v1/notifications");
//							HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//							conn.setDoOutput(true);
//							conn.setRequestMethod("POST");
//							conn.setRequestProperty("Content-Type", "application/json");
//							conn.setRequestProperty("Authorization", "Basic MTJjNDEzNWMtMjcwMy00MWMzLTk5MDAtZjU2MWI1ZWQ4MmVk");
//							conn.setDoOutput(true);
							
//							String input = body.toString();
//							
//							OutputStream os = conn.getOutputStream();
//							os.write(input.getBytes());
//							os.flush();
//							os.close();
//
//							int responseCode = conn.getResponseCode();
//							
//
//							BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//							String inputLine;
//							StringBuffer response = new StringBuffer();
//
//							while ((inputLine = in.readLine()) != null) {
//							    response.append(inputLine);
//							}
//							in.close();
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void insertPaymentMultiLoan() {
		try {
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.set(Calendar.YEAR,1970);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.MINUTE, -15);
			
			Calendar cal2 = new GregorianCalendar();
			cal2.setTime(new Date());
			cal2.set(Calendar.HOUR_OF_DAY,0);
			cal2.set(Calendar.MINUTE,0);
			cal2.set(Calendar.SECOND,0);
			cal2.set(Calendar.MILLISECOND,0);
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			List<GiroBiller> giroBillers = arDAOFactory.getGiroBillerDAO().findByCriteria(
					Order.asc("giroBillerId"), 10, Restrictions.eq("posted", Boolean.FALSE),Restrictions.eq("reversal", Boolean.FALSE), Restrictions.isNotNull("loanProductId")
					, 
					Restrictions.or(Restrictions.lt("transactionDate", cal2.getTime()), 
							Restrictions.and(Restrictions.eq("transactionDate", cal2.getTime()), 
							//Restrictions.sqlRestriction("trunc(transaction_time) < trunc(current_time)")))
							Restrictions.lt("transactionTime", cal.getTime()))
							)
					);
			
			/*for (GiroBiller giroBiller :giroBillers) {
				System.out.println("list girobiller");
				System.out.println(giroBiller.getGiroBillerId());
				System.out.println(giroBiller.getTransactionDate());
				System.out.println(giroBiller.getTransactionTime());
				System.out.println(giroBiller.getTransmissionDate());
			}
			System.out.println("transaction datea1111111111111"+cal2.getTime());
			System.out.println("transaction time22222222222222"+cal.getTime());*/
			log.info(" [ start payment loan multi giro biller >>>> "+giroBillers.size()+" ] "); 
			
			for (GiroBiller giroBiller :giroBillers) {
				List<Loan> loans = loanDAOFactory.getLoanDAO().findByCriteria(Order.asc("overDueDate"), // old Order.asc("loanId")
						Restrictions.eq("productId", giroBiller.getLoanProductId()), Restrictions.eq("accountId", giroBiller.getAccountId()),
						Restrictions.eq("status", LoanStatus.APPROVED), Restrictions.eq("fundingStatus", LoanFundingStatus.TRANSFERED),
						Restrictions.or(Restrictions.ne("paymentStatus", LoanPaymentStatus.CLOSED), Restrictions.isNull("paymentStatus")),
						giroBiller.getLoanId()!=null?Restrictions.eq("loanId", giroBiller.getLoanId()):Restrictions.ne("loanId", new Long(-1))		
						);
				//
				LoanProduct loanProduct = loanDAOFactory.getLoanProductDAO().findById(giroBiller.getLoanProductId());
				Account account = null;
				Session session = null;
				try { 
					session = accountDAOFactory.getAccountDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
					account = (Account)session.createCriteria(Account.class)
							.setFetchMode("address", FetchMode.JOIN)
							.setFetchMode("businessAddress", FetchMode.JOIN)
							.setFetchMode("contact", FetchMode.JOIN)
							.add(Restrictions.eq("accountId", giroBiller.getAccountId()))
							.setMaxResults(1)
							.uniqueResult();
				} finally {
					if (session!=null) session.close();
				}
				// for loan-number payment record!
				String loanNumbers = "";
				BigDecimal paymentAmount = giroBiller.getTransactionAmount();
				if (loans==null || (loans!=null && loans.size()==0)) {
					Payment payment = new Payment();
					payment.setGiroBillerId(giroBiller.getGiroBillerId());
					KewajibanLain kewajibanLain = null;
					kewajibanLain = new KewajibanLain();
					kewajibanLain.setGiroBillerId(giroBiller.getGiroBillerId());
					kewajibanLain.setAccountId(giroBiller.getAccountId());
					kewajibanLain.setAmount(paymentAmount);
					kewajibanLain.setCreateBy("GIRO-BILLER");
					kewajibanLain.setCreateOn(new Date());
					kewajibanLain.setFromVirtualAccount(giroBiller.getFromAccountNumber());
					// TODO =====> kenapa NOL bukan NULL???
					//kewajibanLain.setLoanId(nu);
					LocalDate datePart = new LocalDate(giroBiller.getTransactionDate());
					LocalTime timePart = new LocalTime(giroBiller.getTransactionTime());
					LocalDateTime dateTime = datePart.toLocalDateTime(timePart);
					kewajibanLain.setPaymentDateTime(dateTime.toDate());
					//kewajibanLain.setFromPaymentId(fromPaymentId);
					kewajibanLain.setFromPaymentNote("Pembayaran VA dari peminjam dengan pinjaman lunas dari "+giroBiller.getFromAccountNumber()+" sebesar "+Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue())+" pada "+CommonUtil.getStringFromDate(giroBiller.getTransactionDate(), "dd MMM yyyy")+" "+Formater.getFormatedTime(giroBiller.getTransactionTime()));
					BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
					//BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findById(payment.getToBankAccountId());
					if (bankAccount!=null) {
						payment.setBankId(bankAccount.getBank().getLookupId());
						payment.setAccountName(bankAccount.getAccountName());
						payment.setAccountNumber(bankAccount.getAccountNumber());
						payment.setToBankAccountId(bankAccount.getBankAccountId());
					}
					payment.setTotalPaymentAmount(paymentAmount);
					payment.setAmount(paymentAmount);
					
					//payment.setNote("Pembayaran pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
					payment.setCreateBy("GIRO-BILLER");
					payment.setCreateOn(new Date());
					//payment.setLoanId(giroBiller.getLoanId());
					payment.setMethod(BankMethod.ATM);
					payment.setPaymentDate(giroBiller.getTransactionDate());
					//
					payment.setPaymentDateTime(dateTime.toDate());
					//
					payment.setStatus(PaymentStatus.CONFIRMED);
					//payment.setTotalPaymentAmount(giroBiller.getTransactionAmount());
					Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
					String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("PAYMENT", l, payment.getPaymentDate(), true);
					payment.setTransactionCode(s);
					
					//ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
					/*if (applicationSetup.getNumberOfRandomNumber() > 0) {
						BigDecimal a = payment.getAmount().remainder(new BigDecimal(1000));
						payment.setAmount(payment.getAmount().subtract(a));
						payment.setOtherIncome(a);
					}*/
					payment.setUploadFileId(null);
					loanDAOFactory.getPaymentDAO().save(payment);
					
					// save kewajiban lain!
					if (kewajibanLain!=null) {
						//kewajibanLain.setPaymentDateTime(paymentDateTime);
						kewajibanLain.setFromPaymentId(payment.getPaymentId());
						loanDAOFactory.getKewajibanLainDAO().save(kewajibanLain);
					}
				} else {
					if (loanProduct.getInstallment()==null || (loanProduct.getInstallment()!=null && !loanProduct.getInstallment())) {
						KewajibanLain kewajibanLain = null;
						Long lastPaymentId = null;
						Long lastLoanId = new Long(0);
						for (Loan loan : loans) {
							Payment payment = new Payment();
							payment.setGiroBillerId(giroBiller.getGiroBillerId());
							BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
							//BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findById(payment.getToBankAccountId());
							if (bankAccount!=null) {
								payment.setBankId(bankAccount.getBank().getLookupId());
								payment.setAccountName(bankAccount.getAccountName());
								payment.setAccountNumber(bankAccount.getAccountNumber());
								payment.setToBankAccountId(bankAccount.getBankAccountId());
							}
							if (loan!=null) {
								BigDecimal[] b = loanDAOFactory.getPaymentDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
								BigDecimal previousPayment = b[0];
								BigDecimal outstandingPenalty = b[1];
								BigDecimal outstandingAmount = new BigDecimal(0);
								BigDecimal penalty = new BigDecimal(0);
								BigDecimal voucherAmount = loanDAOFactory.getLoanDAO().getVoucherAmountByLoanId(loan.getLoanId());
								//
								
								
								if (previousPayment==null) previousPayment = new BigDecimal(0);
								Date lastPayDate = loanDAOFactory.getPaymentDAO().getLastPaymentDateLoanId(loan.getLoanId());
								//overDueDate = loan.getOverDueDate().getTime();
								//lastPaymentDate = lastPayDate!=null?lastPayDate.getTime():0;
								BigDecimal loanAmount = loan.getAmount();
								// interest
								if (loan.getType()==InterestType.ARREAR) {
									loanAmount = loanAmount.add((loanAmount.multiply(loan.getInterest())).divide(new BigDecimal(100)));
								}
								//DO DO DO DO
								
								/*	Diganti dengan girobiller transaction date
								 * LocalDate d1 = new LocalDate(new Date().getTime());
								 * */
								LocalDate d1 = new LocalDate(giroBiller.getTransactionDate().getTime());
								LocalDate d2 = new LocalDate(loan.getOverDueDate().getTime());
								d2 = d2.plusDays(loan.getGracePeriode());
								BigDecimal unconfirmPayment = loanDAOFactory.getPaymentDAO().getUnconfirmPaymentByLoanId(loan.getLoanId());
								if (unconfirmPayment==null) unconfirmPayment = new BigDecimal(0);
								if (d1.isAfter(d2)) {
									// terlambat
									int d = 0;
									if (lastPayDate==null) {
										//belum pernah bayar
										d2 = d2.minusDays(loan.getGracePeriode());
										d = Days.daysBetween(d2, d1).getDays();
									} else {
										//sudah pernah bayar
										LocalDate d3 = new LocalDate(lastPayDate.getTime());
										if (d3.isAfter(d2)){
											//last payment > over due
											d = Days.daysBetween(d3, d1).getDays();
										}else {
											//last payment < over due
											d2 = d2.minusDays(loan.getGracePeriode());
											d = Days.daysBetween(d2, d1).getDays();
										}
										
										
									}
									outstandingAmount = loanAmount.subtract(previousPayment).subtract(unconfirmPayment);
									penalty = (((outstandingAmount.multiply(loan.getPenalty())).divide(new BigDecimal(100))).multiply(new BigDecimal(d))).setScale(0, BigDecimal.ROUND_HALF_UP);
									//total = (outstandingAmount.add(penalty)).add(outstandingPenalty);
									
									//System.out.println("penalty >> "+d+" // "+penalty);
									
								} else {
									penalty = new BigDecimal(0);
									outstandingAmount = loanAmount.subtract(previousPayment).subtract(unconfirmPayment);
									//total = outstandingAmount.add(penalty);
								}
								//total = (outstandingAmount.add(penalty)).add(outstandingPenalty);
								if (loan!=null) payment.setLoanId(loan.getLoanId());
								payment.setPenalty(penalty);
								//System.out.println("paymentAmount >> "+paymentAmount+"//"+outstandingAmount+"//"+outstandingPenalty);
								
								if(voucherAmount != null && voucherAmount.compareTo(new BigDecimal(0)) != 0) {
									
									if (paymentAmount.compareTo((outstandingAmount.add(penalty).add(outstandingPenalty).subtract(voucherAmount)))>=0) {
										//log.info("paymentAmount > outstandingAmount+penalty+outstandingPenalty   "+paymentAmount);
										payment.setTotalPaymentAmount((outstandingAmount.add(penalty)).add(outstandingPenalty));
										payment.setAmount(outstandingAmount);
										payment.setPenaltyPayment(penalty.add(outstandingPenalty));
										// kurangi
										paymentAmount = paymentAmount.subtract((outstandingAmount.add(penalty).add(outstandingPenalty)));
										/*if (outstandingPenalty.compareTo(BigDecimal.ZERO)>0){
											payment.setPenaltyPayment(outstandingPenalty);
											payment.setTotalPaymentAmount(payment.getTotalPaymentAmount().add(outstandingPenalty));
										}*/
										//System.out.println("A");
									} else {
										//log.info("paymentAmount < outstandingAmount+penalty+outstandingPenalty   "+paymentAmount);
										payment.setTotalPaymentAmount(paymentAmount);
										if ((paymentAmount.subtract(outstandingAmount)).compareTo(BigDecimal.ZERO)>0) {
											payment.setAmount(outstandingAmount);
											payment.setPenaltyPayment(paymentAmount.subtract(outstandingAmount));
										} else {
											payment.setAmount(paymentAmount);
											payment.setPenaltyPayment(new BigDecimal(0));
										}
										paymentAmount = new BigDecimal(0);
										//System.out.println("B");
									}
								} else {
									if (paymentAmount.compareTo((outstandingAmount.add(penalty).add(outstandingPenalty)))>=0) {
										//log.info("paymentAmount > outstandingAmount+penalty+outstandingPenalty   "+paymentAmount);
										payment.setTotalPaymentAmount((outstandingAmount.add(penalty)).add(outstandingPenalty));
										payment.setAmount(outstandingAmount);
										payment.setPenaltyPayment(penalty.add(outstandingPenalty));
										// kurangi
										paymentAmount = paymentAmount.subtract((outstandingAmount.add(penalty).add(outstandingPenalty)));
										/*if (outstandingPenalty.compareTo(BigDecimal.ZERO)>0){
											payment.setPenaltyPayment(outstandingPenalty);
											payment.setTotalPaymentAmount(payment.getTotalPaymentAmount().add(outstandingPenalty));
										}*/
										//System.out.println("A");
									} else {
										//log.info("paymentAmount < outstandingAmount+penalty+outstandingPenalty   "+paymentAmount);
										payment.setTotalPaymentAmount(paymentAmount);
										if ((paymentAmount.subtract(outstandingAmount)).compareTo(BigDecimal.ZERO)>0) {
											payment.setAmount(outstandingAmount);
											payment.setPenaltyPayment(paymentAmount.subtract(outstandingAmount));
										} else {
											payment.setAmount(paymentAmount);
											payment.setPenaltyPayment(new BigDecimal(0));
										}
										paymentAmount = new BigDecimal(0);
										//System.out.println("B");
									}
								}
								
								
								
								//
								if (payment.getPenaltyPayment()!=null && payment.getPenaltyPayment().compareTo(BigDecimal.ZERO)>0) {
									BigDecimal a = payment.getPenaltyPayment();
									a = loanDAOFactory.getLoanDAO().penaltyPayment(payment.getLoanId(), a);
									if (a.compareTo(BigDecimal.ZERO)>=0) {
										payment.setPenaltyPayment(a);
									}
								}
								//payment.setNote("Pembayaran pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
								payment.setNote("Pelunasan pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
							}
							
							payment.setCreateBy("GIRO-BILLER");
							payment.setCreateOn(new Date());
							//payment.setLoanId(giroBiller.getLoanId());
							payment.setMethod(BankMethod.ATM);
							payment.setPaymentDate(giroBiller.getTransactionDate());
							//
							LocalDate datePart = new LocalDate(giroBiller.getTransactionDate());
							LocalTime timePart = new LocalTime(giroBiller.getTransactionTime());
							LocalDateTime dateTime = datePart.toLocalDateTime(timePart);
							payment.setPaymentDateTime(dateTime.toDate());
							//
							payment.setStatus(PaymentStatus.CONFIRMED);
							//payment.setTotalPaymentAmount(giroBiller.getTransactionAmount());
							Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
							String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("PAYMENT", l, payment.getPaymentDate(), true);
							payment.setTransactionCode(s);
							
							//ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
							/*if (applicationSetup.getNumberOfRandomNumber() > 0) {
								BigDecimal a = payment.getAmount().remainder(new BigDecimal(1000));
								payment.setAmount(payment.getAmount().subtract(a));
								payment.setOtherIncome(a);
							}*/
							
							
							/*AccountBalanceHistory*/
//							Long accountBalanceHistoryId = new Long(0);
							/*-----------------*/
							
							
							
							if (loan!=null) {
								if (loan.getType()==InterestType.ADVANCE) {
									List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
									for (LoanFunding funding :fundings) {
										PaymentDetail paymentDetail = new PaymentDetail();
										paymentDetail.setPayment(payment);
										// pokok saja
										paymentDetail.setAmount((payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount(),2,BigDecimal.ROUND_HALF_UP));
										//paymentDetail.setDeductionAmount(((payment.getAmount().multiply((funding.getAmount().divide(loan.getAmount())))).multiply(loan.getCommission())).divide(new BigDecimal(100)));
										paymentDetail.setDeductionAmount(new BigDecimal(0));
										paymentDetail.setLenderId(funding.getLenderId());
										paymentDetail.setLoanFundingId(funding.getLoanFundingId());
										//paymentDetail.setNote("Pembayaran pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
										paymentDetail.setNote("Pelunasan pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
										//
										payment.getPaymentDetils().add(paymentDetail);
										
										/*AccountBalanceHistory*/
										//Masukkan ke balancehistory
										/*Account lenderAccount = accountDAOFactory.getAccountDAO().findById(funding.getLenderId());
										if(lenderAccount.getIsRdp2p() != null && lenderAccount.getIsRdp2p()) { paymentDetail.setIsRdp2p(true); }
										
										if(lenderAccount.getIsMigrated() != null && lenderAccount.getIsMigrated()) {
											AccountBalanceHistory accountBalanceHistory = new AccountBalanceHistory();
											accountBalanceHistory.setAccountId(funding.getLenderId());
											accountBalanceHistory.setAmount(paymentDetail.getAmount());
											accountBalanceHistory.setType("Kredit");
											accountBalanceHistory.setNote("Pelunasan pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
											accountBalanceHistory.setBalanceDate(payment.getPaymentDateTime());
											accountBalanceHistory.setTransactionType(AccountBalanceHistoryTransactionType.PAYMENT);
											
											BigDecimal availableCashBefore = loanDAOFactory.getAccountBalanceHistoryDAO().calculateAvailableCash(funding.getLenderId());
											accountBalanceHistory.setAvailableCash(availableCashBefore.add(paymentDetail.getAmount()));
											lenderAccount.setAvailableCash(accountBalanceHistory.getAvailableCash());
											
											loanDAOFactory.getAccountBalanceHistoryDAO().save(accountBalanceHistory);
											accountDAOFactory.getAccountDAO().update(lenderAccount);
											accountBalanceHistoryId = accountBalanceHistory.getAccountBalanceHistoryId();
										}*/
									}
								} else {
									List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
									for (LoanFunding funding :fundings) {
										PaymentDetail paymentDetail = new PaymentDetail();
										paymentDetail.setPayment(payment);
										// pokok
										BigDecimal p = (payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount(),2,BigDecimal.ROUND_HALF_UP);
										// interest
										BigDecimal i = (loan.getInterest().multiply(p)).divide(new BigDecimal(100));
										// commission
										BigDecimal c = (i.multiply(loan.getCommission())).divide(new BigDecimal(100));
										paymentDetail.setAmount((p.add(i)).subtract(c));
										paymentDetail.setDeductionAmount(c);
										paymentDetail.setLenderId(funding.getLenderId());
										paymentDetail.setLoanFundingId(funding.getLoanFundingId());
										//paymentDetail.setNote("Pembayaran pinjaman dan admin fee ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
										paymentDetail.setNote("Pelunasan pinjaman dan admin fee ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
										//
										payment.getPaymentDetils().add(paymentDetail);
										
										
										/*AccountBalanceHistory*/
										//Masukkan ke balancehistory
										/*Account lenderAccount = accountDAOFactory.getAccountDAO().findById(funding.getLenderId());
										if(lenderAccount.getIsRdp2p() != null && lenderAccount.getIsRdp2p()) { paymentDetail.setIsRdp2p(true); }
										
										if(lenderAccount.getIsMigrated() != null && lenderAccount.getIsMigrated()) {
											AccountBalanceHistory accountBalanceHistory = new AccountBalanceHistory();
											accountBalanceHistory.setAccountId(funding.getLenderId());
											accountBalanceHistory.setAmount(paymentDetail.getAmount());
											accountBalanceHistory.setType("Kredit");
											accountBalanceHistory.setNote("Pelunasan pinjaman dan admin fee ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
											accountBalanceHistory.setBalanceDate(payment.getPaymentDateTime());
											accountBalanceHistory.setTransactionType(AccountBalanceHistoryTransactionType.PAYMENT);
											
											BigDecimal availableCashBefore = loanDAOFactory.getAccountBalanceHistoryDAO().calculateAvailableCash(funding.getLenderId());
											accountBalanceHistory.setAvailableCash(availableCashBefore.add(paymentDetail.getAmount()));
											lenderAccount.setAvailableCash(accountBalanceHistory.getAvailableCash());
											
											loanDAOFactory.getAccountBalanceHistoryDAO().save(accountBalanceHistory);
											accountDAOFactory.getAccountDAO().update(lenderAccount);
											accountBalanceHistoryId = accountBalanceHistory.getAccountBalanceHistoryId();
										}*/
									}
								}
							}
							//
							payment.setUploadFileId(null);
							loanDAOFactory.getPaymentDAO().save(payment);
							
							/*AccountBalanceHistory*/
							/*if (payment.getPenaltyPayment()!=null && payment.getPenaltyPayment().compareTo(BigDecimal.ZERO)>0) {
								LoanFunding funding = loanDAOFactory.getLoanFundingDAO().findByCriteria(Restrictions.eq("loanId", loan.getLoanId()));
								Account lenderAccount = accountDAOFactory.getAccountDAO().findById(funding.getLenderId());
								
								if(lenderAccount.getIsMigrated() != null && lenderAccount.getIsMigrated()) {
									AccountBalanceHistory accountBalanceHistory = new AccountBalanceHistory();
									accountBalanceHistory.setAccountId(funding.getLenderId());
									accountBalanceHistory.setAmount(payment.getPenaltyPayment());
									accountBalanceHistory.setType("Kredit");
									accountBalanceHistory.setNote("Penerimaan penalty ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
									accountBalanceHistory.setBalanceDate(giroBiller.getTransactionDate());
									accountBalanceHistory.setTransactionType(AccountBalanceHistoryTransactionType.PENALTY_PAYMENT);
									accountBalanceHistory.setKeyId(payment.getPaymentId());
									
									BigDecimal availableCashBefore = loanDAOFactory.getAccountBalanceHistoryDAO().calculateAvailableCash(funding.getLenderId());
									accountBalanceHistory.setAvailableCash(availableCashBefore.add(payment.getPenaltyPayment()));
									lenderAccount.setAvailableCash(accountBalanceHistory.getAvailableCash());
									
									loanDAOFactory.getAccountBalanceHistoryDAO().save(accountBalanceHistory);
									accountDAOFactory.getAccountDAO().update(lenderAccount);
								}
							}
							if(accountBalanceHistoryId != 0) {
								AccountBalanceHistory accountBalanceHistory = loanDAOFactory.getAccountBalanceHistoryDAO().findById(accountBalanceHistoryId);
								accountBalanceHistory.setKeyId(accountBalanceHistoryId);
								loanDAOFactory.getAccountBalanceHistoryDAO().update(accountBalanceHistory);
							}*/
							// last payment-ID
							lastPaymentId = payment.getPaymentId();
							
							if (loan!=null) {
								// update loan
								BigDecimal[] a = loanDAOFactory.getPaymentDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
								if (a[1].compareTo(BigDecimal.ZERO)>0) {
									loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
								} else {
									if (loan.getAmount().compareTo(a[0])<=0) {
										loan.setPaymentStatus(LoanPaymentStatus.CLOSED);
										loanDAOFactory.getPaymentDAO().updatePaidOffDate(loan.getLoanId(), payment.getPaymentId(), payment.getPaymentDate());
										
										/*
										 * check difference funding & payment per-funding!
										 * 10-10-2016
										 */
										for (PaymentDetail paymentDetail : payment.getPaymentDetils()) {
											// check payment-detail-amount
											BigDecimal loanPayment = loanDAOFactory.getPaymentDAO().getPaymentAmountByLoanFundingId(paymentDetail.getLoanFundingId());
											if (loanPayment==null) loanPayment = new BigDecimal(0);
											LoanFunding loanFunding = loanDAOFactory.getLoanFundingDAO().findByCriteria(Restrictions.eq("loanFundingId", paymentDetail.getLoanFundingId()));
											BigDecimal diff = loanFunding.getAmount().subtract(loanPayment);
											if (diff!=null && diff.compareTo(BigDecimal.ZERO)!=0) {
												loanDAOFactory.getPaymentDAO().addPaymentAmountDifference(paymentDetail.getPaymentDetailId(), diff);
											}
											
										}
										
									} else loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
								}
								
								//loanDAOFactory.getPaymentDAO().update(payment);
								loanDAOFactory.getLoanDAO().update(loan);
								
								try {
									log.info("<<<< Start Sending notification >>>>");
									Long prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(loan.getAccountId());
									PrepaidProvider prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findById(prepaidProviderId);
									log.info("prepaidProviderId : "+prepaidProviderId+" // "+prepaidProvider.getVirtualAccountPaymentNotifUrl());
									if (prepaidProvider!=null && prepaidProvider.getVirtualAccountPaymentNotifUrl()!=null && prepaidProvider.getVirtualAccountPaymentNotifUrl().length()>0) {
										String[] status = JSONService.postVirtualAccountPaymentNotification(prepaidProvider.getVirtualAccountPaymentNotifUrl(), 180000, 
												loan.getTransactionCode(), payment.getTotalPaymentAmount().toString(), CommonUtil.getStringFromDate(payment.getPaymentDateTime(), "yyyyMMddHHmmss"), 
												giroBiller.getStan(), "8238"+loan.getVirtualAccount(), loan.getPaymentStatus().name(), ((loan.getAmount().subtract(a[0])).add(a[1])).toString()); 
										//
										if (status!=null) {
											payment.setNotifySendDate(new Date());
											payment.setNotifyStatus(status[1]);
											loanDAOFactory.getPaymentDAO().update(payment);
										}
										log.info("<<<<<< Status "+loan.getTransactionCode()+" - "+CommonUtil.getStringFromDate(payment.getPaymentDateTime(), "yyyyMMddHHmmss")+" : "+status[0]+" "+status[1]+" >>>>>>>");
									}else{
										log.info("<<<<< Provider Null or Url Payment notif null! >>>>>>>");
									}
									
									/*
									 * Sent email notification when payment success
									 */
									try {
										long accountId = loanDAOFactory.getLoanDAO().getAccountIdByLoanId(payment.getLoanId());
										Account account2 = accountDAOFactory.getAccountDAO().findById(accountId);
										String email = accountDAOFactory.getAccountDAO().getEmailByAccountId(accountId);
										
										// email username-password
										Properties p = new Properties();
										
										ClassLoader loader = GiroBillerTask.class.getClassLoader();
									    URL r = loader.getResource("/");
									    String pathCls = r.getPath();
									    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
										
										//ServletContext servletContext = ServletActionContext.getServletContext();
										//System.out.println("ServletContext");
										//String path = servletContext.getRealPath("/");
										//logger.info("path >> "+path);
										p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
										Velocity.init(p);
										VelocityContext context = new VelocityContext();
											
										context.put("name", account2.getName());
										context.put("accountNo", account2.getAccountCode());
										context.put("amount",payment.getAmount());
										
										SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
										context.put("purchaseDate", df.format(loan.getLoanDate()));
										context.put("paymentDate", df.format(payment.getPaymentDate()));
										
										//Date payment = loanDAOFactory.getPaymentDAO().getLastPaymentDateLoanId(loan.getLoanId());
										//Organization organization = basicDAOFactory.getOrganizationDAO().findByCriteria(Restrictions.eq("organizationId", new Long (1)));
										//String phNumber = basicDAOFactory.getOrganizationDAO().getPhoneNumberOrganization(organization.getOrganizationId());
										String phNumber = basicDAOFactory.getContactDAO().findById(new Long (1)).getTelephone1();
										String name = accountDAOFactory.getAccountDAO().getNameByAccountId(giroBiller.getAccountId());
										String emailContact = basicDAOFactory.getContactDAO().findById(new Long (1)).getEmail1();
										if (account.getType()==AccountType.PERSONAL) {
											Personal personal = (Personal)account;
											if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
											else context.put("salutation", "Ibu/Sdri.");
										} else {
											context.put("salutation", "");
										}
										context.put("name", name);
										context.put("phNumber", phNumber);
										context.put("emailContact", emailContact);
										
										StringWriter w = new StringWriter();
										Velocity.mergeTemplate("/template/paymentSuccessVA.vm", context, w );
									
										// save to email
										OutgoingEmail outgoingEmail = new OutgoingEmail();
										outgoingEmail.setEmailDate(new Date());
										outgoingEmail.setSender(applicationSetup.getFromEmailAddress());
										outgoingEmail.setSubject("[Tokomodal] Pembayaran Anda Telah Berhasil");
										outgoingEmail.setTo(email);
										outgoingEmail.setMessage(w.toString());
										outgoingEmail.setType("[Tokomodal] Pembayaran Anda Telah Berhasil");
										
										messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail);
										log.info(">>>>>>>>>> Email notification sent <<<<<<<<<<");
									}
									catch(Exception e) {
										e.printStackTrace();
										log.error(">>>>>>>>>> Something wrong when system is trying to send an email notification <<<<<<<<<<");
									}
									
									
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								
								// cek data PG
								try {
									String ss = JSONService.updateTransaction(applicationSetup.getPgGtwUrl(), 90000, loan.getLoanId(), payment.getPaymentDate());
									log.info("status del PG : "+ss+" loan-ID : "+loan.getLoanId()+" ");
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								
							}
							lastLoanId = loan.getLoanId();
							
							loanNumbers = loanNumbers+((loanNumbers.length()>0?", ":"")+loan.getTransactionCode());
							
							//Jika Amount giro biller sudah 0 , tetapi masi ada pinjaman gantung .break
							if (paymentAmount.compareTo(BigDecimal.ZERO)==0) break;
						}
						// if payment-amount > 0
						if (paymentAmount.compareTo(BigDecimal.ZERO)>0) {
							kewajibanLain = new KewajibanLain();
							kewajibanLain.setGiroBillerId(giroBiller.getGiroBillerId());
							kewajibanLain.setAccountId(giroBiller.getAccountId());
							kewajibanLain.setAmount(paymentAmount);
							kewajibanLain.setCreateBy("GIRO-BILLER");
							kewajibanLain.setCreateOn(new Date());
							kewajibanLain.setFromVirtualAccount(giroBiller.getFromAccountNumber());
							kewajibanLain.setLoanId(lastLoanId);
							LocalDate datePart = new LocalDate(giroBiller.getTransactionDate());
							LocalTime timePart = new LocalTime(giroBiller.getTransactionTime());
							LocalDateTime dateTime = datePart.toLocalDateTime(timePart);
							kewajibanLain.setPaymentDateTime(dateTime.toDate());
							//kewajibanLain.setFromPaymentId(fromPaymentId);
							kewajibanLain.setFromPaymentNote("Lebih bayar dari V/A 8238"+giroBiller.getFromAccountNumber());
							// save kewajiban lain!
							if (kewajibanLain!=null) {
								//kewajibanLain.setPaymentDateTime(paymentDateTime);
								kewajibanLain.setFromPaymentId(lastPaymentId);
								loanDAOFactory.getKewajibanLainDAO().save(kewajibanLain);
							}
						}
					} else {
						KewajibanLain kewajibanLain = null;
						Long lastPaymentId = null;
						Long lastLoanId = new Long(0);
						for (Loan loan : loans) {
							// with amortization
							// check uncomplete payment
							while (paymentAmount.compareTo(BigDecimal.ZERO)>0) {
								if (paymentAmount.compareTo(BigDecimal.ZERO)==0) break;
								log.info("Payment Amount Amortization : " + paymentAmount);
								String sql = " ((principal + interest + penalty) > payment_amount) ";
								LoanAmortization loanAmortization = amortizationDAOFactory.getLoanAmortizationDAO().findByOrderAndCriteria(Order.asc("counter"), Restrictions.eq("loanId", loan.getLoanId()), Restrictions.or(Restrictions.isNull("paymentDate"), (Restrictions.and(Restrictions.isNotNull("paymentDate"), Restrictions.sqlRestriction(sql)))));
								//LoanAmortization lastLoanAmortization = amortizationDAOFactory.getLoanAmortizationDAO().findByOrderAndCriteria(Order.desc("counter"), Restrictions.eq("loanId", loan.getLoanId()), Restrictions.isNotNull("paymentDate"));
								if (loanAmortization!=null) {
									
									Payment payment = new Payment();
									payment.setGiroBillerId(null);
									BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
									//BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findById(payment.getToBankAccountId());
									if (bankAccount!=null) {
										payment.setBankId(bankAccount.getBank().getLookupId());
										payment.setAccountName(bankAccount.getAccountName());
										payment.setAccountNumber(bankAccount.getAccountNumber());
										payment.setToBankAccountId(bankAccount.getBankAccountId());
									}
	
									//BigDecimal[] b = loanDAOFactory.getPaymentDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
									//BigDecimal previousPayment = b[0];
									BigDecimal previousPayment = null;
									//BigDecimal outstandingPenalty = amortizationDAOFactory.getLoanAmortizationDAO().totalOutstandingPenaltyAmount(loan.getLoanId());
									BigDecimal outstandingPenalty = loanAmortization.getPenalty().subtract(loanAmortization.getPaymentPenalty());
									BigDecimal outstandingAmount = new BigDecimal(0);
									BigDecimal penalty = new BigDecimal(0);
									BigDecimal principal = null;
									BigDecimal interest = null;
									//
									if (interest==null) interest = loanAmortization.getInterestPayment()!=null?loanAmortization.getInterestPayment():BigDecimal.ZERO;
									if (principal==null) principal = loanAmortization.getPrincipalPayment()!=null?loanAmortization.getPrincipalPayment():BigDecimal.ZERO;
									if (previousPayment==null) previousPayment = loanAmortization.getPaymentAmount()!=null?loanAmortization.getPaymentAmount().subtract(loanAmortization.getPaymentPenalty()):BigDecimal.ZERO;
									//Date lastPayDate = loanDAOFactory.getPaymentDAO().getLastPaymentDateLoanId(loan.getLoanId());
									Date lastPayDate = loanAmortization.getPaymentDate();
									//overDueDate = loan.getOverDueDate().getTime();
									//lastPaymentDate = lastPayDate!=null?lastPayDate.getTime():0;
									//BigDecimal loanAmount = loan.getAmount();
									BigDecimal loanAmount = loanAmortization.getPrincipal().add(loanAmortization.getInterest());
									//logger.info("loanAmount > "+loanAmount);
									// interest
									/*if (loan.getType()==InterestType.ARREAR) {
										loanAmount = loanAmount.add((loanAmount.multiply(loan.getInterest())).divide(new BigDecimal(100)));
									}*/
									//DO DO DO DO
									
									/*	Diganti dengan girobiller transaction date
									 * LocalDate d1 = new LocalDate(new Date().getTime());
									 * */
									LocalDate d1 = new LocalDate(giroBiller.getTransactionDate().getTime());
									//LocalDate d2 = new LocalDate(loan.getOverDueDate().getTime());
									LocalDate d2 = new LocalDate(loanAmortization.getDueDate() .getTime());
									d2 = d2.plusDays(loan.getGracePeriode());
									BigDecimal unconfirmPayment = loanDAOFactory.getPaymentDAO().getUnconfirmPaymentByLoanId(loan.getLoanId());
									if (unconfirmPayment==null) unconfirmPayment = new BigDecimal(0);
									if (d1.isAfter(d2)) {
										// terlambat
										int d = 0;
										if (lastPayDate==null) {
											//belum pernah bayar
											d2 = d2.minusDays(loan.getGracePeriode());
											d = Days.daysBetween(d2, d1).getDays();
										} else {
											//sudah pernah bayar
											LocalDate d3 = new LocalDate(lastPayDate.getTime());
											if (d3.isAfter(d2)){
												//last payment > over due
												d = Days.daysBetween(d3, d1).getDays();
											}else {
												//last payment < over due
												d2 = d2.minusDays(loan.getGracePeriode());
												d = Days.daysBetween(d2, d1).getDays();
											}
										}
										outstandingAmount = loanAmount.subtract(previousPayment).subtract(unconfirmPayment);
										penalty = (((outstandingAmount.multiply(loan.getPenalty())).divide(new BigDecimal(100))).multiply(new BigDecimal(d))).setScale(0, BigDecimal.ROUND_HALF_UP);
										//total = (outstandingAmount.add(penalty)).add(outstandingPenalty);
										
										//System.out.println("penalty >> "+d+" // "+penalty);
										
									} else {
										penalty = new BigDecimal(0);
										outstandingAmount = loanAmount.subtract(previousPayment).subtract(unconfirmPayment);
										//total = outstandingAmount.add(penalty);
									}
									//total = (outstandingAmount.add(penalty)).add(outstandingPenalty);
									if (loan!=null) payment.setLoanId(loan.getLoanId());
									payment.setPenalty(penalty);
									//
									loanAmortization.setPenalty(payment.getPenalty().add(loanAmortization.getPenalty()));
									//System.out.println("paymentAmount >> "+paymentAmount+"//"+outstandingAmount+"//"+outstandingPenalty);
									// payment equal/greater than OS !!!
									if (paymentAmount.compareTo((outstandingAmount.add(penalty).add(outstandingPenalty)))>=0) {
										//log.info("paymentAmount > outstandingAmount+penalty+outstandingPenalty   "+paymentAmount);
										payment.setTotalPaymentAmount((outstandingAmount.add(penalty)).add(outstandingPenalty));
										payment.setAmount(outstandingAmount);
										payment.setPenaltyPayment(penalty.add(outstandingPenalty));
										// kurangi
										paymentAmount = paymentAmount.subtract((outstandingAmount.add(penalty).add(outstandingPenalty)));
										/*if (outstandingPenalty.compareTo(BigDecimal.ZERO)>0){
											payment.setPenaltyPayment(outstandingPenalty);
											payment.setTotalPaymentAmount(payment.getTotalPaymentAmount().add(outstandingPenalty));
										}*/
										//System.out.println("A");
									} else {
										//log.info("paymentAmount < outstandingAmount+penalty+outstandingPenalty   "+paymentAmount);
										payment.setTotalPaymentAmount(paymentAmount);
										if ((paymentAmount.subtract(outstandingAmount)).compareTo(BigDecimal.ZERO)>0) {
											payment.setAmount(outstandingAmount);
											payment.setPenaltyPayment(paymentAmount.subtract(outstandingAmount));
										} else {
											payment.setAmount(paymentAmount);
											payment.setPenaltyPayment(new BigDecimal(0));
										}
										paymentAmount = new BigDecimal(0);
										//System.out.println("B");
									}
									log.info("LOAN AMORTIZATION : Penalty : " + penalty + " paymentAmount : " + paymentAmount + " totalPaymentAmount : " + payment.getTotalPaymentAmount());
									// pembagian payment pokok dan bunga
									if (payment.getAmount().compareTo(loanAmortization.getPrincipal().subtract(principal))<=0) {
										payment.setPrincipalPayment(payment.getAmount());
										payment.setInterestPayment(payment.getAmount().subtract(payment.getPrincipalPayment()));
									} else {
										if (payment.getAmount().compareTo(loanAmortization.getPrincipal().subtract(principal))>0) {
											payment.setPrincipalPayment(loanAmortization.getPrincipal().subtract(principal));
											payment.setInterestPayment(payment.getAmount().subtract(payment.getPrincipalPayment()));
										} else {
											payment.setPrincipalPayment(payment.getAmount());
											payment.setInterestPayment(new BigDecimal(0));
										}
									}
									
									/*if (payment.getAmount().compareTo(loanAmount)>=0) {
										payment.setPrincipalPayment(loanAmortization.getPrincipal());
										payment.setInterestPayment(loanAmortization.getInterest());
									} else {
										if (loanAmortization.getPrincipal().compareTo(principal)==0) {
											payment.setPrincipalPayment(new BigDecimal(0));
											payment.setInterestPayment(payment.getAmount());
										} else {
											if (payment.getAmount().compareTo(loanAmortization.getPrincipal())>0) {
												payment.setPrincipalPayment(loanAmortization.getPrincipal());
												payment.setInterestPayment(payment.getAmount().subtract(loanAmortization.getPrincipal()));
											} else {
												if (payment.getAmount().compareTo(loanAmortization.getPrincipal().subtract(principal))>0) {
													payment.setPrincipalPayment(loanAmortization.getPrincipal().subtract(principal));
													payment.setInterestPayment(payment.getAmount().subtract(payment.getPrincipalPayment()));
												} else {
													payment.setPrincipalPayment(payment.getAmount());
													payment.setInterestPayment(new BigDecimal(0));
												}
											}
										}
									}*/
									
									loanAmortization.setPaymentDate(giroBiller.getTransactionDate());
									if (payment.getAmount().compareTo(loanAmount) < 0) {
										loanAmortization.setPaymentAmount(payment.getAmount().add(previousPayment.add(loanAmortization.getPaymentPenalty())).add(payment.getPenaltyPayment()));
										loanAmortization.setPrincipalPayment(payment.getPrincipalPayment().add(principal));
										loanAmortization.setInterestPayment(payment.getInterestPayment().add(interest));
									} else {
										loanAmortization.setPaymentAmount(payment.getTotalPaymentAmount().add(previousPayment.add(loanAmortization.getPaymentPenalty())));
										loanAmortization.setPrincipalPayment(payment.getPrincipalPayment().add(principal));
										loanAmortization.setInterestPayment(payment.getInterestPayment().add(interest));
									}
									
									loanAmortization.setPaymentPenalty(payment.getPenaltyPayment().add(loanAmortization.getPaymentPenalty()));
									
									//
									if (payment.getPenaltyPayment()!=null && payment.getPenaltyPayment().compareTo(BigDecimal.ZERO)>0) {
										BigDecimal a = payment.getPenaltyPayment();
										a = loanDAOFactory.getLoanDAO().penaltyPayment(payment.getLoanId(), a);
										if (a.compareTo(BigDecimal.ZERO)>=0) {
											payment.setPenaltyPayment(a);
										}
									}
									payment.setNote("Cicilan Pokok ke "+loanAmortization.getCounter()+" ("+loan.getTransactionCode()+" a/n "+loan.getAccountName()+") ");
								
									payment.setCreateBy("GIRO-BILLER");
									payment.setCreateOn(new Date());
									//payment.setLoanId(giroBiller.getLoanId());
									payment.setMethod(BankMethod.ATM);
									payment.setPaymentDate(giroBiller.getTransactionDate());
									//
									//LocalDate datePart = new LocalDate(giroBiller.getTransactionDate());
									//LocalTime timePart = new LocalTime(giroBiller.getTransactionTime());
									//LocalDateTime dateTime = datePart.toLocalDateTime(timePart);
									payment.setPaymentDateTime(new Date());
									//
									payment.setStatus(PaymentStatus.CONFIRMED);
									//payment.setTotalPaymentAmount(giroBiller.getTransactionAmount());
									Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
									String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("PAYMENT", l, payment.getPaymentDate(), true);
									payment.setTransactionCode(s);
									
									//ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
									/*if (applicationSetup.getNumberOfRandomNumber() > 0) {
										BigDecimal a = payment.getAmount().remainder(new BigDecimal(1000));
										payment.setAmount(payment.getAmount().subtract(a));
										payment.setOtherIncome(a);
									}*/
									
									if (loan.getType()==InterestType.ADVANCE) {
										List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
										for (LoanFunding funding :fundings) {
											PaymentDetail paymentDetail = new PaymentDetail();
											paymentDetail.setPayment(payment);
											// pokok saja
											paymentDetail.setAmount((payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount(),2,BigDecimal.ROUND_HALF_UP));
											//paymentDetail.setDeductionAmount(((payment.getAmount().multiply((funding.getAmount().divide(loan.getAmount())))).multiply(loan.getCommission())).divide(new BigDecimal(100)));
											paymentDetail.setDeductionAmount(new BigDecimal(0));
											paymentDetail.setLenderId(funding.getLenderId());
											paymentDetail.setLoanFundingId(funding.getLoanFundingId());
											paymentDetail.setNote("Cicilan Pokok ke "+loanAmortization.getCounter()+" ("+loan.getTransactionCode()+" a/n "+loan.getAccountName()+")");
											//
											payment.getPaymentDetils().add(paymentDetail);
											
										}
									} else {
										List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
										for (LoanFunding funding :fundings) {
											PaymentDetail paymentDetail = new PaymentDetail();
											paymentDetail.setPayment(payment);
											// pokok
											BigDecimal p = (payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount(),2,BigDecimal.ROUND_HALF_UP);
											// interest
											BigDecimal i = (loan.getInterest().multiply(p)).divide(new BigDecimal(100));
											// commission
											BigDecimal c = (i.multiply(loan.getCommission())).divide(new BigDecimal(100));
											paymentDetail.setAmount((p.add(i)).subtract(c));
											paymentDetail.setDeductionAmount(c);
											paymentDetail.setLenderId(funding.getLenderId());
											paymentDetail.setLoanFundingId(funding.getLoanFundingId());
											paymentDetail.setNote("Cicilan Pokok ke "+loanAmortization.getCounter()+" ("+loan.getTransactionCode()+" a/n "+loan.getAccountName()+")");
											//
											payment.getPaymentDetils().add(paymentDetail);
											
										}
									}
									//
									payment.setUploadFileId(null);
									payment.setLoanAmortizationId(loanAmortization.getLoanAmortizationId());
									loanDAOFactory.getPaymentDAO().save(payment);
									amortizationDAOFactory.getLoanAmortizationDAO().update(loanAmortization);
									
									
									try {
										log.info("<<<< Start Sending notification >>>>");
										
										Long prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(loan.getAccountId());
										PrepaidProvider prepaidProvider = accountDAOFactory.getPrepaidProviderDAO().findById(prepaidProviderId);
										log.info("prepaidProviderId : "+prepaidProviderId+" // "+prepaidProvider.getVirtualAccountPaymentNotifUrl());
										if (prepaidProvider!=null && prepaidProvider.getVirtualAccountPaymentNotifUrl()!=null && prepaidProvider.getVirtualAccountPaymentNotifUrl().length()>0) {
											BigDecimal outStandingAmount = loanAmortization.getInterest().add(loanAmortization.getPrincipal());
											if (loanAmortization.getPenalty()!=null) outStandingAmount = outStandingAmount.add(loanAmortization.getPenalty());
											if (loanAmortization.getPrincipalPayment()!=null) outStandingAmount = outStandingAmount.subtract(loanAmortization.getPrincipalPayment());
											if (loanAmortization.getInterestPayment()!=null) outStandingAmount = outStandingAmount.subtract(loanAmortization.getInterestPayment());
											if (loanAmortization.getPaymentPenalty()!=null) outStandingAmount = outStandingAmount.subtract(loanAmortization.getPaymentPenalty());
																
											String[] status = JSONService.postVirtualAccountPaymentNotification(prepaidProvider.getVirtualAccountPaymentNotifUrl(), 180000, 
													loan.getTransactionCode(), payment.getTotalPaymentAmount().toString(), CommonUtil.getStringFromDate(payment.getPaymentDateTime(), "yyyyMMddHHmmss"), 
													giroBiller.getStan(), "8238"+loan.getVirtualAccount(), loan.getPaymentStatus().name(), (outStandingAmount).toString()); 
											//
											if (status!=null) {
												payment.setNotifySendDate(new Date());
												payment.setNotifyStatus(status[1]);
												loanDAOFactory.getPaymentDAO().update(payment);
											}
											log.info("<<<<<< Status "+loan.getTransactionCode()+" - "+CommonUtil.getStringFromDate(payment.getPaymentDateTime(), "yyyyMMddHHmmss")+" : "+status[0]+" "+status[1]+" >>>>>>>");
										}else{
											log.info("<<<<< Provider Null or Url Payment notif null! >>>>>>>");
										}
										
										
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									
									// cek data PG
									try {
										String ss = JSONService.updateTransaction(applicationSetup.getPgGtwUrl(), 90000, loan.getLoanId(), payment.getPaymentDate());
										log.info("status del PG : "+ss+" loan-ID : "+loan.getLoanId()+" ");
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									// last payment-ID
									lastPaymentId = payment.getPaymentId();
								} else {
									break;
								}
							}

							// check uncomplete penalty-payment
							// TODO
							
							
							
							// update loan
							/*BigDecimal[] a = loanDAOFactory.getPaymentDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
							if (a[1].compareTo(BigDecimal.ZERO)>0) {
								loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
							} else {
								if (loan.getAmount().compareTo(a[0])<=0) {
									loan.setPaymentStatus(LoanPaymentStatus.CLOSED);
									loanDAOFactory.getPaymentDAO().updatePaidOffDate(loan.getLoanId(), payment.getPaymentId(), payment.getPaymentDate());
									
									
									 * check difference funding & payment per-funding!
									 * 10-10-2016
									 
									for (PaymentDetail paymentDetail : payment.getPaymentDetils()) {
										// check payment-detail-amount
										BigDecimal loanPayment = loanDAOFactory.getPaymentDAO().getPaymentAmountByLoanFundingId(paymentDetail.getLoanFundingId());
										if (loanPayment==null) loanPayment = new BigDecimal(0);
										LoanFunding loanFunding = loanDAOFactory.getLoanFundingDAO().findByCriteria(Restrictions.eq("loanFundingId", paymentDetail.getLoanFundingId()));
										BigDecimal diff = loanFunding.getAmount().subtract(loanPayment);
										if (diff!=null && diff.compareTo(BigDecimal.ZERO)!=0) {
											loanDAOFactory.getPaymentDAO().addPaymentAmountDifference(paymentDetail.getPaymentDetailId(), diff);
										}
										
									}
									
								} else loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
							}*/
							BigDecimal[] a = amortizationDAOFactory.getLoanAmortizationDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
							if (a[1].compareTo(BigDecimal.ZERO)>0) {
								loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
							} else {
								if (a[2].compareTo(a[0])>=0) loan.setPaymentStatus(LoanPaymentStatus.CLOSED);
								else loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
							}
							
							//loanDAOFactory.getPaymentDAO().update(payment);
							loanDAOFactory.getLoanDAO().update(loan);
							lastLoanId = loan.getLoanId();
							loanNumbers = loanNumbers+((loanNumbers.length()>0?", ":"")+loan.getTransactionCode());
							
							//Jika Amount giro biller sudah 0 , tetapi masi ada pinjaman gantung .break
							if (paymentAmount.compareTo(BigDecimal.ZERO)==0) break;
							//Jika Amount giro biller sudah 0 , tetapi masi ada pinjaman gantung .break
							//if (paymentAmount.compareTo(BigDecimal.ZERO)==0) break;
						}
						// if payment-amount > 0
						if (paymentAmount.compareTo(BigDecimal.ZERO)>0) {
							kewajibanLain = new KewajibanLain();
							kewajibanLain.setGiroBillerId(giroBiller.getGiroBillerId());
							kewajibanLain.setAccountId(giroBiller.getAccountId());
							kewajibanLain.setAmount(paymentAmount);
							kewajibanLain.setCreateBy("GIRO-BILLER");
							kewajibanLain.setCreateOn(new Date());
							kewajibanLain.setFromVirtualAccount(null);
							kewajibanLain.setLoanId(lastLoanId);
							//LocalDate datePart = new LocalDate(giroBiller.getTransactionDate());
							//LocalTime timePart = new LocalTime(giroBiller.getTransactionTime());
							//LocalDateTime dateTime = datePart.toLocalDateTime(timePart);
							kewajibanLain.setPaymentDateTime(new Date());
							//kewajibanLain.setFromPaymentId(fromPaymentId);
							kewajibanLain.setFromPaymentNote("Lebih bayar dari V/A 8238"+giroBiller.getFromAccountNumber());
							// save kewajiban lain!
							if (kewajibanLain!=null) {
								//kewajibanLain.setPaymentDateTime(paymentDateTime);
								if (lastPaymentId!=null) kewajibanLain.setFromPaymentId(lastPaymentId);
								loanDAOFactory.getKewajibanLainDAO().save(kewajibanLain);
							}
						}
					}
				}
				// update giro-biller
				giroBiller.setPosted(true);
				arDAOFactory.getGiroBillerDAO().update(giroBiller);
				//
				
				if (loanProduct!=null && loanProduct.getPaymentEmailNotificationTo()!=null) {
					if (loanProduct.getPaymentEmailNotificationTo()==EmailNotificationTo.ACCOUNT) {
						
						Properties p = new Properties();
						//ServletContext servletContext = getServletContext();
						ClassLoader loader = GiroBillerTask.class.getClassLoader();
					    URL r = loader.getResource("/");
					    String pathCls = r.getPath();
					    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
						p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
						Velocity.init(p);
						VelocityContext context = new VelocityContext();
						
						if (account.getType()==AccountType.PERSONAL) {
							Personal personal = (Personal)account;
							if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
							else context.put("salutation", "Ibu/Sdri.");
						} else {
							context.put("salutation", "");
						}
						
						context.put("name", account.getName());
						context.put("date", Formater.getFormatedDate(giroBiller.getTransactionDate(), "dd MMM yyyy"));
						context.put("amount", Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue()));
						
						Long lookUpCode = (long) Integer.parseInt(giroBiller.getInstitutionCode());
						Lookup lookUp = basicDAOFactory.getLookupDAO().findByCriteria(Restrictions.eq("code",lookUpCode.toString()));
						if(lookUpCode == 1020){
							String bankName = "BANK SINARMAS";
							context.put("fromBank", bankName);
						}else if(lookUpCode == null){ 
							String bankName = "OTHER BANK";
							context.put("fromBank", bankName);
						}else{
							context.put("fromBank", lookUp.getName());							
						}
						
						BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
						context.put("bank", bankAccount.getBank().getName());
						context.put("accountNumber", bankAccount.getAccountNumber());
						LoanProduct product = loanDAOFactory.getLoanProductDAO().findById(giroBiller.getLoanProductId());
						if (loanNumbers!=null && loanNumbers.length()>0) context.put("loan", product.getName()+ " No. "+loanNumbers);
						else context.put("loan", product.getName()+ " - ");
						
						StringWriter w = new StringWriter();
					    Velocity.mergeTemplate("/template/payment.vm", context, w );
						
						// save to email
						OutgoingEmail outgoingEmail = new OutgoingEmail();
						outgoingEmail.setEmailDate(new Date());
						outgoingEmail.setSender(applicationSetup.getFromEmailAddress());
						outgoingEmail.setSubject("[Tokomodal] Konfirmasi Pembayaran Pinjaman");
						outgoingEmail.setTo(account.getContact().getEmail1());
						outgoingEmail.setMessage(w.toString());
						messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail);
						
					} else if (loanProduct.getPaymentEmailNotificationTo()==EmailNotificationTo.PROVIDER) {
						
						PrepaidProvider prepaidProvider = null;
						Long prepaidProviderId = accountDAOFactory.getAccountDAO().getPrepaidProviderIdByAccountId(account.getAccountId());
						session = null;
						try {
							session = accountDAOFactory.getPrepaidProviderDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
							prepaidProvider = (PrepaidProvider)session.createCriteria(PrepaidProvider.class)
									.setFetchMode("prepaidVouchers", FetchMode.JOIN)
									.setFetchMode("contact", FetchMode.JOIN)
									.add(Restrictions.eq("prepaidProviderId", new Long(prepaidProviderId)))
									//.setCacheable(true)
									.uniqueResult();
						} finally {
							if (session!=null) session.close();
						}
						
						Properties p = new Properties();
						//ServletContext servletContext = getServletContext();
						ClassLoader loader = GiroBillerTask.class.getClassLoader();
					    URL r = loader.getResource("/");
					    String pathCls = r.getPath();
					    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
						p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
						Velocity.init(p);
						VelocityContext context = new VelocityContext();
						
						if (account.getType()==AccountType.PERSONAL) {
							Personal personal = (Personal)account;
							if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
							else context.put("salutation", "Ibu/Sdri.");
						} else {
							context.put("salutation", "");
						}
						
						context.put("name", account.getName());
						context.put("date", Formater.getFormatedDate(giroBiller.getTransactionDate(), "dd MMM yyyy"));
						context.put("amount", Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue()));
						
						Long lookUpCode = (long) Integer.parseInt(giroBiller.getInstitutionCode());
						Lookup lookUp = basicDAOFactory.getLookupDAO().findByCriteria(Restrictions.eq("code",lookUpCode.toString()));
						if(lookUpCode == 1020){
							String bankName = "BANK SINARMAS";
							context.put("fromBank", bankName);
						}else if(lookUpCode == null){ 
							String bankName = "OTHER BANK";
							context.put("fromBank", bankName);
						}else{
							context.put("fromBank", lookUp.getName());							
						}
						
						BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
						context.put("bank", bankAccount.getBank().getName());
						context.put("accountNumber", bankAccount.getAccountNumber());
						LoanProduct product = loanDAOFactory.getLoanProductDAO().findById(giroBiller.getLoanProductId());
						if (loanNumbers!=null && loanNumbers.length()>0) context.put("loan", product.getName()+ " No. "+loanNumbers);
						else context.put("loan", product.getName()+ " - ");
						
						StringWriter w = new StringWriter();
					    Velocity.mergeTemplate("/template/payment.vm", context, w );
						
						// save to email
						OutgoingEmail outgoingEmail = new OutgoingEmail();
						outgoingEmail.setEmailDate(new Date());
						outgoingEmail.setSender(applicationSetup.getFromEmailAddress());
						outgoingEmail.setSubject("[Tokomodal] Konfirmasi Pembayaran Pinjaman");
						outgoingEmail.setTo(prepaidProvider.getContact().getEmail1());
						outgoingEmail.setMessage(w.toString());
						messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail);
					}
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public void insertPayment() {
		try {
			
			List<GiroBiller> giroBillers = arDAOFactory.getGiroBillerDAO().findByCriteria(Order.asc("giroBillerId"), 5, Restrictions.eq("posted", Boolean.FALSE),Restrictions.eq("reversal", Boolean.FALSE), Restrictions.isNotNull("loanId"));
			log.info(" [ start payment loan giro biller bla bla >>>> "+giroBillers.size()+" ] "); 
			for (GiroBiller giroBiller :giroBillers) {
				Payment payment = new Payment();
				KewajibanLain kewajibanLain = null;
				//Loan loan = loanDAOFactory.getLoanDAO().findByCriteria(Restrictions.eq("loanId", giroBiller.getLoanId()));
				Loan loan = loanDAOFactory.getLoanDAO().findByCriteriaAndOrder(Order.asc("loanId"), 
						Restrictions.eq("productId", giroBiller.getLoanId()), Restrictions.eq("accountId", giroBiller.getAccountId()),
						Restrictions.eq("status", LoanStatus.APPROVED), Restrictions.eq("fundingStatus", LoanFundingStatus.TRANSFERED),
						Restrictions.or(Restrictions.ne("paymentStatus", LoanPaymentStatus.CLOSED), Restrictions.isNull("paymentStatus"))
						);	
				
				if (loan==null) {
					kewajibanLain = new KewajibanLain();
					kewajibanLain.setAccountId(giroBiller.getAccountId());
					kewajibanLain.setAmount(giroBiller.getTransactionAmount());
					kewajibanLain.setCreateBy("GIRO-BILLER");
					kewajibanLain.setCreateOn(new Date());
					kewajibanLain.setFromVirtualAccount(giroBiller.getFromAccountNumber());
					kewajibanLain.setLoanId(giroBiller.getLoanId());
					Calendar c = Calendar.getInstance();
					c.setTime(giroBiller.getTransactionDate());
					c.setTime(giroBiller.getTransactionTime());
					kewajibanLain.setPaymentDateTime(c.getTime());
					//kewajibanLain.setFromPaymentId(fromPaymentId);
					kewajibanLain.setFromPaymentNote("Pembayaran VA dari peminjam dengan pinjaman lunas dari "+giroBiller.getFromAccountNumber()+" sebesar "+Formater.getFormatedOutput(0, giroBiller.getTransactionAmount().doubleValue())+" pada "+CommonUtil.getStringFromDate(giroBiller.getTransactionDate(), "dd MMM yyyy")+" "+Formater.getFormatedTime(giroBiller.getTransactionTime()));
					
				}
				
				BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
				//BankAccount bankAccount = accountDAOFactory.getBankAccountDAO().findById(payment.getToBankAccountId());
				if (bankAccount!=null) {
					payment.setBankId(bankAccount.getBank().getLookupId());
					payment.setAccountName(bankAccount.getAccountName());
					payment.setAccountNumber(bankAccount.getAccountNumber());
					payment.setToBankAccountId(bankAccount.getBankAccountId());
				}
				BigDecimal paymentAmount = giroBiller.getTransactionAmount();
				if (loan!=null) {
					BigDecimal[] b = loanDAOFactory.getPaymentDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
					BigDecimal previousPayment = b[0];
					//BigDecimal outstandingPenalty = b[1];
					BigDecimal outstandingAmount = new BigDecimal(0);
					BigDecimal penalty = new BigDecimal(0);
					
					if (previousPayment==null) previousPayment = new BigDecimal(0);
					Date lastPayDate = loanDAOFactory.getPaymentDAO().getLastPaymentDateLoanId(loan.getLoanId());
					//overDueDate = loan.getOverDueDate().getTime();
					//lastPaymentDate = lastPayDate!=null?lastPayDate.getTime():0;
					BigDecimal amount = loan.getAmount();
					// interest
					if (loan.getType()==InterestType.ARREAR) {
						amount = amount.add((amount.multiply(loan.getInterest())).divide(new BigDecimal(100)));
					}
					//DO DO DO DO
					
						Diganti dengan girobiller transaction date
					 * LocalDate d1 = new LocalDate(new Date().getTime());
					 * 
					LocalDate d1 = new LocalDate(giroBiller.getTransactionDate().getTime());
					LocalDate d2 = new LocalDate(loan.getOverDueDate().getTime());
					d2 = d2.plusDays(loan.getGracePeriode());
					BigDecimal unconfirmPayment = loanDAOFactory.getPaymentDAO().getUnconfirmPaymentByLoanId(loan.getLoanId());
					if (unconfirmPayment==null) unconfirmPayment = new BigDecimal(0);
					if (d1.isAfter(d2)) {
						// terlambat
						int d = 0;
						if (lastPayDate==null) {
							d2 = d2.minusDays(loan.getGracePeriode());
							d = Days.daysBetween(d2, d1).getDays();
						} else {
							LocalDate d3 = new LocalDate(lastPayDate.getTime());
							d = Days.daysBetween(d3, d1).getDays();
						} 
						outstandingAmount = amount.subtract(previousPayment).subtract(unconfirmPayment);
						penalty = ((outstandingAmount.multiply(loan.getPenalty())).divide(new BigDecimal(100))).multiply(new BigDecimal(d));
						//total = (outstandingAmount.add(penalty)).add(outstandingPenalty);
					} else {
						penalty = new BigDecimal(0);
						outstandingAmount = amount.subtract(previousPayment).subtract(unconfirmPayment);
						//total = outstandingAmount.add(penalty);
					}
					//total = (outstandingAmount.add(penalty)).add(outstandingPenalty);
					if (loan!=null) payment.setLoanId(loan.getLoanId());
					//
					payment.setTotalPaymentAmount(paymentAmount);
					payment.setPenalty(penalty);
					if (paymentAmount.compareTo(outstandingAmount)>0) {
						if (payment.getPenalty().compareTo((paymentAmount.subtract(outstandingAmount)))>=0) {
							payment.setPenaltyPayment(paymentAmount.subtract(outstandingAmount));
						} else {
							//
							payment.setPenaltyPayment(penalty);
							// lebih bayar!
							if (kewajibanLain==null) kewajibanLain = new KewajibanLain();
							kewajibanLain.setAccountId(giroBiller.getAccountId());
							kewajibanLain.setAmount(((paymentAmount.subtract(outstandingAmount)).subtract(penalty)));
							kewajibanLain.setCreateBy("GIRO-BILLER");
							kewajibanLain.setCreateOn(new Date());
							kewajibanLain.setFromVirtualAccount(giroBiller.getFromAccountNumber());
							kewajibanLain.setLoanId(giroBiller.getLoanId());
							Calendar c = Calendar.getInstance();
							c.setTime(giroBiller.getTransactionDate());
							c.setTime(giroBiller.getTransactionTime());
							kewajibanLain.setPaymentDateTime(c.getTime());
							//kewajibanLain.setFromPaymentId(fromPaymentId);
							kewajibanLain.setFromPaymentNote("Sisa pembayaran VA dari peminjam "+giroBiller.getFromAccountNumber()+" dengan pinjaman "+loan.getTransactionCode()+" sebesar "+Formater.getFormatedOutput(0, kewajibanLain.getAmount().doubleValue())+" pada "+CommonUtil.getStringFromDate(giroBiller.getTransactionDate(), "dd MMM yyyy")+" "+Formater.getFormatedTime(giroBiller.getTransactionTime()));
						}
						//payment.setPenaltyPayment(paymentAmount.subtract(outstandingAmount));
						payment.setAmount(outstandingAmount);
					} else {
						payment.setAmount(paymentAmount);
						payment.setPenaltyPayment(new BigDecimal(0));
					}
					
					//
					if (payment.getPenaltyPayment()!=null && payment.getPenaltyPayment().compareTo(BigDecimal.ZERO)>0) {
						BigDecimal a = payment.getPenaltyPayment();
						a = loanDAOFactory.getLoanDAO().penaltyPayment(payment.getLoanId(), a);
						if (a.compareTo(BigDecimal.ZERO)>=0) {
							payment.setPenaltyPayment(a);
						}
					}
					payment.setNote("Pembayaran pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
				} else {
					payment.setAmount(paymentAmount);
				}
				
				
				payment.setCreateBy("GIRO-BILLER");
				payment.setCreateOn(new Date());
				//payment.setLoanId(giroBiller.getLoanId());
				payment.setMethod(BankMethod.ATM);
				payment.setPaymentDate(giroBiller.getTransactionDate());
				payment.setStatus(PaymentStatus.CONFIRMED);
				payment.setTotalPaymentAmount(giroBiller.getTransactionAmount());
				Long l = basicDAOFactory.getOrganizationDAO().getOrganizationId();
				String s = basicDAOFactory.getRunningNumberDAO().getUniqueRunningNumber("PAYMENT", l, payment.getPaymentDate(), true);
				payment.setTransactionCode(s);
				
				ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
				if (applicationSetup.getNumberOfRandomNumber() > 0) {
					BigDecimal a = payment.getAmount().remainder(new BigDecimal(1000));
					payment.setAmount(payment.getAmount().subtract(a));
					payment.setOtherIncome(a);
					BigDecimal a = payment.getAmount().remainder(new BigDecimal(1000));
					payment.setAmount(payment.getAmount().subtract(a));
					payment.setOtherIncome(a);
				}
				
				if (loan!=null) {
					if (loan.getType()==InterestType.ADVANCE) {
						List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
						for (LoanFunding funding :fundings) {
							PaymentDetail paymentDetail = new PaymentDetail();
							paymentDetail.setPayment(payment);
							// pokok saja
							paymentDetail.setAmount((payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount()));
							//paymentDetail.setDeductionAmount(((payment.getAmount().multiply((funding.getAmount().divide(loan.getAmount())))).multiply(loan.getCommission())).divide(new BigDecimal(100)));
							paymentDetail.setDeductionAmount(new BigDecimal(0));
							paymentDetail.setLenderId(funding.getLenderId());
							paymentDetail.setLoanFundingId(funding.getLoanFundingId());
							paymentDetail.setNote("Pembayaran pinjaman ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
							//
							payment.getPaymentDetils().add(paymentDetail);
						}
					} else {
						List<LoanFunding> fundings = loanDAOFactory.getLoanFundingDAO().findByCriteria(Order.asc("loanFundingId"), Restrictions.eq("loanId", loan.getLoanId()));
						for (LoanFunding funding :fundings) {
							PaymentDetail paymentDetail = new PaymentDetail();
							paymentDetail.setPayment(payment);
							// pokok
							BigDecimal p = (payment.getAmount().multiply(funding.getAmount())).divide(loan.getAmount());
							// interest
							BigDecimal i = (loan.getInterest().multiply(p)).divide(new BigDecimal(100));
							// commission
							BigDecimal c = (i.multiply(loan.getCommission())).divide(new BigDecimal(100));
							paymentDetail.setAmount((p.add(i)).subtract(c));
							paymentDetail.setDeductionAmount(c);
							paymentDetail.setLenderId(funding.getLenderId());
							paymentDetail.setLoanFundingId(funding.getLoanFundingId());
							paymentDetail.setNote("Pembayaran pinjaman dan admin fee ("+loan.getTransactionCode()+" "+Formater.getFormatedDate(loan.getDrawdownDate(), "dd MMM yyyy")+")");
							//
							payment.getPaymentDetils().add(paymentDetail);
						}
					}
				} else {
					
					// 
					
					
				}
				
				payment.setUploadFileId(null);
				loanDAOFactory.getPaymentDAO().save(payment);
				
				// save kewajiban lain!
				if (kewajibanLain!=null) {
					//kewajibanLain.setPaymentDateTime(paymentDateTime);
					kewajibanLain.setFromPaymentId(payment.getPaymentId());
					loanDAOFactory.getKewajibanLainDAO().save(kewajibanLain);
				}
				
				if (loan!=null) {
					// update loan
					BigDecimal[] a = loanDAOFactory.getPaymentDAO().getPaymentAndPenaltyAmountByLoanId(loan.getLoanId());
					if (a[1].compareTo(BigDecimal.ZERO)>0) {
						loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
					} else {
						if (loan.getAmount().compareTo(a[0])==0) {
							loan.setPaymentStatus(LoanPaymentStatus.CLOSED);
							loanDAOFactory.getPaymentDAO().updatePaidOffDate(loan.getLoanId(), payment.getPaymentId(), payment.getPaymentDate());
						}
						else loan.setPaymentStatus(LoanPaymentStatus.PARTIAL);
					}
					
					//loanDAOFactory.getPaymentDAO().update(payment);
					loanDAOFactory.getLoanDAO().update(loan);
				}
				
				//
				giroBiller.setPosted(true);
				arDAOFactory.getGiroBillerDAO().update(giroBiller);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	

}
