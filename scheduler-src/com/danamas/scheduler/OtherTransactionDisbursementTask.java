package com.danamas.scheduler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.BankAccount;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.loan.model.LoanProductThirdParty;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.tokomodal.otherTransaction.model.OtDisbursementTransferInfo;
import com.tokomodal.otherTransaction.model.dao.OtherTransactionDAOFactory;
import com.tokomodal.otherTransaction.model.dao.OtherTransactionDAOFactoryHibernate;
import com.mpe.basic.model.Lookup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;

public class OtherTransactionDisbursementTask {
	
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	OtherTransactionDAOFactory otherTransactionDAOFactory = OtherTransactionDAOFactory.instance(OtherTransactionDAOFactoryHibernate.class);
	
	@SuppressWarnings("unchecked")
	public void doOtherTransaction() {
		try {			
			log.info(" <<<<<<<< START of OtherTransactionDisbursementTask "+this.getClass().getSimpleName()+" doOtherTransaction() >>>>>>>>");
		
			// sum amount and ids
			Object[] o = otherTransactionDAOFactory.getOtherTransactionDAO().getTotalAmountAndTotalId();
			
			if (o[0]!=null && o[1]!=null) {
				if (((BigDecimal)o[0]).compareTo(BigDecimal.ZERO)>0) {
	
					// bank danamas!
					BankAccount danamasBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
					
					LoanProductThirdParty loanProductThirdParty = loanDAOFactory.getLoanProductThirdPartyDAO().findByCriteria(Restrictions.ne("loanProductThirdPartyId", new Long(-1)));
					
					/* only running in java 8
					List<Long> list = new LinkedList<Long>();
					list.addAll((List<Long>)o[1]);
					String strList = list.stream().collect(Collectors.toList()).toString();
					*/
					
					String strNote = o[2] != null ? (String)o[2] : "";
					
					log.info("loanProduct3Party : "+loanProductThirdParty);
					if (loanProductThirdParty!=null) {
	
						Lookup lookup = basicDAOFactory.getLookupDAO().findById(loanProductThirdParty.getKimoTopUpServiceBank().getLookupId());
						log.info(lookup.getCode());
						if (lookup !=null && lookup.getCode().trim().startsWith("153")) {
							Session sessionJ = null;
							Transaction transJ = null;
							try {
								sessionJ = otherTransactionDAOFactory.getOtDisbursementTransferInfoDAO().getSession(com.mpe.common.util.Constants.HIBERNATE_CFG_KEY_1);
								
								transJ = sessionJ.beginTransaction();
								
								OtDisbursementTransferInfo otDisbursementTransferInfo = new OtDisbursementTransferInfo();
								otDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
								otDisbursementTransferInfo.setAccountDebitCode("TMU");
								otDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
								otDisbursementTransferInfo.setAmount((BigDecimal)o[0]);
								otDisbursementTransferInfo.setAccountCredit(loanProductThirdParty.getKimoTopUpServiceAccountNumber());
								otDisbursementTransferInfo.setAccountCreditName(loanProductThirdParty.getKimoTopUpServiceAccountName());
								otDisbursementTransferInfo.setAccountCreditCode("OTX"+CommonUtil.getStringFromDate(new Date(), "yyMMddHHmmssSSS"));
								otDisbursementTransferInfo.setNote(strNote);
								
								otherTransactionDAOFactory.getOtDisbursementTransferInfoDAO().save(otDisbursementTransferInfo, sessionJ);
								long idInfo = otDisbursementTransferInfo.getOtDisbursementTransferInfoId();
								
								otherTransactionDAOFactory.getOtherTransactionDAO().updateOtDisbursementTransferInfoId((List<Long>)o[1], idInfo, sessionJ);
								transJ.commit();
								
								log.info("trx: "+otDisbursementTransferInfo.getAccountCreditCode()+" id: "+idInfo+" tot: "+otDisbursementTransferInfo.getAmount()+" otIds : "+otDisbursementTransferInfo.getNote());
							} catch (Exception ex) {
								if (transJ!=null)transJ.rollback();
								ex.printStackTrace();
							} finally {
								if (sessionJ!=null)sessionJ.close(); sessionJ=null;
							}
						}
					}	
				}
			}
			
			log.info(" <<<<<<<< END of OtherTransactionDisbursementTask "+this.getClass().getSimpleName()+" doOtherTransaction() >>>>>>>>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* tidak dipakai */
	/*
	public void processOtherTransaction() {
		try {
			log.info(" <<<<<<<< START of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processOtherTransaction() >>>>>>>>");
			
			// sum amount and ids transaction_date < today
			Object[] o = otherTransactionDAOFactory.getOtherTransactionDAO().getTotalAmountAndTotalId(new Date());
			
			if (o[0]!=null && o[1]!=null) {
				if (((BigDecimal)o[0]).compareTo(BigDecimal.ZERO)>0 && ((long)o[1])>0) {

					// bank danamas!
					BankAccount danamasBankAccount = accountDAOFactory.getBankAccountDAO().findByCriteria(Restrictions.isNull("accountId"), Restrictions.isNull("prepaidProviderId"));
					
					LoanProductThirdParty loanProductThirdParty = loanDAOFactory.getLoanProductThirdPartyDAO().findByCriteria(Restrictions.ne("loanProductThirdPartyId", new Long(-1)));
					
					log.info("loanProduct3Party : "+loanProductThirdParty);
					if (loanProductThirdParty!=null) {

						Lookup lookup = basicDAOFactory.getLookupDAO().findById(loanProductThirdParty.getKimoTopUpServiceBank().getLookupId());
						log.info(lookup.getCode());
						if (lookup !=null && lookup.getCode().trim().startsWith("153")) {
							
							OtDisbursementTransferInfo otDisbursementTransferInfo = new OtDisbursementTransferInfo();
							otDisbursementTransferInfo.setAccountDebit(danamasBankAccount.getAccountNumber());
							otDisbursementTransferInfo.setAccountDebitCode("TMU");
							otDisbursementTransferInfo.setAccountDebitName(danamasBankAccount.getAccountName());
							otDisbursementTransferInfo.setAmount((BigDecimal)o[0]);
							otDisbursementTransferInfo.setAccountCredit(loanProductThirdParty.getKimoTopUpServiceAccountNumber());
							otDisbursementTransferInfo.setAccountCreditName(loanProductThirdParty.getKimoTopUpServiceAccountName());
							otDisbursementTransferInfo.setAccountCreditCode("OTX"+CommonUtil.getStringFromDate(new Date(), "yyMMddHHmmssSSS"));
							
							otherTransactionDAOFactory.getOtDisbursementTransferInfoDAO().save(otDisbursementTransferInfo);
							long idInfo = otDisbursementTransferInfo.getOtDisbursementTransferInfoId();
							
							otherTransactionDAOFactory.getOtherTransactionDAO().updateOtDisbursementTransferInfoId(new Date(), idInfo);
							log.info("trx: "+otDisbursementTransferInfo.getAccountCreditCode()+" id: "+idInfo+" ids: "+(long)o[1]+" tot: "+otDisbursementTransferInfo.getAmount());
						}
					}	
				}
			}
			
			log.info(" <<<<<<<< END of LoanDisbursementPpobTask "+this.getClass().getSimpleName()+" processOtherTransaction() >>>>>>>>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	*/
}
