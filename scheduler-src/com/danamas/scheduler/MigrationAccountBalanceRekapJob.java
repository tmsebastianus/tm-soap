package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class MigrationAccountBalanceRekapJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			MigrationAccountBalanceRekapTask migrationAccountBalanceRekapTask = new MigrationAccountBalanceRekapTask();
			migrationAccountBalanceRekapTask.copyData();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
