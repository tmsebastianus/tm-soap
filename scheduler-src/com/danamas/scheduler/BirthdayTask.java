package com.danamas.scheduler;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.mpe.common.util.Formater;
import com.mpe.message.model.OutgoingEmail;
import com.mpe.message.model.dao.MessageDAOFactory;
import com.mpe.message.model.dao.MessageDAOFactoryHibernate;
import com.tokomodal.account.model.Account;
import com.tokomodal.account.model.AccountType;
import com.tokomodal.account.model.Personal;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.account.model.other.Gender;

public class BirthdayTask {
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	MessageDAOFactory messageDAOFactory = MessageDAOFactory.instance(MessageDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	public void SendEmailBirthdate(){
		try {
			List<Long> BirthdateDay = accountDAOFactory.getPersonalDAO().findBirthDateToday();
			if (BirthdateDay.size()!=0) {
				log.info(">>>>>>>>>>>>>>>>>>>> Start of BirthdayTask <<<<<<<<<<<<<<<<<<<<");
				
				int i = 0;
				int no = BirthdateDay.size() - 1;			
				for (i = 0; i <=no ; i++) {
//					String name = accountDAOFactory.getAccountDAO().getNameByAccountId((BigInteger) ((BirthdateDay.get(i))));
					Properties p = new Properties();
					
					ClassLoader loader = GiroBillerTask.class.getClassLoader();
				    URL r = loader.getResource("/");
				    String pathCls = r.getPath();
				    String path = pathCls.substring(0, pathCls.indexOf("/WEB-INF"));
					p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, path);
					Velocity.init(p);
					VelocityContext context = new VelocityContext();	
					String name = accountDAOFactory.getAccountDAO().getNameByAccountId(BirthdateDay.get(i));
					context.put("name", name);
					String email = accountDAOFactory.getAccountDAO().getEmailByAccountId(BirthdateDay.get(i));
					Long accountId = accountDAOFactory.getAccountDAO().getAccountIdByEmail(email);
					Account account = accountDAOFactory.getAccountDAO().findById(accountId);
					if (account.getType()==AccountType.PERSONAL) {
						Personal personal = (Personal)account;
						if (personal.getGender()==Gender.L) context.put("salutation", "Bpk/Sdr.");
						else context.put("salutation", "Ibu/Sdri.");
					} else {
						context.put("salutation", "");
					}
					StringWriter w = new StringWriter();
					Velocity.mergeTemplate("/template/birthDate.vm", context, w );
				
					// save to email
					OutgoingEmail outgoingEmail2 = new OutgoingEmail();
					outgoingEmail2.setEmailDate(new Date());
					outgoingEmail2.setSender("Tokomodal - Customer Service <cs@tokomodal.co.id>");
					outgoingEmail2.setSubject("[Tokomodal] Selamat Ulang Tahun");
					outgoingEmail2.setTo(email);
					outgoingEmail2.setMessage(w.toString());
					outgoingEmail2.setType("[Tokomodal] Selamat Ulang Tahun");
					messageDAOFactory.getOutgoingEmailDAO().save(outgoingEmail2);

				}
				
				log.info(">>>>>>>>>>>>>>>>>>>> End of BirthdayTask <<<<<<<<<<<<<<<<<<<<");
					
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
