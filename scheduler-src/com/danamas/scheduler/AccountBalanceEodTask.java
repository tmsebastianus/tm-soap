package com.danamas.scheduler;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.AccountType;
import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.loan.model.AccountBalanceDaily;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;

public class AccountBalanceEodTask {
	
	public void calculateAccountBalanceDaily() {
		
		Logger logger = Logger.getLogger(this.getClass());
		
		BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
		AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
		LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
		
		try {
			
			logger.info(" ************ "+this.getClass().getName()+" *** calculateAccountBalanceDaily() *** START ********** ");
			
			Calendar c1 = new GregorianCalendar();
			c1.set(Calendar.YEAR,2016);
			c1.set(Calendar.MONTH, Calendar.JULY);
			c1.set(Calendar.DATE, 1);
			
			/*
			 * RUN 1x/DAY!
			 * RUN at 01:00 AM!
			 */
			Calendar toCal = new GregorianCalendar();
			toCal.add(Calendar.DATE, -1);
			toCal.set(Calendar.HOUR_OF_DAY, 0);
			toCal.set(Calendar.MINUTE, 0);
			toCal.set(Calendar.SECOND, 0);
			
			
			// list active lender!
			List<Long> lenderIds = accountDAOFactory.getAccountDAO().getActiveLenderByAccountType(null);
			for (Long lenderId : lenderIds) {
				// get last daily balance
				AccountBalanceDaily accountBalanceDaily = loanDAOFactory.getAccountBalanceDailyDAO().findByCriteria(Restrictions.eq("accountId", lenderId));
				if (accountBalanceDaily==null) {
					// get balance from beginning!
					BigDecimal b = loanDAOFactory.getAccountBalanceDAO().getAccountBalanceAmountByAccountId(lenderId, c1.getTime(), toCal.getTime());
					accountBalanceDaily = new AccountBalanceDaily();
					accountBalanceDaily.setAccountId(lenderId);
					accountBalanceDaily.setAmount(b);
					accountBalanceDaily.setBalanceDate(toCal.getTime());
					loanDAOFactory.getAccountBalanceDailyDAO().save(accountBalanceDaily);
				} else {
					// cek date!
					BigDecimal b = loanDAOFactory.getAccountBalanceDAO().getAccountBalanceAmountByAccountId(lenderId, toCal.getTime(), toCal.getTime());
					// get balance
					accountBalanceDaily.setAmount(accountBalanceDaily.getAmount().add(b));
					accountBalanceDaily.setBalanceDate(toCal.getTime());
					loanDAOFactory.getAccountBalanceDailyDAO().update(accountBalanceDaily);
				}
				
			}
			
			
			
			logger.info(" ************ "+this.getClass().getName()+" *** calculateAccountBalanceDaily() *** END ********** ");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
