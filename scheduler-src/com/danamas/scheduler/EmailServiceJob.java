package com.danamas.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author agunghadiw
 * @create on Mar 5, 2014 3:17:03 PM
 */

@DisallowConcurrentExecution
public class EmailServiceJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			EmailServiceTask emailServiceTask = new EmailServiceTask();
			emailServiceTask.sendEmail();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

}
