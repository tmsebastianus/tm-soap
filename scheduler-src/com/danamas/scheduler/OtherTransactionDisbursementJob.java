package com.danamas.scheduler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Restrictions;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;

@DisallowConcurrentExecution
public class OtherTransactionDisbursementJob implements Job {

	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			if (applicationSetup.getIsDirectTransferActive()!=null && applicationSetup.getIsDirectTransferActive() && 
				applicationSetup.getIsTransferInfoEnable()!=null && applicationSetup.getIsTransferInfoEnable()) {
				
				OtherTransactionDisbursementTask otherTransactionDisbursementTask = new OtherTransactionDisbursementTask();
				otherTransactionDisbursementTask.doOtherTransaction();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
