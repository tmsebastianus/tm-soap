package com.danamas.scheduler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.message.model.dao.MessageDAOFactory;
import com.mpe.message.model.dao.MessageDAOFactoryHibernate;
import com.mpe.upload.model.dao.UploadDAOFactory;
import com.mpe.upload.model.dao.UploadDAOFactoryHibernate;

public class RemoveOldRegisterTask {
	
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	MessageDAOFactory messageDAOFactory = MessageDAOFactory.instance(MessageDAOFactoryHibernate.class);
	UploadDAOFactory uploadDAOFactory = UploadDAOFactory.instance(UploadDAOFactoryHibernate.class);
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	
	public void deleteInvalidAccount() {
		try {
			
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			if (applicationSetup!=null && applicationSetup.getMaxValidRegistration()>0) {
				// hour to days
				int days = (int)(applicationSetup.getMaxValidRegistration()/24);
				accountDAOFactory.getAccountDAO().removeInvalidOldAccount(days);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
