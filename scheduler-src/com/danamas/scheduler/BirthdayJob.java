package com.danamas.scheduler;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.tokomodal.loan.model.other.AccountBalanceList;
@DisallowConcurrentExecution
public class BirthdayJob implements Job {
	
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		try {
			
			BirthdayTask birthdayTask = new BirthdayTask();
			birthdayTask.SendEmailBirthdate();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}  

}