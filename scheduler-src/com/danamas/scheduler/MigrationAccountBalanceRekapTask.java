package com.danamas.scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.tokomodal.account.model.dao.AccountDAOFactory;
import com.tokomodal.account.model.dao.AccountDAOFactoryHibernate;
import com.tokomodal.history.model.AccountBalanceRekapHistory;
import com.tokomodal.history.model.dao.HistoryDAOFactory;
import com.tokomodal.history.model.dao.HistoryDAOFactoryHibernate;
import com.tokomodal.loan.model.dao.LoanDAOFactory;
import com.tokomodal.loan.model.dao.LoanDAOFactoryHibernate;
import com.mpe.common.util.CommonUtil;
import com.mpe.common.util.Constants;

public class MigrationAccountBalanceRekapTask {
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	AccountDAOFactory accountDAOFactory = AccountDAOFactory.instance(AccountDAOFactoryHibernate.class);
	LoanDAOFactory loanDAOFactory = LoanDAOFactory.instance(LoanDAOFactoryHibernate.class);
	HistoryDAOFactory historyDAOFactory = HistoryDAOFactory.instance(HistoryDAOFactoryHibernate.class);
			
	public void copyData() {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration("migration.properties");
			String strEndMigrationDate = config.getString("migration.properties");
			Date endMigrationDate = CommonUtil.getDateFromString(strEndMigrationDate, "dd-MM-yyyy");
			
			AccountBalanceRekapHistory accountBalanceRekapHistory = null;
			Session session = null;
			try {
				session = historyDAOFactory.getAccountBalanceRekapHistoryDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
				accountBalanceRekapHistory = (AccountBalanceRekapHistory)session.createCriteria(AccountBalanceRekapHistory.class)
						.add(Restrictions.ne("accountBalanceRekapId", new Long(-1)))
						.addOrder(Order.desc("accountId"))
						.setMaxResults(1)
						.uniqueResult();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				if (session!=null)session.close();
			}
			
			// get lastlender
			Long lastLenderId = null;
			if (accountBalanceRekapHistory!=null) lastLenderId  = accountBalanceRekapHistory.getAccountId(); else lastLenderId = new Long(0);
			
			
			List<Long> ids = accountDAOFactory.getAccountDAO().findAccountLenderList(20, lastLenderId);
			if (ids!=null && ids.size()>0) {
				Calendar c1 = new GregorianCalendar();
				c1.set(Calendar.YEAR,2016);
				c1.set(Calendar.MONTH, Calendar.JULY);
				c1.set(Calendar.DATE, 1);
				c1.set(Calendar.HOUR_OF_DAY, 0);
				c1.set(Calendar.MINUTE, 0);
				c1.set(Calendar.SECOND, 0);
				c1.set(Calendar.MILLISECOND, 0);
				
				Calendar c2 = new GregorianCalendar();
				c2.setTime(endMigrationDate); // change to cut of migration
				c2.set(Calendar.HOUR_OF_DAY, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0);
				
				for (Long id : ids) {
					loanDAOFactory.getAccountBalanceRekapDAO().insertMigrationAccountBalanceRekap(id, c1.getTime(), c2.getTime());
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
