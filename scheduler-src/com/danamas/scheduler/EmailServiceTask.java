package com.danamas.scheduler;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;
import com.mpe.common.util.Constants;
import com.mpe.message.model.OutgoingEmail;
import com.mpe.message.model.dao.MessageDAOFactory;
import com.mpe.message.model.dao.MessageDAOFactoryHibernate;
import com.mpe.upload.model.UploadFile;
import com.mpe.upload.model.dao.UploadDAOFactory;
import com.mpe.upload.model.dao.UploadDAOFactoryHibernate;

/**
 * @author agunghadiw
 * @create on Mar 5, 2014 3:17:18 PM
 */

public class EmailServiceTask {
	
	Log log = LogFactory.getFactory().getInstance(this.getClass());
	
	BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
	MessageDAOFactory messageDAOFactory = MessageDAOFactory.instance(MessageDAOFactoryHibernate.class);
	UploadDAOFactory uploadDAOFactory = UploadDAOFactory.instance(UploadDAOFactoryHibernate.class);
	
	@SuppressWarnings("unchecked")
	public void sendEmail() {
		
		try {
			
			//PropertiesConfiguration config = new PropertiesConfiguration("email.properties");
			//String mailHost = config.getString("mailHost");
			
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			if (applicationSetup!=null && applicationSetup.isEmailInternalEnable()) {
				
				String mailHost = applicationSetup.getSmtpEmail();
				if (mailHost!=null && mailHost.length()>0) {
					Session session = null;
					List<OutgoingEmail> outgoingEmails = null;
					try {
						session = messageDAOFactory.getOutgoingEmailDAO().getSession(Constants.HIBERNATE_CFG_KEY_1);
						outgoingEmails = session.createCriteria(OutgoingEmail.class)
								.setFetchMode("uploadFile", FetchMode.JOIN)
								.add(Restrictions.isNull("sentDate"))
								//.add(Restrictions.eq("emailDate", new Date()))
								.add(Restrictions.or(Restrictions.isNull("retryTimes"), Restrictions.lt("retryTimes", applicationSetup.getMaxRetryTimes()!=null?applicationSetup.getMaxRetryTimes():3)))
								.setMaxResults(10) // limit every 10 data
								.list();
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						if (session!=null) session.close();
					}
					
					log.info(" [ EMAIL SERVICE START >> "+outgoingEmails.size()+"  ] ");
					
					for (OutgoingEmail outgoingEmail : outgoingEmails) {
						try {
							javax.mail.Session ss = null;
							Properties props = new Properties();
							props.put("mail.smtp.host", mailHost);
							if (applicationSetup.getSmtpUserName()!=null && applicationSetup.getSmtpUserName().length()>0 
									&& applicationSetup.getSmtpUserName()!=null && applicationSetup.getSmtpUserName().length()>0) {
								final String u = applicationSetup.getSmtpUserName();
								final String p = applicationSetup.getSmtpPassword();
								ss = javax.mail.Session.getInstance(props, new Authenticator() {
									protected PasswordAuthentication getPasswordAuthentication() {
										return new PasswordAuthentication(u, p);
									}
								});
							} else {
								ss = javax.mail.Session.getInstance(props, null);
							}
							ss.setDebug(false);
							
							MimeMessage msg = new MimeMessage(ss);
							msg.setFrom(new InternetAddress(outgoingEmail.getSender()));
							msg.setRecipient(Message.RecipientType.TO, new InternetAddress(outgoingEmail.getTo()));
							msg.addRecipient(Message.RecipientType.BCC, new InternetAddress("notification@tokomodal.co.id"));
							msg.setSubject(outgoingEmail.getSubject());
							msg.setSentDate(outgoingEmail.getEmailDate());
							// setText(text, charset)
							//msg.setText(rs.getString("message"));
							// send html
							//msg.setDataHandler(new javax.activation.DataHandler(new ByteArrayDataSource(rs.getString("message"), "text/html")));
							MimeBodyPart messagePart = new MimeBodyPart();
							messagePart.setDataHandler(new javax.activation.DataHandler(new ByteArrayDataSource(outgoingEmail.getMessage(), "text/html")));
							Multipart multipart = new MimeMultipart();
							multipart.addBodyPart(messagePart);
							
							if (outgoingEmail.getUploadFileId()!=null) {
								UploadFile uploadFile = uploadDAOFactory.getUploadFileDAO().findById(outgoingEmail.getUploadFileId());
								// Set the email attachment file
								if (uploadFile!=null) {
									MimeBodyPart attachmentPart = new MimeBodyPart();
									ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(uploadFile.getFileContent(), uploadFile.getFileContentType());
									attachmentPart.setDataHandler(new DataHandler(byteArrayDataSource));
									if (uploadFile.getFileName()!=null && uploadFile.getFileName().length()>0) {
										attachmentPart.setFileName(uploadFile.getFileName());
									}
									multipart.addBodyPart(attachmentPart);
								}
							}
							msg.setContent(multipart);								
							Transport.send(msg);
							
							// delete if success
							outgoingEmail.setSentDate(new Date());
							if (outgoingEmail.getEmailContract()!=null && outgoingEmail.getEmailContract()==true && outgoingEmail.getUploadFileId()!=null) {
								// delete file
								uploadDAOFactory.getUploadFileDAO().delete(outgoingEmail.getUploadFileId());
								outgoingEmail.setUploadFileId(null);
							}
							messageDAOFactory.getOutgoingEmailDAO().update(outgoingEmail);
							
						} catch (Exception e) {
							e.printStackTrace();
							log.info("[ Error sending email = "+e.getMessage()+" ]");
							try {
								outgoingEmail.setRetryTimes(outgoingEmail.getRetryTimes()!=null?(outgoingEmail.getRetryTimes()+1):1);
								messageDAOFactory.getOutgoingEmailDAO().update(outgoingEmail);
							} catch (Exception e2) {
								// delete error/broken email
								messageDAOFactory.getOutgoingEmailDAO().delete(outgoingEmail.getOutgoingEmailId());
							}
							
						}
					}
					log.info(" [ EMAIL SERVICE STOP >> "+outgoingEmails.size()+"  ] ");
				}
				
				//System.out.println("[ Email sender job ......"+mailHost+ " ] ");
			}
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
