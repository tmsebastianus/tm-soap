package com.danamas.scheduler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Restrictions;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.mpe.basic.model.ApplicationSetup;
import com.mpe.basic.model.dao.BasicDAOFactory;
import com.mpe.basic.model.dao.BasicDAOFactoryHibernate;

@DisallowConcurrentExecution
public class LoanDisbursementPpobJob implements Job {

	Log log = LogFactory.getFactory().getInstance(this.getClass());

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {

			BasicDAOFactory basicDAOFactory = BasicDAOFactory.instance(BasicDAOFactoryHibernate.class);
			
			ApplicationSetup applicationSetup = basicDAOFactory.getApplicationSetupDAO().findByCriteria(Restrictions.ne("applicationSetupId", new Long(-1)));
			log.info("----------- ppob loan disburesment ------------ ppob loan => "+applicationSetup.getIsPpobLoanActive()+" ~ ppob co => "+applicationSetup.getIsPpobCashOutActive()+" ~ drt > "+applicationSetup.getIsDirectTransferActive());
			if (applicationSetup.getIsTransferInfoEnable()!=null && applicationSetup.getIsTransferInfoEnable()) {
				if (applicationSetup.getIsPpobLoanActive()!=null || 
					applicationSetup.getIsPpobCashOutActive()!=null ||
					applicationSetup.getIsDirectTransferActive()!=null
					) {
					
					LoanDisbursementPpobTask loanDisbursementPpobTask = new LoanDisbursementPpobTask();
	
					if (applicationSetup.getIsDirectTransferActive()!=null && applicationSetup.getIsDirectTransferActive()) {
						
						loanDisbursementPpobTask.processPpob();
						loanDisbursementPpobTask.drawdownLoanAfterPpob();
						loanDisbursementPpobTask.processCashOutPpob();
						loanDisbursementPpobTask.processAm();
						//loanDisbursementPpobTask.processKl();
						
					} else if (applicationSetup.getIsDirectTransferActive()!=null && !applicationSetup.getIsDirectTransferActive()) {
					
						if (applicationSetup.getIsPpobLoanActive()!=null && applicationSetup.getIsPpobLoanActive()) {
							loanDisbursementPpobTask.processPpob();
							loanDisbursementPpobTask.drawdownLoanAfterPpob();
						}
						//
						if (applicationSetup.getIsPpobCashOutActive()!=null && applicationSetup.getIsPpobCashOutActive()) loanDisbursementPpobTask.processCashOutPpob();
					
					}
						
				}
			}	
			log.info("----------- end of ppob loan disburesment ------------");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
